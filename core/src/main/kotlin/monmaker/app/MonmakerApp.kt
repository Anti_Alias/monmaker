package monmaker.app

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Graphics
import com.badlogic.gdx.Input
import com.badlogic.gdx.controllers.Controllers
import com.badlogic.gdx.controllers.PovDirection
import monmaker.game.Game
import monmaker.controller.Button
import monmaker.controller.KeyboardMapping
import monmaker.logging.Logger
import kotlin.math.max

typealias GdxController = com.badlogic.gdx.controllers.Controller

/**
 * Helper class for creating monmaker apps.
 */
abstract class MonmakerApp : ApplicationAdapter() {

    private lateinit var config: UserConfig
    private lateinit var game: Game
    private lateinit var fullscreenDisplayMode: Graphics.DisplayMode
    private var gdxController: GdxController? = null
    private var sleepUntilNano: Long = -1L
    private val keyboardMapping = KeyboardMapping.default()

    /**
     * Fullscreen flag.
     */
    private val isFullscreen: Boolean get() = Gdx.graphics.isFullscreen

    /**
     * Sets the configuration object for the app.
     * Not to be called manually.
     */
    fun setConfiguration(config: UserConfig) {
        this.config = config
    }

    /**
     * Sets the fullscreen display mode when fullscreen is toggled.
     */
    fun setFullscreenDisplayMode(fullscreenDisplayMode: Graphics.DisplayMode) {
        this.fullscreenDisplayMode = fullscreenDisplayMode
    }

    /**
     * Sets the LibGDX controller.
     */
    internal fun setController(controller: GdxController) {
        if(gdxController == null)
            gdxController = controller
    }

    /**
     * Unsets the LibGDX controller.
     */
    internal fun unsetController(controller: GdxController) {
        if(gdxController == controller)
            gdxController = null
    }

    internal fun buttonDown(controller: GdxController, buttonCode: Int) {
        if(gdxController == controller) {
            val button: Button? = Button.fromControllerCode(buttonCode)
            if(button != null) game.controller.press(button)
        }
    }

    internal fun buttonUp(controller: GdxController, buttonCode: Int) {
        if(gdxController == controller) {
            val button: Button? = Button.fromControllerCode(buttonCode)
            if(button != null) game.controller.release(button)
        }
    }

    internal fun keyDown(keycode: Int) {
        val button: Button? = keyboardMapping[keycode]
        if(button != null)
            game.controller.press(button)
    }

    internal fun keyUp(keycode: Int) {
        val button: Button? = keyboardMapping[keycode]
        if(button != null)
            game.controller.release(button)
    }

    internal fun povMoved(direction: PovDirection) {

        // Cears directional buttons
        val controller = game.controller
        controller.release(Button.UP)
        controller.release(Button.DOWN)
        controller.release(Button.LEFT)
        controller.release(Button.RIGHT)

        // Determines which buttons are being presses
        if(direction == PovDirection.north) game.controller.press(Button.UP)
        else if(direction == PovDirection.south) game.controller.press(Button.DOWN)
        else if(direction == PovDirection.east) game.controller.press(Button.RIGHT)
        else if(direction == PovDirection.west) game.controller.press(Button.LEFT)
        if(direction == PovDirection.northEast) {
            game.controller.press(Button.UP)
            game.controller.press(Button.RIGHT)
        }
        if(direction == PovDirection.northWest) {
            game.controller.press(Button.UP)
            game.controller.press(Button.LEFT)
        }
        if(direction == PovDirection.southEast) {
            game.controller.press(Button.DOWN)
            game.controller.press(Button.RIGHT)
        }
        if(direction == PovDirection.southWest) {
            game.controller.press(Button.DOWN)
            game.controller.press(Button.LEFT)
        }
    }

    /**
     * When invoked, creates a [Game] instance to play.
     */
    abstract fun createGame(): Game

    /**
     * Invoked when OpenGL, OpenAL, etc are ready.
     * This creates the Game and its resources.
     */
    override fun create() {

        // Assigns listeners
        Controllers.addListener(MonmakerControllerListener(this))
        Gdx.input.inputProcessor = MonmakerInputProcessor(this)

        // Creates the game. Immediately goes into fullscreen if configured to do so.
        game = createGame()
        if(!isFullscreen && config.mode == Mode.FULLSCREEN)
            fullscreen()
        logger.log("Created Game '${game.name}'.")
    }

    /**
     * Executes game logic and renders game.
     */
    override fun render () {

        val altLeftPressed = Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT)
        val altRightPressed = Gdx.input.isKeyPressed(Input.Keys.ALT_RIGHT)
        val enterPressed = Gdx.input.isKeyJustPressed(Input.Keys.ENTER)
        if ((altLeftPressed || altRightPressed) && enterPressed)
            toggleFullscreen()

        // Runs game and renders graphics
        // Supports vsync and non-vsync modes.
        if(vsync()) {
            game.run(Gdx.graphics.deltaTime)
            game.render(Gdx.graphics.width, Gdx.graphics.height)
        }
        else {
            if(sleepUntilNano == -1L) sleepUntilNano = currentTimeNano() + sleepTimeNano()
            else {
                sleep()
                sleepUntilNano += sleepTimeNano()
            }
            game.run(Gdx.graphics.deltaTime)
            game.render(Gdx.graphics.width, Gdx.graphics.height)
        }
    }

    /**
     * Framerate of the app. Depends on fullscreen/windowed status.
     */
    private fun fps(): Int = if(isFullscreen) config.fullscreen.fps else config.windowed.fps

    /**
     * Vsync status of the app. Depends on fullscreen/windowed status.
     */
    private fun vsync(): Boolean = if(isFullscreen) config.fullscreen.vsync else config.windowed.vsync

    /**
     * Estimated time in which the non-vsync app should sleep after runnnig/rendering.
     */
    private fun sleepTimeNano(): Long = (1000000 * 1000 / fps()).toLong()

    /**
     * Causes the application to sleep. Amount of time depends on the FPS.
     */
    private fun sleep() {
        val currentTimeNano: Long = currentTimeNano()
        val sleepTime: Long = max(0L, sleepUntilNano - currentTimeNano) / 1000000L
        Thread.sleep(sleepTime)
    }

    private fun currentTimeNano(): Long = System.nanoTime()

    /**
     * Toggles between fullscreen and windowed mode.
     */
    private fun toggleFullscreen() {
        if(isFullscreen) windowed()
        else fullscreen()
    }

    private fun windowed() {
        Gdx.graphics.setWindowedMode(config.windowed.width, config.windowed.height)
        Gdx.graphics.setVSync(config.windowed.vsync)
        logger.log("Set to windowed mode")
    }

    private fun fullscreen() {
        Gdx.graphics.setFullscreenMode(fullscreenDisplayMode)
        Gdx.graphics.setVSync(config.fullscreen.vsync)
        logger.log("Set to fullscreen mode")
    }


    /**
     * Disposes of all game's resources.
     */
    override fun dispose () {
        game.unload()
        logger.log("Game '${game.name} disposed.")
    }

    companion object {
        private val logger = Logger.forClass<MonmakerApp>()
    }
}