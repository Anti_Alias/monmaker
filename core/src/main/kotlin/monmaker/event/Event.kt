package monmaker.event

/**
 * Represents some event that happened in a [World].
 */
interface Event

/**
 * Represents a single game tick.
 */
data class TickEvent(val deltaTime: Float) : Event