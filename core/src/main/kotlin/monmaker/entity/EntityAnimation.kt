package monmaker.entity

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import monmaker.world.Camera
import monmaker.exceptions.MonmakerException
import monmaker.extensions.loadSync
import monmaker.game.AssetPaths
import kotlin.math.roundToInt

/**
 * Represents four variants of an animation for an Entity.
 * These animations are in directions north, south, east and west.
 * When loaded from an image, frames will be chopped in sizes of [frameWidth]tileX[frameHeight].
 * Image need not be a vertical or horizontal strip.
 * From there, the frames will be divided into four chunks, with each chunk being a quarter of the number of frames total.
 *
 * The first quarter is for the southern animation.
 * The second quarter is for the eastern animation.
 * The third quarter is for the northern animation.
 * The fourth quarter is for the western animation.
 *
 * @property file Image file to load from.
 * @property frameWidth Width of each frame in pixels.
 * @property frameHeight Height of each frame in pixels.
 * @property direction Initial direction animation should be facing. Defaults to [Direction.S].
 * @property centerX Center of animation in which to pivot. Defaults to frameWidth/2. (Center of body).
 * @property centerY Center of animation in which to pivot. Defaults to 1. (Location of feet + 1 pixel up).
 * @property frameIndices Indices of frames that should be part of the animation.
 */
class EntityAnimation @JvmOverloads constructor(
    val file: String,
    val frameWidth: Int = 32,
    val frameHeight: Int = 32,
    var centerX: Int = frameWidth/2,
    var centerY: Int = 1,
    val frameIndices: List<Int>? = null
) {

    private var southernFrames: Array<TextureRegion>? = null
    private var easternFrames: Array<TextureRegion>? = null
    private var northernFrames: Array<TextureRegion>? = null
    private var westernFrames: Array<TextureRegion>? = null

    init {
        if(frameWidth <= 0 || frameHeight <= 0)
            throw MonmakerException("Invalid frame size ${frameWidth}tileX${frameHeight}.")
    }

    /**
     * Loads contents from asset manager.
     */
    internal fun load(assetManager: AssetManager, paths: AssetPaths) {

        // Loads texture
        val fullPath = "${paths.animations}/$file"
        val texture: Texture = assetManager.loadSync(fullPath)
        try {

            // Loads frames into single list
            val allFrames = mutableListOf<TextureRegion>()
            var x = 0
            var y = 0
            while (y + frameHeight <= texture.height) {
                while (x + frameWidth <= texture.width) {
                    val region = TextureRegion(texture, x, y, frameWidth, frameHeight)
                    allFrames.add(region)
                    x += frameWidth
                }
                y += frameHeight
            }

            // Only includes frames in frameIndices, if it exists.
            val frames = if(frameIndices == null) allFrames else {
                val result = mutableListOf<TextureRegion>()
                for(index in frameIndices)
                    result.add(allFrames[index])
                result
            }

            // Splits list into quarters
            if (frames.size % 4 != 0)
                throw MonmakerException("File '$fullPath' had ${frames.size} frames, which is not divisible by 4.")
            val quarterSize: Int = frames.size / 4
            southernFrames = frames.subList(0, quarterSize).toTypedArray()
            easternFrames = frames.subList(quarterSize, quarterSize*2).toTypedArray()
            northernFrames = frames.subList(quarterSize*2, quarterSize*3).toTypedArray()
            westernFrames = frames.subList(quarterSize*3, frames.size).toTypedArray()
        }
        catch(t: Throwable) {
            assetManager.unload(fullPath)
        }
    }

    /**
     * Unloads contents from asset manager.
     */
    internal fun unload(assetManager: AssetManager, paths: AssetPaths) {
        assetManager.unload("${paths.animations}/$file")
        southernFrames = null
        easternFrames = null
        northernFrames = null
        westernFrames = null
    }

    /**
     * Renders this EntityAnimation.
     * Assumed to be loaded.
     *
     * @param t T-value which determines which frame to render.
     * When t=0f, render the first frame.
     * When t=1f, render the last. Etc.
     * @param x Supposed-x coordinate in pixels of this animation.
     * @param y Supposed-y coordinate in pixels of this animation.
     * @parma direction Direction to render the animation.
     * @param batch Batch used to render this animation.
     */
    internal fun render(t: Float, x: Float, y: Float, direction: Direction, batch: Batch) {
        if(t < 0 || t > 1) throw MonmakerException("T $t is outside of the range [0, 1]")
        val frames: Array<TextureRegion> = when(direction) {
            Direction.S -> southernFrames!!
            Direction.E -> easternFrames!!
            Direction.N -> northernFrames!!
            Direction.W -> westernFrames!!
        }
        val index: Int = Math.min(frames.size-1, (frames.size * t).toInt())
        val region: TextureRegion = frames[index]
        val finalX: Float = (x-centerX).roundToInt().toFloat()
        val finalY: Float = (y-centerY).roundToInt().toFloat()
        batch.begin()
        batch.draw(region, finalX, finalY)
        batch.end()
    }

    /**
     * Determines if the animation is on screen.
     * @param x Supposed-x coordinate in pixels of this animation.
     * @param y Supposed-y coordinate in pixels of this animation.
     */
    internal fun isOnScreen(x: Float, y: Float, camera: Camera): Boolean {
        val minX: Float = x-centerX
        val minY: Float = y-centerY
        val maxX: Float = minX + frameWidth
        val maxY: Float = minY + frameHeight
        return  camera.minX <= maxX &&
                camera.maxX >= minX &&
                camera.minY <= maxY &&
                camera.maxY >= minY
    }
}