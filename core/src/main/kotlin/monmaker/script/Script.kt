package monmaker.script

import monmaker.entity.Entity
import monmaker.event.Event
import monmaker.exceptions.MonmakerException
import monmaker.controller.Controller
import monmaker.game.Game
import monmaker.game.Promptor
import monmaker.world.World
import java.lang.reflect.Method
import java.util.*


/**
 * Represents behaviors that a [World] or [Entity] may have.
 */
abstract class Script {

    private var currentNext: Part? = null

    /**
     * Identifier for this Script. Generated randomly.
     */
    val id: UUID = UUID.randomUUID()

    /**
     * World this Script resides in.
     */
    abstract val world: World

    /**
     * [Game] this Script resides in.
     */
    val game: Game get() = world.game

    /**
     * Helper accessor to the [Game]'s [Promptor].
     */
    val promptor: Promptor get() = game.promptor

    /**
     * helper accessor to the [Game]'s [Controller].
     */
    val controller: Controller get() = game.controller

    /**
     * Produces a [Part] instance.
     * @param part Name of next part of this script to invoke.
     * @param scriptId ID of script to run once this [Part] has been completed.
     * @param counter Number of times this [Part] should be invoked before its completed.
     * @param args Arguments to pass into next part.
     */
    @JvmOverloads fun part(
        part: String,
        scriptId: UUID = id,
        counter: Int = 1,
        args: List<Any> = emptyList()
    ): Part {
        val part = Part(part, scriptId, counter, args)
        currentNext = part
        return part
    }

    /**
     * Handles an event.
     */
    abstract fun on(event: Event)

    /**
     * Invoked when script is registered by its parent, or its parent is registered to its parent.
     */
    open fun register() {}

    /**
     * Invoked when script is deregistered from its parent, or its parent is removed from its parent.
     */
    open fun deregister() {}

    /**
     * Runs a part with n-number of arguments.
     */
    internal fun runPart(part: String, args: List<Any>) {
        val classesList: List<Class<*>> = args.map { it.javaClass }
        val classes: Array<Class<*>> = classesList.toTypedArray()
        val argsArray: Array<Any> = args.toTypedArray()
        try {
            val method: Method = javaClass.getMethod(part, *classes)
            method.invoke(this, *argsArray)
        }
        catch(t: Throwable) {
            throw MonmakerException("Script '${javaClass.name} could not find part '$part' for arguments $classesList.", t)
        }
    }
}