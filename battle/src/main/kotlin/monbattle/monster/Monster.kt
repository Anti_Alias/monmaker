package monbattle.monster

import monbattle.battle.Battle
import monbattle.event.Event
import monbattle.exceptions.BattleException
import monbattle.species.BaseStats
import monbattle.species.Gender
import monbattle.species.Species
import java.util.*
import kotlin.math.floor

/**
 * Represents a living monster.
 */
class Monster @JvmOverloads constructor(
    level: Int = 1,
    val species: Species,
    val gender: Gender,
    val nature: Nature,
    val ivs: IndividualValues,
    val evs: EffortValues,
    var nickname: String? = null
) {

    /**
     * Name of this Monster.
     * @return [nickname] if it is not null. Otherwise, name of the species.
     */
    val name: String get() = nickname ?: species.name

    /**
     * Current level of the monster.
     */
    var level: Int = level
        set(value) {
            if(value !in 0..100) throw BattleException("Monster level $level outside of range [0, 100].")
            field = value
        }

    /**
     * Current status conditions of the monster.
     */
    val statusConditions: MutableSet<StatusCondition> = mutableSetOf()

    /**
     * Current HP of the monster.
     */
    var currentHP: Int = maxHp
        set(value) {
            if(value !in 0..maxHp)
                throw BattleException("HP value $value outside of range [0, ${species.baseStats.hp}]")
            field = value
        }

    /**
     * Max HP of this monster.
     */
    val maxHp: Int get() {
        val baseStats: BaseStats = species.baseStats
        val numerator: Int = (2 * baseStats.hp + ivs.hp + evs.hp/4) * level
        val lhs: Int = numerator / 100
        val rhs: Int = level + 10
        return lhs + rhs
    }

    /**
     * Attack for this Monster.
     */
    val attack: Int get() = calculateStat(
        species.baseStats.attack,
        ivs.attack,
        evs.attack,
        nature.attackMultiplier
    )

    /**
     * Defense for this Monster.
     */
    val defense: Int get() = calculateStat(
        species.baseStats.defense,
        ivs.defense,
        evs.defense,
        nature.defenseMultiplier
    )

    /**
     * Special attack for this Monster.
     */
    val specialAttack: Int get() = calculateStat(
        species.baseStats.specialAttack,
        ivs.specialAttack,
        evs.specialAttack,
        nature.specialAttackMultiplier
    )


    /**
     * Special defense for this Monster.
     */
    val specialDefense: Int get() = calculateStat(
        species.baseStats.specialDefense,
        ivs.specialDefense,
        evs.specialDefense,
        nature.specialDefenseMultiplier
    )

    /**
     * Speed for this Monster.
     */
    val speed: Int get() = calculateStat(
        species.baseStats.speed,
        ivs.speed,
        evs.speed,
        nature.speedMultiplier
    )

    /**
     * @return true if [currentHP] == 0.
     */
    val isDefeated: Boolean get() = currentHP == 0

    /**
     * Responds to an [Event].
     * @param event Event that occurred.
     * @param battle Battle it took place in.
     */
    internal fun on(event: Event, battle: Battle) {

    }

    /**
     * Calculates a particular stat using this monster's level, base stat, iv of stat, ev of stat and this monster's
     * nature multiplier for that stat.
     *
     * @param base Base stat
     * @param iv IV for that stat
     * @param ev EV for that stat
     * @param natureMultiplier Multiplier of this monster's nature
     */
    private fun calculateStat(base: Int, iv: Int, ev: Int, natureMultiplier: Double): Int {
        val numerator: Int = (2 * base + iv + ev / 4) * level
        val result: Double = (numerator / 100 + 5) * natureMultiplier
        return floor(result).toInt()
    }

    companion object {

        val MAX_LEVEL = 100

        /**
         * Generates a Monster, a member of the species.
         * @param species Species of monster to create.
         * @param nickname Optional nickname of the monster.
         * @param minLevel Minimum level.
         * @param maxLevel Maximum level.
         * @param random Source of randomness.
         * @return random monster generated.
         */
        @JvmOverloads
        fun random(
            species: Species,
            minLevel: Int = 1,
            maxLevel: Int = 100,
            nickname: String? = null,
            random: Random = Random()
        ): Monster {
            if(minLevel !in 1..MAX_LEVEL)
                throw BattleException("minLevel must be in range [1, 100]. Got $minLevel.")
            if(maxLevel !in minLevel..MAX_LEVEL)
                throw BattleException("maxLevel must be in range [minLevel, 100]. Got $maxLevel.")
            return Monster(
                level = random.nextInt(maxLevel-minLevel+1) + minLevel,
                nickname = nickname,
                species = species,
                gender = if (species.isGenderless) Gender.GENDERLESS else Gender.random(species.genderRatio!!, random),
                nature = Nature.random(random),
                ivs = IndividualValues.random(random),
                evs = EffortValues()
            )
        }
    }
}