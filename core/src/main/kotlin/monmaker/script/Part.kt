package monmaker.script

import monmaker.exceptions.MonmakerException
import monmaker.world.World
import java.util.UUID


/**
 * Acts as a callback primitive for a [Script].
 *
 * @property part Method to invoke after some action is completed.
 * @property scriptId ID of script with part to invoke.
 * @param counter Counter for the [Part] instance. Instance is considered "done" when this counter reaches 0.
 * Defaults to 1.
 * @param args Arguments to pass into next part.
 */
class Part(
    val part: String,
    val scriptId: UUID,
    counter: Int,
    val args: List<Any>
) {

    /**
     * Unique identifier.
     */
    val id: UUID = UUID.randomUUID()

    /**
     * Counter for the [Part] instance. Instance is considered "done" when this counter reaches 0.
     * Defaults to 1.
     */
    var counter: Int = counter
        private set

    init {
        if(counter <= 0)
            throw MonmakerException("Counter for a 'Next' instance must be positive. Got $counter.")
    }

    /**
     * Signals that this [Part] is complete.
     * Decrements internal counter variable.
     * When that counter reaches 0, [isComplete] returns true.
     *
     * @property world World to run the next script for if [isComplete] returns true.
     */
    fun complete(world: World) {
        if(counter > 0)
            counter -= 1
        if(isComplete) {
            val nextScript: Script = world.getScript(scriptId)
            nextScript.runPart(part, args)
        }
    }

    /**
     * @return true if complete.
     */
    val isComplete: Boolean get() = counter == 0
}