package monbattle.battle

import monbattle.decision.Decision
import monbattle.event.DecisionMadeEvent
import monbattle.event.DecisionPerformedEvent
import monbattle.event.DecisionRequestedEvent
import monbattle.event.Event
import monbattle.exceptions.BattleException
import monbattle.monster.Monster
import java.util.*

/**
 * Represents a team of monsters that will do battle.
 * @property name Name of the team.
 * @param monsters Monsters to start the team with.
 * @param slotCount Number of slots to hold monsters while they are on the field.
 */
class Team(
    val name: String,
    monsters: Collection<Monster>,
    slotCount: Int = 1
) {
    private val _monsters: MutableList<Monster>
    private var decision: Decision? = null


    init {
        if(slotCount <= 0) throw BattleException("A Team cannot have fewer than 1 slot. Got $slotCount.")
        if(monsters.isEmpty()) throw BattleException("A Team must have at least 1 monster. Got 0.")
        _monsters = monsters.toMutableList()
    }

    /**
     * Makes a decision for this [Team].
     *
     * @param decision Decision to make.
     * @param battle Battle this team is in.
     */
    fun decide(decision: Decision, battle: Battle) {
        this.decision ?: throw BattleException("Team '$name' cannot make a decision because it has already made one.")
        this.decision = decision
        battle.fire(DecisionMadeEvent(name))
    }

    /**
     * Performs a decision for this [Team].
     *
     * @param battle Battle this team is in.
     */
    internal fun performDecision(battle: Battle) {
        val decision = this.decision
        decision ?: throw BattleException("Team cannot perform a decision because it did not have one.")
        decision.perform(battle)
        battle.fire(DecisionPerformedEvent(name))
    }

    /**
     * Clears current decision, and requests for a new one.
     *
     * @param battle Battle this team is in.
     */
    internal fun nextDecision(battle: Battle) {
        this.decision = null
        battle.fire(DecisionRequestedEvent(name))
    }

    /**
     * @return true if all [Monster]s are defeated.
     */
    val isDefeated: Boolean get() = monsters.all { it.isDefeated }

    /**
     * Responds to a given [Event].
     * @param event Event that occurred.
     * @param battle Battle it occurred in.
     */
    internal fun on(event: Event, battle: Battle) {
        monsters.forEach { it.on(event, battle) }
    }

    /**
     * Monsters on this Team.
     */
    val monsters: List<Monster> = Collections.unmodifiableList(_monsters)
}