package monmaker.logging

import com.badlogic.gdx.Gdx

/**
 * Simple logger class.
 */
class Logger(val metaTag: String) {
    fun log(message: Any) {
        Gdx.app.log(metaTag, message.toString())
    }
    fun error(message: Any) {
        Gdx.app.error(metaTag, message.toString())
    }
    fun debug(message: Any) {
        Gdx.app.debug(metaTag, message.toString())
    }

    companion object {
        /**
         * Creates a [Logger] instance.
         * @param T Type meta tag logger
         */
        inline fun <reified T> forClass(): Logger = Logger(T::class.java.name)
    }
}