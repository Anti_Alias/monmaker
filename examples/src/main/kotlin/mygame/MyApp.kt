package mygame

import monbattle.app.MonmakerApp
import monbattle.game.Game


/**
 * Platform-independent application.
 */
class MyApp : MonmakerApp() {
    /**
     * Upon resources being ready (OpenGL, OpenAL, etc), creates game.
     */
    override fun createGame() = Game(
        name = "The Game",
        resolutionWidth = 16*30,
        resolutionHeight = 9*30,
        showFps = true,
        startingScreen = "Overworld",
        screenGenerator = SimpleScreenGenerator
    )
}
