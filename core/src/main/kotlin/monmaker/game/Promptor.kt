package monmaker.game

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import com.rafaskoberg.gdx.typinglabel.TypingAdapter
import com.rafaskoberg.gdx.typinglabel.TypingLabel
import com.rafaskoberg.gdx.typinglabel.TypingListener
import monmaker.controller.Button
import monmaker.event.PromptCloseEvent
import monmaker.event.PromptOpenEvent
import monmaker.exceptions.MonmakerException
import monmaker.extensions.loadSync
import monmaker.queue.ActionQueue
import monmaker.sound.Sound

/**
 * Object belonging to a [Game] instance responsible for prompting messages on the screen.
 * @property game Game that owns this [Promptor].
 * @property skin Name of skin file to use.
 * @property atlasFile Name of atlas file to use.
 * @param advanceButton Button to listen for in order to advance the promptor
 */
class Promptor @JvmOverloads constructor (
    val skinFile: String = "skin.json",
    val atlasFile: String = "skin.atlas",
    val promptWidth: Float = 300f,
    val promptHeight: Float = 80f,
    val advanceButton: Button = Button.A,
    val selectSound: Sound? = Sound("select.wav", 0.5f)
) {

    lateinit internal var game: Game                // Game that holds this Promptor
    private lateinit var skin: Skin
    private var pane: ScrollPane? = null
    private var label: TypingLabel? = null
    private var justPrompted: Boolean = false

    /**
     * Current status of the [Promptor].
     */
    var status: Status = Status.CLOSED
        private set

    /**
     * @return true if status is PROMPTING or PROMPTED.
     */
    val isOpen: Boolean get() = status == Status.PROMPTING || status == Status.PROMPTED

    /**
     * Queue of commands that allow for the promptor to prompt messages in order.
     */
    val queue: ActionQueue<Promptor> = ActionQueue()

    init {
        if(promptWidth < 0f || promptHeight < 0f)
            throw MonmakerException("Invalid prompt size ${promptWidth}x${promptHeight}")
    }

    /**
     * Prompts with the specified text.
     * @param text Text to prompt.
     * @param suppressEvent If true, will not emit a [PromptOpenEvent] upon opening.
     * @param listener Optional listener that will listen for events in the [Promptor].
     */
    @JvmOverloads fun prompt(text: String, suppressEvent: Boolean = false, listener: TypingListener? = null) {

        // Clears current label
        clearGraphics()

        // Creates typing label
        val label = TypingLabel(text, skin)
        with(label) {
            setAlignment(Align.topLeft, Align.left)
            setWrap(true)
            label.typingListener = if(listener == null) object : TypingAdapter() {
                override fun end() { status = Status.PROMPTED }
            }
            else TypingListenerWrapper(listener)
        }

        // Creates table/pane to store graphics
        val table = Table()
        val pane = ScrollPane(table, skin)
        with(pane) {
            setSize(promptWidth, promptHeight)
            setPosition((game.resolutionWidth - promptWidth)/2f, 1f)
        }
        pane.layout()

        // Creates pane to place label on
        with(table) {
            align(Align.topLeft)
            add(label)
                .padTop(3f)
                .padLeft(2f)
                .size(table.width, table.height)
                .row()
        }

        // Adds pane
        game.stage.addActor(pane)
        this.pane = pane
        this.label = label

        // Changes status. If just opened and event is not suppressed, fires event.
        val opened = status == Status.CLOSED
        status = Status.PROMPTING
        if(!suppressEvent && opened)
            game.fire(PromptOpenEvent())

        // Plays prompt sound
        playSound()

        // Sets justPrompted flag
        justPrompted = true
    }

    /**
     * Stops prompting and closes.
     * @param suppressEvent If true, will not emit a [PromptCloseEvent] upon closing.
     */
    @JvmOverloads fun close(suppressEvent: Boolean = false) {
        if(status == Status.CLOSED) return
        clearGraphics()
        status = Status.CLOSED
        if(!suppressEvent)
            game.fire(PromptCloseEvent())
    }

    internal fun load() {
        val paths: AssetPaths = game.assetPaths
        skin = game.assetManager.loadSync("${paths.skins}/$skinFile")
        val atlas: TextureAtlas = game.assetManager.loadSync("${paths.skins}/$atlasFile")
        atlas.textures.forEach { it.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest) }
        skin.addRegions(atlas)
    }

    internal fun unload() {
        val paths: AssetPaths = game.assetPaths
        game.assetManager.unload("${paths.skins}/$skinFile")
        game.assetManager.unload("${paths.skins}/$atlasFile")
    }

    /**
     * Runs internal queue.
     */
    internal fun run(deltaTime: Float) {
        queue.run(this, deltaTime)
        val label = label
        if(!justPrompted && label != null) {
            // Evil code
            if (game.controller.isPressed(advanceButton)) {
                val field = label.javaClass.getDeclaredField("textSpeed")
                field.isAccessible = true
                field.set(label, fastSpeed)
            }
            else if (game.controller.isReleased(advanceButton)) {
                val field = label.javaClass.getDeclaredField("textSpeed")
                field.isAccessible = true
                field.set(label, normalSpeed)
            }
        }
        justPrompted = false
    }

    private fun clearGraphics() {
        pane?.remove()
        this.pane = null
        this.label = null
    }

    /**
     * Plays promptor sound.
     */
    private fun playSound() {
        val selectSound = selectSound
        if(selectSound != null) {
            game.soundPlayer.playSound(selectSound)
        }
    }

    /**
     * Represents the status of the Promptor.
     */
    enum class Status {
        CLOSED,
        PROMPTING,
        PROMPTED
    }

    /**
     * Helpful [TypingListener] implementation that wraps an existing one while altering the state of the
     * existing [Promptor] when it finished prompting.
     */
    private inner class TypingListenerWrapper(val underlying: TypingListener) : TypingListener by underlying {
        override fun end() {
            status = Status.PROMPTED
            underlying.end()
        }
    }

    companion object {
        private val fastSpeed = 0.01f
        private val normalSpeed = 0.035f
    }
}