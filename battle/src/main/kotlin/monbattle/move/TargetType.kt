package monbattle.move

/**
 * Represents the type of target a move can select.
 */
enum class TargetType { SELECTED, ALL, FIELD }