package monmaker.sound

import monmaker.exceptions.MonmakerException

/**
 * @property file Sound file to play.
 * @property volume Volume value between 0 and 1.
 * @property blockSeconds Amount of time this sound must play before being allowed
 * to be played again. Attempts to play sound while blocking will do nothing.
 */
data class Sound @JvmOverloads constructor(val file: String, val volume: Float = 1f, val blockSeconds: Float = 0f) {
    init {
        if(volume < 0f || volume > 1f) throw MonmakerException("volume must be in range [0, 1]. Got ${volume}.")
    }
}