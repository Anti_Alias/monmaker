package monbattle.desktop

import com.badlogic.gdx.Graphics
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import monbattle.app.MonmakerConfig
import monbattle.exceptions.MonmakerException

class Lwjgl3MonmakerConfig(
    windowedWidth: Int,
    windowedHeight: Int,
    fullscreenDisplayMode: Graphics.DisplayMode
) : MonmakerConfig {

    val config = Lwjgl3ApplicationConfiguration()
    init {
        config.useVsync(true)
    }


    override var windowWidth: Int = windowedWidth
        set(value) {
            field = value
            config.setWindowedMode(value, windowHeight)
        }

    override var windowHeight: Int = windowedHeight
        set(value) {
            field = value
            config.setWindowedMode(windowWidth, field)
        }

    override var fullscreenMode: Graphics.DisplayMode = fullscreenDisplayMode
        set(value) {
            field = value
            config.setFullscreenMode(value)
        }

    override var fps: Int = 60
        set(value) {
            if(value <= 0f) throw MonmakerException("Invalid FPS $value.")
            field = value
        }
}
