package monmaker.grid

import monmaker.entity.Entity
import monmaker.event.Event
import java.util.*


/**
 * Represents a single Tile in a particular grid.
 * Inherits all tile-layer properties in a tiled map.
 * @property x Global tileX-coordinate of this tile.
 * @property y Global tileY-coordinate of this tile.
 * @property properties Tile properties this Tile owns.
 * @param grid Grid that holds this tile.
 */
class Tile internal constructor(
    val x: Int,
    val y: Int,
    internal val _properties: MutableMap<String, Any>,
    val grid: Grid,
    internal var listener: TileListener = StubTileListener
) {

    /**
     * Properties of this Tile.
     */
    val properties: Map<String, Any> = Collections.unmodifiableMap(_properties)

    /**
     * Check if property exists and is true.
     * If property exists, but is not a boolean, returns faluse.
     */
    fun isTrue(property: String): Boolean {
        val value: Any? = properties[property]
        return value is Boolean && value
    }

    /**
     * Determines if tile is solid or not.
     */
    val isSolid: Boolean get() = isTrue("solid")

    /**
     * Entities in this tile, if any.
     * Empty list if there are none.
     */
    val entities: List<Entity> = grid.entityPositions[GridPosition(x, y)] ?: emptyList()

    /**
     * Determines if Tile has any Entities in it.
     */
    val isOccupied: Boolean get() {
        val entities: List<Entity>? = grid.entityPositions[GridPosition(x, y)]
        return if(entities != null) entities.isNotEmpty() else false
    }

    /**
     * Asynchronously spawns an Entity on this this Tile.
     * @param entity Entity to spawn.
     * @return this tile.
     */
    fun spawn(entity: Entity): Tile {
        grid.world.spawnEntity(entity, x, y)
        return this
    }

    /**
     * Asynchronously spawns an Entity on this this Tile.
     * @param entity Entity to spawn.
     * @param then Callback function to invoke after entity is spawned.
     * @return this tile.
     */
    fun spawn(entity: Entity, then: ()->Unit): Tile {
        grid.world.spawnEntity(entity, x, y, then)
        return this
    }

    /**
     * Merges information about a tile.
     */
    fun merge(info: TileInfo) {
        _properties.putAll(info.properties)
        this.listener = info.listener
    }

    /**
     * Fires an [Event] into this [Tile].
     */
    fun fire(event: Event) {
        listener.on(event, this)
        entities.forEach { it.fire(event) }
    }

    override fun toString(): String = "Tile(tileX=$x, tileY=$y, properties=$properties)"
}