package monmaker.extensions

import monmaker.queue.*
import monmaker.entity.Direction
import monmaker.entity.Entity
import monmaker.entity.EntityAnimation
import monmaker.sound.Sound

/**
 * Asynchronously travels in a given path.
 * @param path Path to travel in.
 * IE: "NNEESSWWENSSSSNN" where each character in the path string is a cardinal direction. Not case sensitive.
 * @param durationSeconds How long it should take to travel in a single direction (not the entire path).
 * @return this Entity.
 */
@JvmOverloads fun CommandQueue<Entity>.travel(
    path: String,
    durationSeconds: Float = walkingDuration,
    bumpSound: Sound? = null
): CommandQueue<Entity> {
    for(pathChar: Char in path) {
        val dir = Direction.valueOf(pathChar.toString().toUpperCase())
        travel(dir, durationSeconds, bumpSound)
    }
    return this
}

/**
 * Asynchronously travels in a given direction.
 * @param direction Direction to travel in. If null, travels in place.
 * @param durationSeconds How long it should take to travel in that direction.
 * @return this Entity.
 */
@JvmOverloads fun CommandQueue<Entity>.travel(
    direction: Direction?,
    durationSeconds: Float = walkingDuration,
    bumpSound: Sound? = null
): CommandQueue<Entity> = add(TravelCommand(direction, bumpSound), durationSeconds)

/**
 * Asynchronously runs bump walk cycle.
 */
@JvmOverloads fun CommandQueue<Entity>.bump(
    durationSeconds: Float = bumpDuration,
    sound: Sound? = null
): CommandQueue<Entity> = add(BumpCommand(sound), durationSeconds)

/**
 * Asynchronously looks in a given direction.
 */
fun CommandQueue<Entity>.look(direction: Direction): CommandQueue<Entity> =
    add(LookCommand(direction), 0f)

/**
 * Asynchronously looks in a given direction.Direction to look in. Should be either "N", "S", "E", or "W".
 * @param direction
 * Not case sensitive.
 */
fun CommandQueue<Entity>.look(direction: String): CommandQueue<Entity> =
    look(Direction.valueOf(direction.toUpperCase()))

/**
 * Asynchronously sets the state of the entity.
 * This determines which [EntityAnimation] gets displayed.
 */
fun CommandQueue<Entity>.setState(state: String): CommandQueue<Entity> = add(SetStateCommand(state), 0f)

private const val bumpDuration: Float = 1/2f
private const val walkingDuration: Float = 16/60f
