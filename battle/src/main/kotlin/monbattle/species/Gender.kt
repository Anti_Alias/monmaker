package monbattle.species

import java.util.*

/**
 * Gender of a monster.
 */
enum class Gender {

    MALE, FEMALE, GENDERLESS;

    companion object {
        /**
         * @param genderRatio Gender ratio. Should be in range [0, 1].
         * 0 implies gender is always MALE.
         * 1 implies gender is always FEMALE.
         * @param random Source of randomness.
         * @return either [MALE] or [FEMALE] depending ont he gender ratio.
         */
        @JvmStatic
        fun random(genderRatio: Double, random: Random): Gender {
            val num: Double = random.nextDouble()
            return if (num < genderRatio) FEMALE else MALE
        }

        /**
         * @param ratio Gender ratio.
         * @return either [MALE] or [FEMALE] depending ont he gender ratio.
         */
        @JvmStatic
        fun random(ratio: Double): Gender = random(ratio, Random())
    }
}