package monmaker.entity

/**
 * Direction an entity can travel/face.
 * Represents north, south, east and west.
 */
enum class Direction {
    N, S, E, W;

    /**
     * Offset-x of direction.
     * Either -1, 0 or 1
     */
    val x: Int get() = when(this) {
        N -> 0
        S -> 0
        E -> 1
        W -> -1
    }

    /**
     * Offset-y of direction.
     * Either -1, 0 or 1
     */
    val y: Int get() = when(this) {
        N -> 1
        S -> -1
        E -> 0
        W -> 0
    }

    /**
     * Reverse of this direction.
     */
    val reverse: Direction get() = when(this) {
        N -> S
        S -> N
        E -> W
        W -> E
    }
}