package monmaker.sound

import monmaker.exceptions.MonmakerException

/**
 * Represents information about music to play.
 * @property file Music file to play.
 * @property volume Volume value between 0 and 1.
 * @property startSeconds Position in seconds to start in when playing music for the first time.
 * @property loopStartSeconds Position in seconds to loop if looping is enabled.
 * @property loop If true, music will loop.
 */
data class Music @JvmOverloads constructor(
    val file: String,
    val volume: Float = 0.4f,
    val startSeconds: Float = 0f,
    val loopStartSeconds: Float = 0f,
    val endSeconds: Float = Float.MAX_VALUE,
    val loop: Boolean = true
) {

    // Validates arguments.
    init {
        if(volume < 0f || volume > 1f) throw MonmakerException("volume must be in range [0, 1]. Got ${volume}.")
        if(startSeconds < 0f) throw MonmakerException("startSeconds must be >= 0. Got $startSeconds}.")
        if(loopStartSeconds < 0f) throw MonmakerException("loopStartSeconds must be >= 0. Got $loopStartSeconds}.")
    }
}