package monmaker.world

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Matrix4
import monmaker.entity.Entity
import monmaker.event.Event
import monmaker.event.TickEvent
import monmaker.exceptions.MonmakerException
import monmaker.generator.Generator
import monmaker.generator.createGenerator
import monmaker.grid.Grid
import monmaker.grid.MapInfo
import monmaker.logging.Logger
import monmaker.screen.Screen
import monmaker.script.Script
import monmaker.script.WorldScript
import monmaker.sound.Music
import java.util.*

private typealias GdxCamera = com.badlogic.gdx.graphics.Camera

/**
 * Represents a tile-based world in which entities reside.
 *n
 * @property grid Grid in which tiled-based logic resides.
 * @property music Music to play when player is in this World.
 * @param entityGenerator Object with methods in the format 'generate<identifier>' that return an [Entity].
 */
class World @JvmOverloads constructor(
    val grid: Grid,
    private val entityGenerator: Generator<Entity>,
    val music: Music? = null,
    private val onStart: ((World)->Unit)? = null
) : Screen() {

    /**
     * Alternate constructor that simplifies the addition of an EntityGenerator.
     */
    @JvmOverloads constructor (
        grid: Grid,
        entityGenerator: Any? = null,
        music: Music? = null,
        onStart: ((World)->Unit)? = null
    ) : this(grid, createGenerator<Entity>(entityGenerator), music, onStart)

    internal val entities: MutableSet<Entity> = mutableSetOf()
    private val scripts: MutableSet<Script> = mutableSetOf()
    private val actionQueue: Queue<()->Unit> = LinkedList()
    internal val projection: Matrix4 = Matrix4()

    /**
     * Camera used to compute projection matrix.
     */
    private val gdxCamera: GdxCamera = OrthographicCamera()

    /**
     * Camera that determines what is visible.
     */
    val camera: Camera = Camera(0f, 0f, this)

    /**
     * Acquires a script by id.
     */
    fun getScript(id: UUID): Script = scripts.first { it.id == id }

    /**
     * Acquires a script by id. If not found, returns null.
     */
    fun getScriptOrNull(id: UUID): Script? = scripts.firstOrNull { it.id == id }

    /**
     * Determines if this World contains the specified script.
     */
    fun containsScript(script: Script): Boolean = scripts.contains(script)

    /**
     * Determines if this World contains a script with the specified id.
     */
    fun containsScript(id: UUID): Boolean = scripts.any { it.id == id }

    /**
     * Sets viewport size.
     */
    override fun setViewport(width: Int, height: Int) {
        val gdxCamera = OrthographicCamera(width.toFloat(), height.toFloat())
        projection.set(gdxCamera.combined)
    }

    /**
     * Adds a [Script] to this World.
     */
    fun addWorldScript(script: WorldScript) {
        if(containsScript(script))
            throw MonmakerException("Attempted to add duplicate script '${script.javaClass.name}' to world.")
        script._world = this
        scripts.add(script)
        script.register()
        logger.log("Total scripts: ${scripts.size}.")
    }

    /**
     * Acquires an Entity by name.
     */
    fun getEntity(name: String): Entity {
        val entity: Entity? = entities.firstOrNull { it.name == name }
        return entity ?: throw MonmakerException("Could not find entity '$name' in world.")
    }

    /**
     * Acquires an Entity by name. If not found, returns null.
     */
    fun getEntityOrNull(name: String): Entity? = entities.firstOrNull { it.name == name }

    /**
     * Determines if this World contains the specified entity.
     */
    fun containsEntity(entity: Entity): Boolean = entities.contains(entity)

    /**
     * Determines if this World contains an entity with the specified name.
     */
    fun containsEntity(name: String): Boolean = entities.any { it.name == name }

    /**
     * Fires an event into this world. All non-running scripts that are configured to be notified for this type
     * of event will be notified.
     */
    override fun fire(event: Event) {
        for(script in scripts)
            script.on(event)
    }

    /**
     * Runs the World's game logic.
     * @param deltaTime Time since last frame in seconds.
     */
    override fun run(deltaTime: Float) {

        // Runs async functions
        runCallbacks()

        // Fires a tick event
        fire(TickEvent(deltaTime))

        // Updates entities
        for(entity in entities)
            entity.run(deltaTime)

        // Updates camera, and loads/unloads maps based on the position of it.
        camera.update()
        grid.sync()
    }

    /**
     * Renders the current grid.
     */
    override fun render() {

        // Computes projection matrix based on resolution size
        camera.computeTransformation()
        grid.computeView()

        // Renders layers underneath entities
        grid.renderUnderLayers()

        // Renders entities
        renderEntities()

        // Renders layers on top of entities
        grid.renderOverLayers()
    }

    /**
     * Asynchronously spawns an Entity.
     * @param entity Entity to spawn.
     * @param x Tile-tileX to spawn it at.
     * @param y Tile-tileY to spawn it at.
     */
    fun spawnEntity(entity: Entity, x: Int, y: Int) {
        actionQueue.add { spawnEntityNow(entity, x, y) }
    }


    /**
     * Asynchronously spawns an Entity, then invokes a callback function.
     * @param entity Entity to spawn.
     * @param x Tile-tileX to spawn it at.
     * @param y Tile-tileY to spawn it at.
     * @param then Callback function to invoke after entity is spawned.
     */
    fun spawnEntity(entity: Entity, x: Int, y: Int, then: ()->Unit) {
        actionQueue.add {
            spawnEntityNow(entity, x, y)
            then()
        }
    }

    /**
     * Asynchronously spawns an Entity at their spawn point, then invokes a callback function.
     * @param entityName Name of entity to spawn.
     * @param x Tile-tileX to spawn it at.
     * @param y Tile-tileY to spawn it at.
     */
    internal fun spawnEntity(entityName: String, x: Int, y: Int) {
        if(containsEntity(entityName)) {
            logger.debug("Tried to spawn Entity with name '$entityName' in world, but one was already spawned.")
            return
        }
        val entity: Entity? = entityGenerator.generate(entityName)
        if(entity != null) {
            if(entity.name != entityName) {
                throw MonmakerException("Generated entity should have had name '$entityName', but had name '${entity.name}'.")
            }
            spawnEntity(entity, x, y)
        }
    }

    /**
     * Asynchronously despawns an Entity.
     * @param entity Entity to remove.
     */
    fun aDespawnEntity(entity: Entity) {
        actionQueue.add { despawnEntityNow(entity) }
    }

    /**
     * Asynchronously despawns an Entity, then invokes a callback function.
     * @param entity Entity to remove.
     * @param then Callback function to invoke after entity is despawned.
     */
    fun aDespawnEntity(entity: Entity, then: ()->Unit) {
        actionQueue.add {
            despawnEntityNow(entity)
            then()
        }
    }

    /**
     * Asynchronously despawns an Entity.
     * @param entityName Name of entity to remove.
     */
    internal fun aDespawnEntity(entityName: String) {
        actionQueue.add {
            val entity: Entity? = entities.firstOrNull { it.name == entityName }
            if(entity != null)
                despawnEntityNow(entity)
        }
    }

    private fun spawnEntityNow(entity: Entity, x:Int, y:Int) {
        if(entity._world != null) throw MonmakerException(
            "Entity '${entity.name}' could not be spawned in world " +
            "because it already belonged to another one.'"
        )
        entities.add(entity)
        entity._world = this
        entity.load()
        entity.setLocation(x, y)
        for(script in entity.scripts) {
            this.scripts.add(script)
            script.register()
        }
        logger.log("Spawned entity '${entity.name}'")
        logger.log("Total entities '${entities.size}'")
        logger.log("Total scripts: ${scripts.size}.")
    }

    private fun despawnEntityNow(entity: Entity) {
        if(entity._world != this) throw MonmakerException(
            "Entity '${entity.name}' could not be despawned from world '${entity.name}' " +
            "because it did not belong to it."
        )
        entity.scripts.forEach { it.deregister() }
        entity.unload()
        entity._world = null
        scripts.removeAll(entity.scripts)
        entities.remove(entity)
        logger.log("Despawned entity '${entity.name}'.")
        logger.log("Total entities '${entities.size}'")
        logger.log("Total scripts: ${scripts.size}.")
    }

    /**
     * Renders entities in order of their Y-axis
     *
     * @param deltaTime Time duration since last frame in seconds.
     */
    private fun renderEntities() {
        val batch: Batch = game.renderers.spriteBatch
        batch.projectionMatrix.set(projection)
        batch.transformMatrix.set(camera.transformation)
        entities
            .sortedByDescending{ it.y }
            .forEach { it.render(game.renderers.spriteBatch) }
    }

    /**
     * Runs the callback functions for all next instances that are in the completed state.
     * Then, removes them.
     */
    private fun runCallbacks() {

        // Flushes action queue
        while(actionQueue.isNotEmpty()) {
            val action = actionQueue.remove()
            action()
        }
    }

    /**
     * Disposes of everything residing in this world.
     * This includes entities and grid information.
     */
    override fun unload() {
        grid.unload()
        for(entity in entities)
            entity.unload()
        logger.log("Unloaded world'.")
    }


    /**
     * Loads resources for this World
     */
    override fun load() {
        // Assigns grid to this World.
        grid.world = this

        // Focuses on starting map
        val startingMapInfo: MapInfo = grid.startingMapInfo
        camera.x = startingMapInfo.x * grid.tileWidth + camera.width/2
        camera.y = startingMapInfo.y * grid.tileHeight + camera.height/2
        grid.sync()

        // Plays music if available
        if(music != null) {
            game.soundPlayer.playMusic(music)
        }
        logger.log("Loaded world.")

        // Runs onStart callback.
        onStart?.invoke(this)
    }


    companion object {
        val logger = Logger.forClass<World>()
    }
}