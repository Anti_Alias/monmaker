package monmaker.queue

import com.rafaskoberg.gdx.typinglabel.TypingListener
import monmaker.event.PromptOpenEvent
import monmaker.event.PromptCloseEvent
import monmaker.exceptions.MonmakerException
import monmaker.game.Promptor

/**
 * Represents an action that executes in an [ActionQueue].
 */
interface Action<T> {

    /**
     * Flag that determines if the [Action] is done.
     */
    val isDone: Boolean

    /**
     * Runs first step of the command. This may set [isDone] to true.
     * @param target Target to execute on.
     */
    fun start(target: T) {}

    /**
     * Executes command.
     * @param target Target to execute on.
     * @param deltaTime Time in seconds that has elapsed since the last frame.
     */
    fun run(target: T, deltaTime: Float) {}
}

/**
 * Action for simply waiting.
 */
class WaitAction<T>(private val durationSeconds: Float) : Action<T> {
    private var secondsElapsed: Float = 0f
    init {
        if(durationSeconds < 0f)  MonmakerException("durationSeconds must be at least 0. Got $durationSeconds.")
    }
    override val isDone: Boolean get() = secondsElapsed >= durationSeconds
    override fun run(target: T, deltaTime: Float) {
        secondsElapsed += deltaTime
    }
}

/**
 * Action that prompts a message in a [Promptor].
 * @property message Message to prompt.
 * @param suppressEvent If true, will not emit a [PromptOpenEvent] upon opening.t
 * @property listener Optional listener for evens fired by the [Promptor].
 */
class PromptAction @JvmOverloads constructor(
    val message: String,
    val autoClose: Boolean = false,
    val suppressEvent: Boolean = false,
    val listener: TypingListener? = null
) : Action<Promptor> {
    private var started: Boolean = false
    private lateinit var promptor: Promptor
    override val isDone: Boolean get() =
        (autoClose || promptor.game.controller.isPressed(promptor.advanceButton)) &&
        started &&
        promptor.status != Promptor.Status.PROMPTING
    override fun start(target: Promptor) {
        target.prompt(message, suppressEvent, listener)
        this.promptor = target
        started = true
    }
}

/**
 * Action that closes a [Promptor].
 * @param suppressEvent If true, will not emit a [PromptCloseEvent] upon closing.
 */
class CloseAction(private val suppressEvent: Boolean) : Action<Promptor> {
    override var isDone: Boolean = false
    override fun start(target: Promptor) {
        target.close(suppressEvent)
        isDone = true
    }
}