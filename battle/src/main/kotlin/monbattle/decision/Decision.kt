package monbattle.decision

import monbattle.battle.Battle

/**
 * Performs a decision in battle.
 */
interface Decision {
    fun perform(battle: Battle)
}