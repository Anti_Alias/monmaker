package monbattle.type

/**
 * Represents one of the types of a monster.
 */
interface Type
object Normal : Type
object Fighting : Type
object Flying : Type
object Poison : Type
object Ground : Type
object Rock : Type
object Bug : Type
object Ghost : Type
object Steel : Type
object Fire : Type
object Water : Type
object Grass : Type
object Electric : Type
object Psychic : Type
object Ice : Type
object Dragon : Type
object Dark : Type
object Fairy : Type
object Empty : Type

interface TypeChart {
    /**
     * @return Efficacy of type1 vs type2.
     */
    fun efficacy(type1: Type, type2: Type): Double

    /**
     * @return a modified type chart.
     *
     * @param efficacies Vararg of type efficacy changes.
     */
    fun modified(vararg efficacies: Pair<Pair<Type, Type>, Double>): TypeChart =
        ModifiedTypeChart(this, efficacies.toMap())
}

/**
 * Type efficacy of generation 6.
 */
object GenSixTypeChart : TypeChart {
    private val values: Map<Pair<Type, Type>, Double> = mapOf(
        (Normal to Rock) to 0.5,
        (Normal to Ghost) to 0.0,
        (Normal to Steel) to 0.5,
        (Fire to Fire) to 0.5,
        (Fire to Water) to 0.5,
        (Fire to Grass) to 2.0,
        (Fire to Ice) to 2.0,
        (Fire to Bug) to 2.0,
        (Fire to Rock) to 0.5,
        (Fire to Dragon) to 0.5,
        (Fire to Steel) to 2.0,
        (Water to Fire) to 2.0,
        (Water to Water) to 0.5,
        (Water to Grass) to 0.5,
        (Water to Ground) to 2.0,
        (Water to Rock) to 2.0,
        (Water to Dragon) to 0.5,
        (Electric to Water) to 2.0,
        (Electric to Electric) to 0.5,
        (Electric to Grass) to 0.5,
        (Electric to Ground) to 0.0,
        (Electric to Flying) to 2.0,
        (Electric to Dragon) to 0.5,
        (Grass to Fire) to 0.5,
        (Grass to Water) to 2.0,
        (Grass to Grass) to 0.5,
        (Grass to Poison) to 0.5,
        (Grass to Ground) to 2.0,
        (Grass to Flying) to 0.5,
        (Grass to Bug) to 0.5,
        (Grass to Rock) to 2.0,
        (Grass to Dragon) to 0.5,
        (Grass to Steel) to 0.5,
        (Ice to Fire) to 0.5,
        (Ice to Water) to 0.5,
        (Ice to Grass) to 2.0,
        (Ice to Ice) to 0.5,
        (Ice to Ground) to 2.0,
        (Ice to Flying) to 2.0,
        (Ice to Dragon) to 2.0,
        (Ice to Steel) to 0.5,
        (Fighting to Normal) to 2.0,
        (Fighting to Ice) to 2.0,
        (Fighting to Poison) to 0.5,
        (Fighting to Flying) to 0.5,
        (Fighting to Psychic) to 0.5,
        (Fighting to Bug) to 0.5,
        (Fighting to Rock) to 2.0,
        (Fighting to Ghost) to 0.0,
        (Fighting to Dark) to 2.0,
        (Fighting to Steel) to 2.0,
        (Fighting to Fairy) to 0.5,
        (Poison to Grass) to 2.0,
        (Poison to Poison) to 0.5,
        (Poison to Ground) to 0.5,
        (Poison to Rock) to 0.5,
        (Poison to Ghost) to 0.5,
        (Poison to Steel) to 0.0,
        (Poison to Fairy) to 2.0,
        (Ground to Fire) to 2.0,
        (Ground to Electric) to 2.0,
        (Ground to Grass) to 0.5,
        (Ground to Poison) to 2.0,
        (Ground to Flying) to 0.0,
        (Ground to Bug) to 0.5,
        (Ground to Rock) to 2.0,
        (Ground to Steel) to 2.0,
        (Flying to Electric) to 0.5,
        (Flying to Grass) to 2.0,
        (Flying to Fighting) to 2.0,
        (Flying to Bug) to 2.0,
        (Flying to Rock) to 0.5,
        (Flying to Steel) to 0.5,
        (Psychic to Fighting) to 2.0,
        (Psychic to Poison) to 2.0,
        (Psychic to Psychic) to 0.5,
        (Psychic to Dark) to 0.0,
        (Psychic to Steel) to 0.5,
        (Bug to Fire) to 0.5,
        (Bug to Grass) to 2.0,
        (Bug to Fighting) to 0.5,
        (Bug to Poison) to 0.5,
        (Bug to Flying) to 0.5,
        (Bug to Psychic) to 2.0,
        (Bug to Ghost) to 0.5,
        (Bug to Dark) to 2.0,
        (Bug to Steel) to 0.5,
        (Bug to Fairy) to 0.5,
        (Rock to Fire) to 2.0,
        (Rock to Ice) to 2.0,
        (Rock to Fighting) to 0.5,
        (Rock to Ground) to 0.5,
        (Rock to Flying) to 2.0,
        (Rock to Bug) to 2.0,
        (Rock to Steel) to 0.5,
        (Ghost to Normal) to 0.0,
        (Ghost to Psychic) to 2.0,
        (Ghost to Ghost) to 2.0,
        (Ghost to Dark) to 0.5,
        (Dragon to Dragon) to 2.0,
        (Dragon to Steel) to 0.5,
        (Dragon to Fairy) to 0.0,
        (Dark to Fighting) to 0.5,
        (Dark to Psychic) to 2.0,
        (Dark to Ghost) to 2.0,
        (Dark to Dark) to 0.5,
        (Dark to Fairy) to 0.5,
        (Steel to Fire) to 0.5,
        (Steel to Water) to 0.5,
        (Steel to Electric) to 0.5,
        (Steel to Ice) to 2.0,
        (Steel to Rock) to 2.0,
        (Steel to Steel) to 0.5,
        (Steel to Fairy) to 2.0,
        (Fairy to Fire) to 0.5,
        (Fairy to Fighting) to 2.0,
        (Fairy to Poison) to 0.5,
        (Fairy to Dragon) to 2.0,
        (Fairy to Dark) to 2.0,
        (Fairy to Steel) to 0.5
    )
    override fun efficacy(type1: Type, type2: Type): Double = values.getOrElse(type1 to type2) { 1.0 }
}

/**
 * Represents a modification to a [TypeChart].
 */
class ModifiedTypeChart(
    val underlying: TypeChart,
    val modifications: Map<Pair<Type, Type>, Double>
) : TypeChart {
    override fun efficacy(type1: Type, type2: Type): Double {
        return modifications.getOrElse(type1 to type2) {
            underlying.efficacy(type1, type2)
        }
    }
}