package monbattle.event

import monbattle.battle.Battle
import monbattle.battle.Team

/**
 * Represents an event that occurred in a [Battle].
 */
interface Event {
    val message: String get() = "${javaClass.name} fired"
}

/**
 * Represents an event signifying that a turn in a [Battle] started.
 */
data class TurnStartedEvent(val number: Int) : Event

/**
 * Event that signifies that a decision was requested for a [Team].
 */
data class DecisionRequestedEvent(val teamName: String) : Event

/**
 * Event that signifies that a decision was made for a [Team].
 */
data class DecisionMadeEvent(val teamName: String) : Event

/**
 * Event that signifies that a decision was made by a [Team].
 */
data class DecisionPerformedEvent(val teamName: String) : Event