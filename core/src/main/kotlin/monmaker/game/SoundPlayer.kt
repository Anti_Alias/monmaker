package monmaker.game

import monmaker.extensions.loadSync
import monmaker.sound.Music
import monmaker.sound.Sound

private typealias GdxMusic = com.badlogic.gdx.audio.Music
private typealias GdxSound = com.badlogic.gdx.audio.Sound

/**
 * Represents an object responsible for loading/playing sounds.
 * @param game Game that holds this SoundPlayer.
 */
class SoundPlayer internal constructor(private val game: Game) {

    private var music: GdxMusic? = null
    private val loadedSounds: MutableMap<String, GdxSound> = mutableMapOf()
    private val soundTime: MutableMap<Sound, Float> = mutableMapOf()

    /**
     * Plays a given sound.
     * @param sound Sound to play
     * Subsequent attempts will do nothing until time has elapsed.
     */
    fun playSound(sound: Sound) {

        // Quits if sound is blocked
        val time: Float = soundTime[sound] ?: 0f
        if(time > 0f) return

        // Otherwise, play the sound.
        val paths: AssetPaths = game.assetPaths
        val fullPath = "${paths.sounds}/${sound.file}"
        val gdxSound: GdxSound = loadedSounds.getOrPut(fullPath) {
            game.assetManager.loadSync(fullPath)
        }
        soundTime.put(sound, sound.blockSeconds)
        gdxSound.play(sound.volume)
    }

    /**
     * Plays music.
     * @param music Music definition to use.
     */
    fun playMusic(music: Music) {

        // Stops current music
        stopMusic()

        // Loads music from file
        val assetPaths: AssetPaths = game.assetPaths
        val fullPath = "${assetPaths.music}/${music.file}"
        val gdxMusic: GdxMusic = game.assetManager.loadSync(fullPath)
        if(music.loop) gdxMusic.setOnCompletionListener {
            gdxMusic.play()
            gdxMusic.position = music.loopStartSeconds
        }
        gdxMusic.play()
        gdxMusic.volume = music.volume
        gdxMusic.position = music.startSeconds
        this.music = gdxMusic
    }

    /**
     * Stops current music that is playing.
     * If no music is playing, this method does nothing.
     */
    fun stopMusic() {
        val music = this.music
        if(music != null) {
            music.stop()
            music.dispose()
        }
    }

    /**
     * Updates the player
     */
    internal fun update(deltaTime: Float) {
        val entries = soundTime.entries.toList()
        for(entry in entries) {
            val newTime: Float = entry.value - deltaTime
            if(newTime <= 0f) soundTime.remove(entry.key)
            else soundTime[entry.key] = newTime
        }
    }
}