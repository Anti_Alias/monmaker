package monmaker.entity

import com.badlogic.gdx.graphics.g2d.Batch
import monmaker.event.SelectEvent
import monmaker.queue.CommandQueue
import monmaker.event.Event
import monmaker.exceptions.MonmakerException
import monmaker.world.World
import monmaker.grid.Grid
import monmaker.grid.Tile
import monmaker.logging.Logger
import monmaker.script.EntityScript
import monmaker.script.Script
import java.util.*
import kotlin.math.max
import kotlin.math.roundToInt

/**
 * Represents a single game object in a [World] confined to a [Grid].
 *
 * @property name Name of the entity.
 * @property states State of this Entity.
 * @property direction Direction the entity is facing. Defaults to [Direction.S].
 * @property animationMode Mode in which animations will play.
 * @property scripts Scripts to control this Entity.
 */
class Entity @JvmOverloads constructor(
    val name: String,
    val states: States,
    var direction: Direction = Direction.S,
    var animationMode: AnimationMode = AnimationMode.MOVEMENT,
    scripts: Collection<EntityScript> = emptyList()
) {

    internal var _world: World? = null
    internal val scripts: List<EntityScript> = consumeScripts(scripts)

    /**
     * Queue of commands that will be performed asynchronously on this Entity.
     */
    val queue: CommandQueue<Entity> = CommandQueue()

    /**
     * Travelling flag
     */
    var isTravelling: Boolean = false

    /**
     * Last foot to extend when walking/running. -1 means right and 1 means left.
     */
    internal var foot: Int = -1

    /**
     * X-coordinate of this Entity in tiles.
     */
    var tileX: Int = 0
        private set

    /**
     * Y-coordinate of this Entity in tiles.
     */
    var tileY: Int = 0
        private set

    /**
     * X-coordinate to draw entity.
     */
    var x: Float = 0f

    /**
     * Y-coordinate to draw entity.
     */
    var y: Float = 0f

    /**
     * T-value for animation frame.
     * When 0, animation is on first frame. When 1, animation is on last frame.
     */
    var t: Float = 0f
        private set(value) {
            if(value < 0f) throw MonmakerException("t value of entity cannot be < 0. Got $t.")
            if(value > 1f) throw MonmakerException("t value of entity cannot be > 1. Got $t.")
            field = value
        }

    /**
     * Tile that this Entity sits on.
     */
    val tile: Tile get() = world.grid.getTile(tileX, tileY)

    /**
     * The [Tile] to this [Entity] in the direction the [Entity] is facing.
     */
    val adjacentTile: Tile get() = getAdjacentTile(direction)

    /**
     * Gets the tile adjacent [Tile] to this [Entity].
     * @param direction [Direction] of the tile adjacent to this [Entity].
     * Defaults to the direction the [Entity] is facing in.
     */
    fun getAdjacentTile(direction: Direction): Tile =
        world.grid.getTile(tileX + direction.x, tileY + direction.y)

    /**
     * Causes this [Entity] to select the [Tile] in front of it.
     */
    fun selectAdjacentTile() {
        adjacentTile.fire(SelectEvent(this, direction))
    }

    /**
     * Sets animation mode of this entity.
     * @param animationMode AnimatinoMode to use.
     * @return this entity.
     */
    fun setAnimationMode(animationMode: AnimationMode): Entity {
        this.animationMode = animationMode
        return this
    }

    /**
     * Sets t value for animation if [animationMode] is set to [AnimationMode.MOVEMENT].
     * Otherwise, this function does nothing.
     * @parma t T value for this entity.
     * @return this entity.
     */
    fun setT(t: Float): Entity {
        if(animationMode == AnimationMode.MOVEMENT)
            this.t = t
        return this
    }

    /**
     * Sets the tile location of the Entity while syncing the animation's position.
     *
     * @param tileX X of tile desired.
     * @param tileY  Y of tile desired.
     * @param syncPosition If true, graphics will sync to position specified.
     *
     * @return this Entity.
     */
    @JvmOverloads fun setLocation(tileX: Int, tileY: Int, syncPosition: Boolean = true): Entity {

        // Syncs graphics with new position
        if(syncPosition && _world != null)
            syncAnimationPosition(tileX, tileY)

        // Done early if position is no different from current
        if(tileX == this.tileX && tileY == this.tileY) return this

        // Assigns new position and updates position in the grid if it has one
        _world?.grid?.updateEntityPosition(this, tileX, tileY)
        this.tileX = tileX
        this.tileY = tileY

        // Done
        return this
    }

    /**
     * World this Entity belongs to.
     */
    val world: World get() =
        _world ?: throw MonmakerException("Entity '$name' could not access its world because it did not belong to one.")

    /**
     * Acquires a script by id.
     */
    fun getScript(id: UUID): Script = scripts.first { it.id == id }

    /**
     * Acquires a script by id. If not found, returns null.
     */
    fun getScriptOrNull(id: UUID): Script? = scripts.firstOrNull { it.id == id }

    /**
     * Determines if this World contains the specified script.
     */
    fun containsScript(script: Script): Boolean = scripts.contains(script)

    /**
     * Determines if this World contains a script with the specified id.
     */
    fun containsScript(id: UUID): Boolean = scripts.any { it.id == id}

    /**
     * Fires an [Event] into this [Entity].
     * Sends it to all [Script] stored.
     */
    fun fire(event: Event) {
        scripts.forEach { it.on(event) }
    }

    /**
     * Runs the commands of this entity.
     */
    internal fun run(deltaTime: Float) {
        // Runs command queue
        queue.run(this, deltaTime)
    }

    /**
     * Renders this Entity.
     * Does nothing if animation is offscreen.
     * @param deltaTime Time duration since last frame in seconds.
     * @param batch Batch used for rendering entity.
     */
    internal fun render(batch: Batch) {
        val animation: EntityAnimation = states.currentAnimation
        if(animation.isOnScreen(x, y, world.camera)) {
            val newT: Float = max(0f, t-0.0000001f)             // Prevents odd bug in which the final frame of a walk animation appears when it should not.
            animation.render(newT, x.roundToInt().toFloat(), y.roundToInt().toFloat(), direction, batch)
        }
    }

    /**
     * Registers this Entity with a World.
     */
    internal fun load() {
        states.load(world)
        syncAnimationPosition(tileX, tileY)
        logger.log("Loaded entity '$name'.")
    }

    /**
     * Unregisters this Entity with its World.
     */
    internal fun unload() {
        states.unload(world)
        logger.log("Unloaded entity '$name'.")
    }

    /**
     * Syncs animation position with tile position.
     */
    private fun syncAnimationPosition(tileX: Int, tileY: Int) {
        val tileWidth: Int = world.grid.tileWidth
        val tileHeight: Int = world.grid.tileHeight
        x = (tileX*tileWidth + tileWidth/2).toFloat()
        y = (tileY*tileHeight).toFloat()
    }


    private fun consumeScripts(scripts: Collection<EntityScript>): List<EntityScript> {
        val result = mutableListOf<EntityScript>()
        for(script in scripts) {
            if(script._entity != null) throw MonmakerException(
                "Attempted to add script '${script.javaClass.name}' to entity '$name', " +
                "but it was already stored in entity ${script.entity.name}."
            )
            if(result.contains(script))
                throw MonmakerException("Attempted to add duplicate script '${script.javaClass.name}' to entity '$name'.")
            result.add(script)
            script._entity = this
        }
        return result
    }

    companion object {
        private val logger = Logger.forClass<Entity>()
    }
}