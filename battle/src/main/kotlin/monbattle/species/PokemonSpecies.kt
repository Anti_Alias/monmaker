package monbattle.species

import monbattle.type.*

object PokemonSpecies {
    val BULBASAUR = Species(
        name = "bulbasaur",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 45,
            attack = 49,
            defense = 49,
            specialAttack = 65,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val IVYSAUR = Species(
        name = "ivysaur",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 62,
            defense = 63,
            specialAttack = 80,
            specialDefense = 80,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val VENUSAUR = Species(
        name = "venusaur",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 80,
            attack = 82,
            defense = 83,
            specialAttack = 100,
            specialDefense = 100,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val CHARMANDER = Species(
        name = "charmander",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 39,
            attack = 52,
            defense = 43,
            specialAttack = 60,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val CHARMELEON = Species(
        name = "charmeleon",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 58,
            attack = 64,
            defense = 58,
            specialAttack = 80,
            specialDefense = 65,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val CHARIZARD = Species(
        name = "charizard",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 78,
            attack = 84,
            defense = 78,
            specialAttack = 109,
            specialDefense = 85,
            speed = 100
        ),
        genderRatio = 0.125
    )
    val SQUIRTLE = Species(
        name = "squirtle",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 44,
            attack = 48,
            defense = 65,
            specialAttack = 50,
            specialDefense = 64,
            speed = 43
        ),
        genderRatio = 0.125
    )
    val WARTORTLE = Species(
        name = "wartortle",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 59,
            attack = 63,
            defense = 80,
            specialAttack = 65,
            specialDefense = 80,
            speed = 58
        ),
        genderRatio = 0.125
    )
    val BLASTOISE = Species(
        name = "blastoise",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 79,
            attack = 83,
            defense = 100,
            specialAttack = 85,
            specialDefense = 105,
            speed = 78
        ),
        genderRatio = 0.125
    )
    val CATERPIE = Species(
        name = "caterpie",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 30,
            defense = 35,
            specialAttack = 20,
            specialDefense = 20,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val METAPOD = Species(
        name = "metapod",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 20,
            defense = 55,
            specialAttack = 25,
            specialDefense = 25,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val BUTTERFREE = Species(
        name = "butterfree",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 45,
            defense = 50,
            specialAttack = 90,
            specialDefense = 80,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val WEEDLE = Species(
        name = "weedle",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 40,
            attack = 35,
            defense = 30,
            specialAttack = 20,
            specialDefense = 20,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val KAKUNA = Species(
        name = "kakuna",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 45,
            attack = 25,
            defense = 50,
            specialAttack = 25,
            specialDefense = 25,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val BEEDRILL = Species(
        name = "beedrill",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 40,
            specialAttack = 45,
            specialDefense = 80,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val PIDGEY = Species(
        name = "pidgey",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 40,
            specialAttack = 35,
            specialDefense = 35,
            speed = 56
        ),
        genderRatio = 0.5
    )
    val PIDGEOTTO = Species(
        name = "pidgeotto",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 63,
            attack = 60,
            defense = 55,
            specialAttack = 50,
            specialDefense = 50,
            speed = 71
        ),
        genderRatio = 0.5
    )
    val PIDGEOT = Species(
        name = "pidgeot",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 83,
            attack = 80,
            defense = 75,
            specialAttack = 70,
            specialDefense = 70,
            speed = 101
        ),
        genderRatio = 0.5
    )
    val RATTATA = Species(
        name = "rattata",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 56,
            defense = 35,
            specialAttack = 25,
            specialDefense = 35,
            speed = 72
        ),
        genderRatio = 0.5
    )
    val RATICATE = Species(
        name = "raticate",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 81,
            defense = 60,
            specialAttack = 50,
            specialDefense = 70,
            speed = 97
        ),
        genderRatio = 0.5
    )
    val SPEAROW = Species(
        name = "spearow",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 60,
            defense = 30,
            specialAttack = 31,
            specialDefense = 31,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val FEAROW = Species(
        name = "fearow",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 65,
            specialAttack = 61,
            specialDefense = 61,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val EKANS = Species(
        name = "ekans",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 60,
            defense = 44,
            specialAttack = 40,
            specialDefense = 54,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val ARBOK = Species(
        name = "arbok",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 95,
            defense = 69,
            specialAttack = 65,
            specialDefense = 79,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val PIKACHU = Species(
        name = "pikachu",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 55,
            defense = 40,
            specialAttack = 50,
            specialDefense = 50,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val RAICHU = Species(
        name = "raichu",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 90,
            defense = 55,
            specialAttack = 90,
            specialDefense = 80,
            speed = 110
        ),
        genderRatio = 0.5
    )
    val SANDSHREW = Species(
        name = "sandshrew",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 75,
            defense = 85,
            specialAttack = 20,
            specialDefense = 30,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val SANDSLASH = Species(
        name = "sandslash",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 100,
            defense = 110,
            specialAttack = 45,
            specialDefense = 55,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val NIDORAN_F = Species(
        name = "nidoran-f",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 47,
            defense = 52,
            specialAttack = 40,
            specialDefense = 40,
            speed = 41
        ),
        genderRatio = 1.0
    )
    val NIDORINA = Species(
        name = "nidorina",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 62,
            defense = 67,
            specialAttack = 55,
            specialDefense = 55,
            speed = 56
        ),
        genderRatio = 1.0
    )
    val NIDOQUEEN = Species(
        name = "nidoqueen",
        type1 = Poison,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 90,
            attack = 92,
            defense = 87,
            specialAttack = 75,
            specialDefense = 85,
            speed = 76
        ),
        genderRatio = 1.0
    )
    val NIDORAN_M = Species(
        name = "nidoran-m",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 46,
            attack = 57,
            defense = 40,
            specialAttack = 40,
            specialDefense = 40,
            speed = 50
        ),
        genderRatio = 0.0
    )
    val NIDORINO = Species(
        name = "nidorino",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 61,
            attack = 72,
            defense = 57,
            specialAttack = 55,
            specialDefense = 55,
            speed = 65
        ),
        genderRatio = 0.0
    )
    val NIDOKING = Species(
        name = "nidoking",
        type1 = Poison,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 81,
            attack = 102,
            defense = 77,
            specialAttack = 85,
            specialDefense = 75,
            speed = 85
        ),
        genderRatio = 0.0
    )
    val CLEFAIRY = Species(
        name = "clefairy",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 45,
            defense = 48,
            specialAttack = 60,
            specialDefense = 65,
            speed = 35
        ),
        genderRatio = 0.75
    )
    val CLEFABLE = Species(
        name = "clefable",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 70,
            defense = 73,
            specialAttack = 95,
            specialDefense = 90,
            speed = 60
        ),
        genderRatio = 0.75
    )
    val VULPIX = Species(
        name = "vulpix",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 38,
            attack = 41,
            defense = 40,
            specialAttack = 50,
            specialDefense = 65,
            speed = 65
        ),
        genderRatio = 0.75
    )
    val NINETALES = Species(
        name = "ninetales",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 73,
            attack = 76,
            defense = 75,
            specialAttack = 81,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = 0.75
    )
    val JIGGLYPUFF = Species(
        name = "jigglypuff",
        type1 = Normal,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 115,
            attack = 45,
            defense = 20,
            specialAttack = 45,
            specialDefense = 25,
            speed = 20
        ),
        genderRatio = 0.75
    )
    val WIGGLYTUFF = Species(
        name = "wigglytuff",
        type1 = Normal,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 140,
            attack = 70,
            defense = 45,
            specialAttack = 85,
            specialDefense = 50,
            speed = 45
        ),
        genderRatio = 0.75
    )
    val ZUBAT = Species(
        name = "zubat",
        type1 = Poison,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 35,
            specialAttack = 30,
            specialDefense = 40,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val GOLBAT = Species(
        name = "golbat",
        type1 = Poison,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 80,
            defense = 70,
            specialAttack = 65,
            specialDefense = 75,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val ODDISH = Species(
        name = "oddish",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 45,
            attack = 50,
            defense = 55,
            specialAttack = 75,
            specialDefense = 65,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val GLOOM = Species(
        name = "gloom",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 65,
            defense = 70,
            specialAttack = 85,
            specialDefense = 75,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val VILEPLUME = Species(
        name = "vileplume",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 75,
            attack = 80,
            defense = 85,
            specialAttack = 110,
            specialDefense = 90,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val PARAS = Species(
        name = "paras",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 35,
            attack = 70,
            defense = 55,
            specialAttack = 45,
            specialDefense = 55,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val PARASECT = Species(
        name = "parasect",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 60,
            attack = 95,
            defense = 80,
            specialAttack = 60,
            specialDefense = 80,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val VENONAT = Species(
        name = "venonat",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 55,
            defense = 50,
            specialAttack = 40,
            specialDefense = 55,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val VENOMOTH = Species(
        name = "venomoth",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 70,
            attack = 65,
            defense = 60,
            specialAttack = 90,
            specialDefense = 75,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val DIGLETT = Species(
        name = "diglett",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 10,
            attack = 55,
            defense = 25,
            specialAttack = 35,
            specialDefense = 45,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val DUGTRIO = Species(
        name = "dugtrio",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 100,
            defense = 50,
            specialAttack = 50,
            specialDefense = 70,
            speed = 120
        ),
        genderRatio = 0.5
    )
    val MEOWTH = Species(
        name = "meowth",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 35,
            specialAttack = 40,
            specialDefense = 40,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val PERSIAN = Species(
        name = "persian",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 70,
            defense = 60,
            specialAttack = 65,
            specialDefense = 65,
            speed = 115
        ),
        genderRatio = 0.5
    )
    val PSYDUCK = Species(
        name = "psyduck",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 52,
            defense = 48,
            specialAttack = 65,
            specialDefense = 50,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val GOLDUCK = Species(
        name = "golduck",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 82,
            defense = 78,
            specialAttack = 95,
            specialDefense = 80,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val MANKEY = Species(
        name = "mankey",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 80,
            defense = 35,
            specialAttack = 35,
            specialDefense = 45,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val PRIMEAPE = Species(
        name = "primeape",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 105,
            defense = 60,
            specialAttack = 60,
            specialDefense = 70,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val GROWLITHE = Species(
        name = "growlithe",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 70,
            defense = 45,
            specialAttack = 70,
            specialDefense = 50,
            speed = 60
        ),
        genderRatio = 0.25
    )
    val ARCANINE = Species(
        name = "arcanine",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 110,
            defense = 80,
            specialAttack = 100,
            specialDefense = 80,
            speed = 95
        ),
        genderRatio = 0.25
    )
    val POLIWAG = Species(
        name = "poliwag",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 50,
            defense = 40,
            specialAttack = 40,
            specialDefense = 40,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val POLIWHIRL = Species(
        name = "poliwhirl",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 65,
            defense = 65,
            specialAttack = 50,
            specialDefense = 50,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val POLIWRATH = Species(
        name = "poliwrath",
        type1 = Water,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 90,
            attack = 95,
            defense = 95,
            specialAttack = 70,
            specialDefense = 90,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val ABRA = Species(
        name = "abra",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 25,
            attack = 20,
            defense = 15,
            specialAttack = 105,
            specialDefense = 55,
            speed = 90
        ),
        genderRatio = 0.25
    )
    val KADABRA = Species(
        name = "kadabra",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 35,
            defense = 30,
            specialAttack = 120,
            specialDefense = 70,
            speed = 105
        ),
        genderRatio = 0.25
    )
    val ALAKAZAM = Species(
        name = "alakazam",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 50,
            defense = 45,
            specialAttack = 135,
            specialDefense = 95,
            speed = 120
        ),
        genderRatio = 0.25
    )
    val MACHOP = Species(
        name = "machop",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 80,
            defense = 50,
            specialAttack = 35,
            specialDefense = 35,
            speed = 35
        ),
        genderRatio = 0.25
    )
    val MACHOKE = Species(
        name = "machoke",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 100,
            defense = 70,
            specialAttack = 50,
            specialDefense = 60,
            speed = 45
        ),
        genderRatio = 0.25
    )
    val MACHAMP = Species(
        name = "machamp",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 130,
            defense = 80,
            specialAttack = 65,
            specialDefense = 85,
            speed = 55
        ),
        genderRatio = 0.25
    )
    val BELLSPROUT = Species(
        name = "bellsprout",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 50,
            attack = 75,
            defense = 35,
            specialAttack = 70,
            specialDefense = 30,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val WEEPINBELL = Species(
        name = "weepinbell",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 50,
            specialAttack = 85,
            specialDefense = 45,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val VICTREEBEL = Species(
        name = "victreebel",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 80,
            attack = 105,
            defense = 65,
            specialAttack = 100,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val TENTACOOL = Species(
        name = "tentacool",
        type1 = Water,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 40,
            attack = 40,
            defense = 35,
            specialAttack = 50,
            specialDefense = 100,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val TENTACRUEL = Species(
        name = "tentacruel",
        type1 = Water,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 80,
            attack = 70,
            defense = 65,
            specialAttack = 80,
            specialDefense = 120,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val GEODUDE = Species(
        name = "geodude",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 40,
            attack = 80,
            defense = 100,
            specialAttack = 30,
            specialDefense = 30,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val GRAVELER = Species(
        name = "graveler",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 55,
            attack = 95,
            defense = 115,
            specialAttack = 45,
            specialDefense = 45,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val GOLEM = Species(
        name = "golem",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 80,
            attack = 120,
            defense = 130,
            specialAttack = 55,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val PONYTA = Species(
        name = "ponyta",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 85,
            defense = 55,
            specialAttack = 65,
            specialDefense = 65,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val RAPIDASH = Species(
        name = "rapidash",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 100,
            defense = 70,
            specialAttack = 80,
            specialDefense = 80,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val SLOWPOKE = Species(
        name = "slowpoke",
        type1 = Water,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 90,
            attack = 65,
            defense = 65,
            specialAttack = 40,
            specialDefense = 40,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val SLOWBRO = Species(
        name = "slowbro",
        type1 = Water,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 95,
            attack = 75,
            defense = 110,
            specialAttack = 100,
            specialDefense = 80,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val MAGNEMITE = Species(
        name = "magnemite",
        type1 = Electric,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 25,
            attack = 35,
            defense = 70,
            specialAttack = 95,
            specialDefense = 55,
            speed = 45
        ),
        genderRatio = null
    )
    val MAGNETON = Species(
        name = "magneton",
        type1 = Electric,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 50,
            attack = 60,
            defense = 95,
            specialAttack = 120,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = null
    )
    val FARFETCHD = Species(
        name = "farfetchd",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 52,
            attack = 90,
            defense = 55,
            specialAttack = 58,
            specialDefense = 62,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val DODUO = Species(
        name = "doduo",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 35,
            attack = 85,
            defense = 45,
            specialAttack = 35,
            specialDefense = 35,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val DODRIO = Species(
        name = "dodrio",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 110,
            defense = 70,
            specialAttack = 60,
            specialDefense = 60,
            speed = 110
        ),
        genderRatio = 0.5
    )
    val SEEL = Species(
        name = "seel",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 45,
            defense = 55,
            specialAttack = 45,
            specialDefense = 70,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val DEWGONG = Species(
        name = "dewgong",
        type1 = Water,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 90,
            attack = 70,
            defense = 80,
            specialAttack = 70,
            specialDefense = 95,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val GRIMER = Species(
        name = "grimer",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 80,
            defense = 50,
            specialAttack = 40,
            specialDefense = 50,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val MUK = Species(
        name = "muk",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 105,
            attack = 105,
            defense = 75,
            specialAttack = 65,
            specialDefense = 100,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SHELLDER = Species(
        name = "shellder",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 65,
            defense = 100,
            specialAttack = 45,
            specialDefense = 25,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val CLOYSTER = Species(
        name = "cloyster",
        type1 = Water,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 50,
            attack = 95,
            defense = 180,
            specialAttack = 85,
            specialDefense = 45,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val GASTLY = Species(
        name = "gastly",
        type1 = Ghost,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 30,
            attack = 35,
            defense = 30,
            specialAttack = 100,
            specialDefense = 35,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val HAUNTER = Species(
        name = "haunter",
        type1 = Ghost,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 45,
            attack = 50,
            defense = 45,
            specialAttack = 115,
            specialDefense = 55,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val GENGAR = Species(
        name = "gengar",
        type1 = Ghost,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 65,
            defense = 60,
            specialAttack = 130,
            specialDefense = 75,
            speed = 110
        ),
        genderRatio = 0.5
    )
    val ONIX = Species(
        name = "onix",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 35,
            attack = 45,
            defense = 160,
            specialAttack = 30,
            specialDefense = 45,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val DROWZEE = Species(
        name = "drowzee",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 48,
            defense = 45,
            specialAttack = 43,
            specialDefense = 90,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val HYPNO = Species(
        name = "hypno",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 73,
            defense = 70,
            specialAttack = 73,
            specialDefense = 115,
            speed = 67
        ),
        genderRatio = 0.5
    )
    val KRABBY = Species(
        name = "krabby",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 105,
            defense = 90,
            specialAttack = 25,
            specialDefense = 25,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val KINGLER = Species(
        name = "kingler",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 130,
            defense = 115,
            specialAttack = 50,
            specialDefense = 50,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val VOLTORB = Species(
        name = "voltorb",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 50,
            specialAttack = 55,
            specialDefense = 55,
            speed = 100
        ),
        genderRatio = null
    )
    val ELECTRODE = Species(
        name = "electrode",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 70,
            specialAttack = 80,
            specialDefense = 80,
            speed = 150
        ),
        genderRatio = null
    )
    val EXEGGCUTE = Species(
        name = "exeggcute",
        type1 = Grass,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 60,
            attack = 40,
            defense = 80,
            specialAttack = 60,
            specialDefense = 45,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val EXEGGUTOR = Species(
        name = "exeggutor",
        type1 = Grass,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 95,
            attack = 95,
            defense = 85,
            specialAttack = 125,
            specialDefense = 75,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val CUBONE = Species(
        name = "cubone",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 95,
            specialAttack = 40,
            specialDefense = 50,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val MAROWAK = Species(
        name = "marowak",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 80,
            defense = 110,
            specialAttack = 50,
            specialDefense = 80,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val HITMONLEE = Species(
        name = "hitmonlee",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 120,
            defense = 53,
            specialAttack = 35,
            specialDefense = 110,
            speed = 87
        ),
        genderRatio = 0.0
    )
    val HITMONCHAN = Species(
        name = "hitmonchan",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 105,
            defense = 79,
            specialAttack = 35,
            specialDefense = 110,
            speed = 76
        ),
        genderRatio = 0.0
    )
    val LICKITUNG = Species(
        name = "lickitung",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 55,
            defense = 75,
            specialAttack = 60,
            specialDefense = 75,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val KOFFING = Species(
        name = "koffing",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 65,
            defense = 95,
            specialAttack = 60,
            specialDefense = 45,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val WEEZING = Species(
        name = "weezing",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 120,
            specialAttack = 85,
            specialDefense = 70,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val RHYHORN = Species(
        name = "rhyhorn",
        type1 = Ground,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 80,
            attack = 85,
            defense = 95,
            specialAttack = 30,
            specialDefense = 30,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val RHYDON = Species(
        name = "rhydon",
        type1 = Ground,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 105,
            attack = 130,
            defense = 120,
            specialAttack = 45,
            specialDefense = 45,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val CHANSEY = Species(
        name = "chansey",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 250,
            attack = 5,
            defense = 5,
            specialAttack = 35,
            specialDefense = 105,
            speed = 50
        ),
        genderRatio = 1.0
    )
    val TANGELA = Species(
        name = "tangela",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 55,
            defense = 115,
            specialAttack = 100,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val KANGASKHAN = Species(
        name = "kangaskhan",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 105,
            attack = 95,
            defense = 80,
            specialAttack = 40,
            specialDefense = 80,
            speed = 90
        ),
        genderRatio = 1.0
    )
    val HORSEA = Species(
        name = "horsea",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 40,
            defense = 70,
            specialAttack = 70,
            specialDefense = 25,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val SEADRA = Species(
        name = "seadra",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 65,
            defense = 95,
            specialAttack = 95,
            specialDefense = 45,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val GOLDEEN = Species(
        name = "goldeen",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 67,
            defense = 60,
            specialAttack = 35,
            specialDefense = 50,
            speed = 63
        ),
        genderRatio = 0.5
    )
    val SEAKING = Species(
        name = "seaking",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 92,
            defense = 65,
            specialAttack = 65,
            specialDefense = 80,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val STARYU = Species(
        name = "staryu",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 45,
            defense = 55,
            specialAttack = 70,
            specialDefense = 55,
            speed = 85
        ),
        genderRatio = null
    )
    val STARMIE = Species(
        name = "starmie",
        type1 = Water,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 60,
            attack = 75,
            defense = 85,
            specialAttack = 100,
            specialDefense = 85,
            speed = 115
        ),
        genderRatio = null
    )
    val MR_MIME = Species(
        name = "mr-mime",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 65,
            specialAttack = 100,
            specialDefense = 120,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val SCYTHER = Species(
        name = "scyther",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 110,
            defense = 80,
            specialAttack = 55,
            specialDefense = 80,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val JYNX = Species(
        name = "jynx",
        type1 = Ice,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 65,
            attack = 50,
            defense = 35,
            specialAttack = 115,
            specialDefense = 95,
            speed = 95
        ),
        genderRatio = 1.0
    )
    val ELECTABUZZ = Species(
        name = "electabuzz",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 83,
            defense = 57,
            specialAttack = 95,
            specialDefense = 85,
            speed = 105
        ),
        genderRatio = 0.25
    )
    val MAGMAR = Species(
        name = "magmar",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 95,
            defense = 57,
            specialAttack = 100,
            specialDefense = 85,
            speed = 93
        ),
        genderRatio = 0.25
    )
    val PINSIR = Species(
        name = "pinsir",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 125,
            defense = 100,
            specialAttack = 55,
            specialDefense = 70,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val TAUROS = Species(
        name = "tauros",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 100,
            defense = 95,
            specialAttack = 40,
            specialDefense = 70,
            speed = 110
        ),
        genderRatio = 0.0
    )
    val MAGIKARP = Species(
        name = "magikarp",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 20,
            attack = 10,
            defense = 55,
            specialAttack = 15,
            specialDefense = 20,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val GYARADOS = Species(
        name = "gyarados",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 95,
            attack = 125,
            defense = 79,
            specialAttack = 60,
            specialDefense = 100,
            speed = 81
        ),
        genderRatio = 0.5
    )
    val LAPRAS = Species(
        name = "lapras",
        type1 = Water,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 130,
            attack = 85,
            defense = 80,
            specialAttack = 85,
            specialDefense = 95,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val DITTO = Species(
        name = "ditto",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 48,
            attack = 48,
            defense = 48,
            specialAttack = 48,
            specialDefense = 48,
            speed = 48
        ),
        genderRatio = null
    )
    val EEVEE = Species(
        name = "eevee",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 55,
            defense = 50,
            specialAttack = 45,
            specialDefense = 65,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val VAPOREON = Species(
        name = "vaporeon",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 130,
            attack = 65,
            defense = 60,
            specialAttack = 110,
            specialDefense = 95,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val JOLTEON = Species(
        name = "jolteon",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 65,
            defense = 60,
            specialAttack = 110,
            specialDefense = 95,
            speed = 130
        ),
        genderRatio = 0.125
    )
    val FLAREON = Species(
        name = "flareon",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 130,
            defense = 60,
            specialAttack = 95,
            specialDefense = 110,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val PORYGON = Species(
        name = "porygon",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 60,
            defense = 70,
            specialAttack = 85,
            specialDefense = 75,
            speed = 40
        ),
        genderRatio = null
    )
    val OMANYTE = Species(
        name = "omanyte",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 35,
            attack = 40,
            defense = 100,
            specialAttack = 90,
            specialDefense = 55,
            speed = 35
        ),
        genderRatio = 0.125
    )
    val OMASTAR = Species(
        name = "omastar",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 70,
            attack = 60,
            defense = 125,
            specialAttack = 115,
            specialDefense = 70,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val KABUTO = Species(
        name = "kabuto",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 30,
            attack = 80,
            defense = 90,
            specialAttack = 55,
            specialDefense = 45,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val KABUTOPS = Species(
        name = "kabutops",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 60,
            attack = 115,
            defense = 105,
            specialAttack = 65,
            specialDefense = 70,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val AERODACTYL = Species(
        name = "aerodactyl",
        type1 = Rock,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 80,
            attack = 105,
            defense = 65,
            specialAttack = 60,
            specialDefense = 75,
            speed = 130
        ),
        genderRatio = 0.125
    )
    val SNORLAX = Species(
        name = "snorlax",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 160,
            attack = 110,
            defense = 65,
            specialAttack = 65,
            specialDefense = 110,
            speed = 30
        ),
        genderRatio = 0.125
    )
    val ARTICUNO = Species(
        name = "articuno",
        type1 = Ice,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 90,
            attack = 85,
            defense = 100,
            specialAttack = 95,
            specialDefense = 125,
            speed = 85
        ),
        genderRatio = null
    )
    val ZAPDOS = Species(
        name = "zapdos",
        type1 = Electric,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 90,
            attack = 90,
            defense = 85,
            specialAttack = 125,
            specialDefense = 90,
            speed = 100
        ),
        genderRatio = null
    )
    val MOLTRES = Species(
        name = "moltres",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 90,
            attack = 100,
            defense = 90,
            specialAttack = 125,
            specialDefense = 85,
            speed = 90
        ),
        genderRatio = null
    )
    val DRATINI = Species(
        name = "dratini",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 41,
            attack = 64,
            defense = 45,
            specialAttack = 50,
            specialDefense = 50,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val DRAGONAIR = Species(
        name = "dragonair",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 61,
            attack = 84,
            defense = 65,
            specialAttack = 70,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val DRAGONITE = Species(
        name = "dragonite",
        type1 = Dragon,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 91,
            attack = 134,
            defense = 95,
            specialAttack = 100,
            specialDefense = 100,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val MEWTWO = Species(
        name = "mewtwo",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 106,
            attack = 110,
            defense = 90,
            specialAttack = 154,
            specialDefense = 90,
            speed = 130
        ),
        genderRatio = null
    )
    val MEW = Species(
        name = "mew",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val CHIKORITA = Species(
        name = "chikorita",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 49,
            defense = 65,
            specialAttack = 49,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val BAYLEEF = Species(
        name = "bayleef",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 62,
            defense = 80,
            specialAttack = 63,
            specialDefense = 80,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val MEGANIUM = Species(
        name = "meganium",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 82,
            defense = 100,
            specialAttack = 83,
            specialDefense = 100,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val CYNDAQUIL = Species(
        name = "cyndaquil",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 39,
            attack = 52,
            defense = 43,
            specialAttack = 60,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val QUILAVA = Species(
        name = "quilava",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 58,
            attack = 64,
            defense = 58,
            specialAttack = 80,
            specialDefense = 65,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val TYPHLOSION = Species(
        name = "typhlosion",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 78,
            attack = 84,
            defense = 78,
            specialAttack = 109,
            specialDefense = 85,
            speed = 100
        ),
        genderRatio = 0.125
    )
    val TOTODILE = Species(
        name = "totodile",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 65,
            defense = 64,
            specialAttack = 44,
            specialDefense = 48,
            speed = 43
        ),
        genderRatio = 0.125
    )
    val CROCONAW = Species(
        name = "croconaw",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 80,
            defense = 80,
            specialAttack = 59,
            specialDefense = 63,
            speed = 58
        ),
        genderRatio = 0.125
    )
    val FERALIGATR = Species(
        name = "feraligatr",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 105,
            defense = 100,
            specialAttack = 79,
            specialDefense = 83,
            speed = 78
        ),
        genderRatio = 0.125
    )
    val SENTRET = Species(
        name = "sentret",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 46,
            defense = 34,
            specialAttack = 35,
            specialDefense = 45,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val FURRET = Species(
        name = "furret",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 76,
            defense = 64,
            specialAttack = 45,
            specialDefense = 55,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val HOOTHOOT = Species(
        name = "hoothoot",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 30,
            defense = 30,
            specialAttack = 36,
            specialDefense = 56,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val NOCTOWL = Species(
        name = "noctowl",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 100,
            attack = 50,
            defense = 50,
            specialAttack = 86,
            specialDefense = 96,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val LEDYBA = Species(
        name = "ledyba",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 20,
            defense = 30,
            specialAttack = 40,
            specialDefense = 80,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val LEDIAN = Species(
        name = "ledian",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 35,
            defense = 50,
            specialAttack = 55,
            specialDefense = 110,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SPINARAK = Species(
        name = "spinarak",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 40,
            attack = 60,
            defense = 40,
            specialAttack = 40,
            specialDefense = 40,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val ARIADOS = Species(
        name = "ariados",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 70,
            attack = 90,
            defense = 70,
            specialAttack = 60,
            specialDefense = 70,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val CROBAT = Species(
        name = "crobat",
        type1 = Poison,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 85,
            attack = 90,
            defense = 80,
            specialAttack = 70,
            specialDefense = 80,
            speed = 130
        ),
        genderRatio = 0.5
    )
    val CHINCHOU = Species(
        name = "chinchou",
        type1 = Water,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 75,
            attack = 38,
            defense = 38,
            specialAttack = 56,
            specialDefense = 56,
            speed = 67
        ),
        genderRatio = 0.5
    )
    val LANTURN = Species(
        name = "lanturn",
        type1 = Water,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 125,
            attack = 58,
            defense = 58,
            specialAttack = 76,
            specialDefense = 76,
            speed = 67
        ),
        genderRatio = 0.5
    )
    val PICHU = Species(
        name = "pichu",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 20,
            attack = 40,
            defense = 15,
            specialAttack = 35,
            specialDefense = 35,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val CLEFFA = Species(
        name = "cleffa",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 25,
            defense = 28,
            specialAttack = 45,
            specialDefense = 55,
            speed = 15
        ),
        genderRatio = 0.75
    )
    val IGGLYBUFF = Species(
        name = "igglybuff",
        type1 = Normal,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 90,
            attack = 30,
            defense = 15,
            specialAttack = 40,
            specialDefense = 20,
            speed = 15
        ),
        genderRatio = 0.75
    )
    val TOGEPI = Species(
        name = "togepi",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 20,
            defense = 65,
            specialAttack = 40,
            specialDefense = 65,
            speed = 20
        ),
        genderRatio = 0.125
    )
    val TOGETIC = Species(
        name = "togetic",
        type1 = Fairy,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 40,
            defense = 85,
            specialAttack = 80,
            specialDefense = 105,
            speed = 40
        ),
        genderRatio = 0.125
    )
    val NATU = Species(
        name = "natu",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 50,
            defense = 45,
            specialAttack = 70,
            specialDefense = 45,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val XATU = Species(
        name = "xatu",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 75,
            defense = 70,
            specialAttack = 95,
            specialDefense = 70,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val MAREEP = Species(
        name = "mareep",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 40,
            defense = 40,
            specialAttack = 65,
            specialDefense = 45,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val FLAAFFY = Species(
        name = "flaaffy",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 55,
            defense = 55,
            specialAttack = 80,
            specialDefense = 60,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val AMPHAROS = Species(
        name = "ampharos",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 75,
            defense = 85,
            specialAttack = 115,
            specialDefense = 90,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val BELLOSSOM = Species(
        name = "bellossom",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 80,
            defense = 95,
            specialAttack = 90,
            specialDefense = 100,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val MARILL = Species(
        name = "marill",
        type1 = Water,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 70,
            attack = 20,
            defense = 50,
            specialAttack = 20,
            specialDefense = 50,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val AZUMARILL = Species(
        name = "azumarill",
        type1 = Water,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 100,
            attack = 50,
            defense = 80,
            specialAttack = 60,
            specialDefense = 80,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SUDOWOODO = Species(
        name = "sudowoodo",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 100,
            defense = 115,
            specialAttack = 30,
            specialDefense = 65,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val POLITOED = Species(
        name = "politoed",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 75,
            defense = 75,
            specialAttack = 90,
            specialDefense = 100,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val HOPPIP = Species(
        name = "hoppip",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 35,
            attack = 35,
            defense = 40,
            specialAttack = 35,
            specialDefense = 55,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SKIPLOOM = Species(
        name = "skiploom",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 45,
            defense = 50,
            specialAttack = 45,
            specialDefense = 65,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val JUMPLUFF = Species(
        name = "jumpluff",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 55,
            defense = 70,
            specialAttack = 55,
            specialDefense = 95,
            speed = 110
        ),
        genderRatio = 0.5
    )
    val AIPOM = Species(
        name = "aipom",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 70,
            defense = 55,
            specialAttack = 40,
            specialDefense = 55,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SUNKERN = Species(
        name = "sunkern",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 30,
            defense = 30,
            specialAttack = 30,
            specialDefense = 30,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val SUNFLORA = Species(
        name = "sunflora",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 75,
            defense = 55,
            specialAttack = 105,
            specialDefense = 85,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val YANMA = Species(
        name = "yanma",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 65,
            defense = 45,
            specialAttack = 75,
            specialDefense = 45,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val WOOPER = Species(
        name = "wooper",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 55,
            attack = 45,
            defense = 45,
            specialAttack = 25,
            specialDefense = 25,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val QUAGSIRE = Species(
        name = "quagsire",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 95,
            attack = 85,
            defense = 85,
            specialAttack = 65,
            specialDefense = 65,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val ESPEON = Species(
        name = "espeon",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 65,
            defense = 60,
            specialAttack = 130,
            specialDefense = 95,
            speed = 110
        ),
        genderRatio = 0.125
    )
    val UMBREON = Species(
        name = "umbreon",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 65,
            defense = 110,
            specialAttack = 60,
            specialDefense = 130,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val MURKROW = Species(
        name = "murkrow",
        type1 = Dark,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 42,
            specialAttack = 85,
            specialDefense = 42,
            speed = 91
        ),
        genderRatio = 0.5
    )
    val SLOWKING = Species(
        name = "slowking",
        type1 = Water,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 95,
            attack = 75,
            defense = 80,
            specialAttack = 100,
            specialDefense = 110,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val MISDREAVUS = Species(
        name = "misdreavus",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 60,
            specialAttack = 85,
            specialDefense = 85,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val UNOWN = Species(
        name = "unown",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 48,
            attack = 72,
            defense = 48,
            specialAttack = 72,
            specialDefense = 48,
            speed = 48
        ),
        genderRatio = null
    )
    val WOBBUFFET = Species(
        name = "wobbuffet",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 190,
            attack = 33,
            defense = 58,
            specialAttack = 33,
            specialDefense = 58,
            speed = 33
        ),
        genderRatio = 0.5
    )
    val GIRAFARIG = Species(
        name = "girafarig",
        type1 = Normal,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 70,
            attack = 80,
            defense = 65,
            specialAttack = 90,
            specialDefense = 65,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val PINECO = Species(
        name = "pineco",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 65,
            defense = 90,
            specialAttack = 35,
            specialDefense = 35,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val FORRETRESS = Species(
        name = "forretress",
        type1 = Bug,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 75,
            attack = 90,
            defense = 140,
            specialAttack = 60,
            specialDefense = 60,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val DUNSPARCE = Species(
        name = "dunsparce",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 70,
            defense = 70,
            specialAttack = 65,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val GLIGAR = Species(
        name = "gligar",
        type1 = Ground,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 75,
            defense = 105,
            specialAttack = 35,
            specialDefense = 65,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val STEELIX = Species(
        name = "steelix",
        type1 = Steel,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 75,
            attack = 85,
            defense = 200,
            specialAttack = 55,
            specialDefense = 65,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val SNUBBULL = Species(
        name = "snubbull",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 80,
            defense = 50,
            specialAttack = 40,
            specialDefense = 40,
            speed = 30
        ),
        genderRatio = 0.75
    )
    val GRANBULL = Species(
        name = "granbull",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 120,
            defense = 75,
            specialAttack = 60,
            specialDefense = 60,
            speed = 45
        ),
        genderRatio = 0.75
    )
    val QWILFISH = Species(
        name = "qwilfish",
        type1 = Water,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 65,
            attack = 95,
            defense = 85,
            specialAttack = 55,
            specialDefense = 55,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SCIZOR = Species(
        name = "scizor",
        type1 = Bug,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 70,
            attack = 130,
            defense = 100,
            specialAttack = 55,
            specialDefense = 80,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val SHUCKLE = Species(
        name = "shuckle",
        type1 = Bug,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 20,
            attack = 10,
            defense = 230,
            specialAttack = 10,
            specialDefense = 230,
            speed = 5
        ),
        genderRatio = 0.5
    )
    val HERACROSS = Species(
        name = "heracross",
        type1 = Bug,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 80,
            attack = 125,
            defense = 75,
            specialAttack = 40,
            specialDefense = 95,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SNEASEL = Species(
        name = "sneasel",
        type1 = Dark,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 55,
            attack = 95,
            defense = 55,
            specialAttack = 35,
            specialDefense = 75,
            speed = 115
        ),
        genderRatio = 0.5
    )
    val TEDDIURSA = Species(
        name = "teddiursa",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 80,
            defense = 50,
            specialAttack = 50,
            specialDefense = 50,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val URSARING = Species(
        name = "ursaring",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 130,
            defense = 75,
            specialAttack = 75,
            specialDefense = 75,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val SLUGMA = Species(
        name = "slugma",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 40,
            defense = 40,
            specialAttack = 70,
            specialDefense = 40,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val MAGCARGO = Species(
        name = "magcargo",
        type1 = Fire,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 120,
            specialAttack = 90,
            specialDefense = 80,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val SWINUB = Species(
        name = "swinub",
        type1 = Ice,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 40,
            specialAttack = 30,
            specialDefense = 30,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val PILOSWINE = Species(
        name = "piloswine",
        type1 = Ice,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 80,
            specialAttack = 60,
            specialDefense = 60,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val CORSOLA = Species(
        name = "corsola",
        type1 = Water,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 65,
            attack = 55,
            defense = 95,
            specialAttack = 65,
            specialDefense = 95,
            speed = 35
        ),
        genderRatio = 0.75
    )
    val REMORAID = Species(
        name = "remoraid",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 65,
            defense = 35,
            specialAttack = 65,
            specialDefense = 35,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val OCTILLERY = Species(
        name = "octillery",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 105,
            defense = 75,
            specialAttack = 105,
            specialDefense = 75,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val DELIBIRD = Species(
        name = "delibird",
        type1 = Ice,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 45,
            attack = 55,
            defense = 45,
            specialAttack = 65,
            specialDefense = 45,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val MANTINE = Species(
        name = "mantine",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 85,
            attack = 40,
            defense = 70,
            specialAttack = 80,
            specialDefense = 140,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val SKARMORY = Species(
        name = "skarmory",
        type1 = Steel,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 80,
            defense = 140,
            specialAttack = 40,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val HOUNDOUR = Species(
        name = "houndour",
        type1 = Dark,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 45,
            attack = 60,
            defense = 30,
            specialAttack = 80,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val HOUNDOOM = Species(
        name = "houndoom",
        type1 = Dark,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 75,
            attack = 90,
            defense = 50,
            specialAttack = 110,
            specialDefense = 80,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val KINGDRA = Species(
        name = "kingdra",
        type1 = Water,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 75,
            attack = 95,
            defense = 95,
            specialAttack = 95,
            specialDefense = 95,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val PHANPY = Species(
        name = "phanpy",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 60,
            defense = 60,
            specialAttack = 40,
            specialDefense = 40,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val DONPHAN = Species(
        name = "donphan",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 120,
            defense = 120,
            specialAttack = 60,
            specialDefense = 60,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val PORYGON2 = Species(
        name = "porygon2",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 80,
            defense = 90,
            specialAttack = 105,
            specialDefense = 95,
            speed = 60
        ),
        genderRatio = null
    )
    val STANTLER = Species(
        name = "stantler",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 73,
            attack = 95,
            defense = 62,
            specialAttack = 85,
            specialDefense = 65,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SMEARGLE = Species(
        name = "smeargle",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 20,
            defense = 35,
            specialAttack = 20,
            specialDefense = 45,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val TYROGUE = Species(
        name = "tyrogue",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 35,
            defense = 35,
            specialAttack = 35,
            specialDefense = 35,
            speed = 35
        ),
        genderRatio = 0.0
    )
    val HITMONTOP = Species(
        name = "hitmontop",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 95,
            defense = 95,
            specialAttack = 35,
            specialDefense = 110,
            speed = 70
        ),
        genderRatio = 0.0
    )
    val SMOOCHUM = Species(
        name = "smoochum",
        type1 = Ice,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 45,
            attack = 30,
            defense = 15,
            specialAttack = 85,
            specialDefense = 65,
            speed = 65
        ),
        genderRatio = 1.0
    )
    val ELEKID = Species(
        name = "elekid",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 63,
            defense = 37,
            specialAttack = 65,
            specialDefense = 55,
            speed = 95
        ),
        genderRatio = 0.25
    )
    val MAGBY = Species(
        name = "magby",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 75,
            defense = 37,
            specialAttack = 70,
            specialDefense = 55,
            speed = 83
        ),
        genderRatio = 0.25
    )
    val MILTANK = Species(
        name = "miltank",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 80,
            defense = 105,
            specialAttack = 40,
            specialDefense = 70,
            speed = 100
        ),
        genderRatio = 1.0
    )
    val BLISSEY = Species(
        name = "blissey",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 255,
            attack = 10,
            defense = 10,
            specialAttack = 75,
            specialDefense = 135,
            speed = 55
        ),
        genderRatio = 1.0
    )
    val RAIKOU = Species(
        name = "raikou",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 85,
            defense = 75,
            specialAttack = 115,
            specialDefense = 100,
            speed = 115
        ),
        genderRatio = null
    )
    val ENTEI = Species(
        name = "entei",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 115,
            attack = 115,
            defense = 85,
            specialAttack = 90,
            specialDefense = 75,
            speed = 100
        ),
        genderRatio = null
    )
    val SUICUNE = Species(
        name = "suicune",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 75,
            defense = 115,
            specialAttack = 90,
            specialDefense = 115,
            speed = 85
        ),
        genderRatio = null
    )
    val LARVITAR = Species(
        name = "larvitar",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 50,
            attack = 64,
            defense = 50,
            specialAttack = 45,
            specialDefense = 50,
            speed = 41
        ),
        genderRatio = 0.5
    )
    val PUPITAR = Species(
        name = "pupitar",
        type1 = Rock,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 70,
            attack = 84,
            defense = 70,
            specialAttack = 65,
            specialDefense = 70,
            speed = 51
        ),
        genderRatio = 0.5
    )
    val TYRANITAR = Species(
        name = "tyranitar",
        type1 = Rock,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 100,
            attack = 134,
            defense = 110,
            specialAttack = 95,
            specialDefense = 100,
            speed = 61
        ),
        genderRatio = 0.5
    )
    val LUGIA = Species(
        name = "lugia",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 106,
            attack = 90,
            defense = 130,
            specialAttack = 90,
            specialDefense = 154,
            speed = 110
        ),
        genderRatio = null
    )
    val HO_OH = Species(
        name = "ho-oh",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 106,
            attack = 130,
            defense = 90,
            specialAttack = 110,
            specialDefense = 154,
            speed = 90
        ),
        genderRatio = null
    )
    val CELEBI = Species(
        name = "celebi",
        type1 = Psychic,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val TREECKO = Species(
        name = "treecko",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 35,
            specialAttack = 65,
            specialDefense = 55,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val GROVYLE = Species(
        name = "grovyle",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 65,
            defense = 45,
            specialAttack = 85,
            specialDefense = 65,
            speed = 95
        ),
        genderRatio = 0.125
    )
    val SCEPTILE = Species(
        name = "sceptile",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 85,
            defense = 65,
            specialAttack = 105,
            specialDefense = 85,
            speed = 120
        ),
        genderRatio = 0.125
    )
    val TORCHIC = Species(
        name = "torchic",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 60,
            defense = 40,
            specialAttack = 70,
            specialDefense = 50,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val COMBUSKEN = Species(
        name = "combusken",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 60,
            specialAttack = 85,
            specialDefense = 60,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val BLAZIKEN = Species(
        name = "blaziken",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 80,
            attack = 120,
            defense = 70,
            specialAttack = 110,
            specialDefense = 70,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val MUDKIP = Species(
        name = "mudkip",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 70,
            defense = 50,
            specialAttack = 50,
            specialDefense = 50,
            speed = 40
        ),
        genderRatio = 0.125
    )
    val MARSHTOMP = Species(
        name = "marshtomp",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 70,
            attack = 85,
            defense = 70,
            specialAttack = 60,
            specialDefense = 70,
            speed = 50
        ),
        genderRatio = 0.125
    )
    val SWAMPERT = Species(
        name = "swampert",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 100,
            attack = 110,
            defense = 90,
            specialAttack = 85,
            specialDefense = 90,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val POOCHYENA = Species(
        name = "poochyena",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 55,
            defense = 35,
            specialAttack = 30,
            specialDefense = 30,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val MIGHTYENA = Species(
        name = "mightyena",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 90,
            defense = 70,
            specialAttack = 60,
            specialDefense = 60,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val ZIGZAGOON = Species(
        name = "zigzagoon",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 38,
            attack = 30,
            defense = 41,
            specialAttack = 30,
            specialDefense = 41,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val LINOONE = Species(
        name = "linoone",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 78,
            attack = 70,
            defense = 61,
            specialAttack = 50,
            specialDefense = 61,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val WURMPLE = Species(
        name = "wurmple",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 45,
            defense = 35,
            specialAttack = 20,
            specialDefense = 30,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val SILCOON = Species(
        name = "silcoon",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 35,
            defense = 55,
            specialAttack = 25,
            specialDefense = 25,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val BEAUTIFLY = Species(
        name = "beautifly",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 70,
            defense = 50,
            specialAttack = 100,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val CASCOON = Species(
        name = "cascoon",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 35,
            defense = 55,
            specialAttack = 25,
            specialDefense = 25,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val DUSTOX = Species(
        name = "dustox",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 70,
            specialAttack = 50,
            specialDefense = 90,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val LOTAD = Species(
        name = "lotad",
        type1 = Water,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 30,
            specialAttack = 40,
            specialDefense = 50,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val LOMBRE = Species(
        name = "lombre",
        type1 = Water,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 50,
            specialAttack = 60,
            specialDefense = 70,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val LUDICOLO = Species(
        name = "ludicolo",
        type1 = Water,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 80,
            attack = 70,
            defense = 70,
            specialAttack = 90,
            specialDefense = 100,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val SEEDOT = Species(
        name = "seedot",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 40,
            defense = 50,
            specialAttack = 30,
            specialDefense = 30,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val NUZLEAF = Species(
        name = "nuzleaf",
        type1 = Grass,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 70,
            attack = 70,
            defense = 40,
            specialAttack = 60,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val SHIFTRY = Species(
        name = "shiftry",
        type1 = Grass,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 90,
            attack = 100,
            defense = 60,
            specialAttack = 90,
            specialDefense = 60,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val TAILLOW = Species(
        name = "taillow",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 30,
            specialAttack = 30,
            specialDefense = 30,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SWELLOW = Species(
        name = "swellow",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 60,
            specialAttack = 75,
            specialDefense = 50,
            speed = 125
        ),
        genderRatio = 0.5
    )
    val WINGULL = Species(
        name = "wingull",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 30,
            specialAttack = 55,
            specialDefense = 30,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val PELIPPER = Species(
        name = "pelipper",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 100,
            specialAttack = 95,
            specialDefense = 70,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val RALTS = Species(
        name = "ralts",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 28,
            attack = 25,
            defense = 25,
            specialAttack = 45,
            specialDefense = 35,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val KIRLIA = Species(
        name = "kirlia",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 38,
            attack = 35,
            defense = 35,
            specialAttack = 65,
            specialDefense = 55,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val GARDEVOIR = Species(
        name = "gardevoir",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 68,
            attack = 65,
            defense = 65,
            specialAttack = 125,
            specialDefense = 115,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val SURSKIT = Species(
        name = "surskit",
        type1 = Bug,
        type2 = Water,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 32,
            specialAttack = 50,
            specialDefense = 52,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val MASQUERAIN = Species(
        name = "masquerain",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 60,
            defense = 62,
            specialAttack = 100,
            specialDefense = 82,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val SHROOMISH = Species(
        name = "shroomish",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 40,
            defense = 60,
            specialAttack = 40,
            specialDefense = 60,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val BRELOOM = Species(
        name = "breloom",
        type1 = Grass,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 60,
            attack = 130,
            defense = 80,
            specialAttack = 60,
            specialDefense = 60,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val SLAKOTH = Species(
        name = "slakoth",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 60,
            specialAttack = 35,
            specialDefense = 35,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val VIGOROTH = Species(
        name = "vigoroth",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 80,
            defense = 80,
            specialAttack = 55,
            specialDefense = 55,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val SLAKING = Species(
        name = "slaking",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 150,
            attack = 160,
            defense = 100,
            specialAttack = 95,
            specialDefense = 65,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val NINCADA = Species(
        name = "nincada",
        type1 = Bug,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 31,
            attack = 45,
            defense = 90,
            specialAttack = 30,
            specialDefense = 30,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val NINJASK = Species(
        name = "ninjask",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 61,
            attack = 90,
            defense = 45,
            specialAttack = 50,
            specialDefense = 50,
            speed = 160
        ),
        genderRatio = 0.5
    )
    val SHEDINJA = Species(
        name = "shedinja",
        type1 = Bug,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 1,
            attack = 90,
            defense = 45,
            specialAttack = 30,
            specialDefense = 30,
            speed = 40
        ),
        genderRatio = null
    )
    val WHISMUR = Species(
        name = "whismur",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 64,
            attack = 51,
            defense = 23,
            specialAttack = 51,
            specialDefense = 23,
            speed = 28
        ),
        genderRatio = 0.5
    )
    val LOUDRED = Species(
        name = "loudred",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 84,
            attack = 71,
            defense = 43,
            specialAttack = 71,
            specialDefense = 43,
            speed = 48
        ),
        genderRatio = 0.5
    )
    val EXPLOUD = Species(
        name = "exploud",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 104,
            attack = 91,
            defense = 63,
            specialAttack = 91,
            specialDefense = 73,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val MAKUHITA = Species(
        name = "makuhita",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 72,
            attack = 60,
            defense = 30,
            specialAttack = 20,
            specialDefense = 30,
            speed = 25
        ),
        genderRatio = 0.25
    )
    val HARIYAMA = Species(
        name = "hariyama",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 144,
            attack = 120,
            defense = 60,
            specialAttack = 40,
            specialDefense = 60,
            speed = 50
        ),
        genderRatio = 0.25
    )
    val AZURILL = Species(
        name = "azurill",
        type1 = Normal,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 50,
            attack = 20,
            defense = 40,
            specialAttack = 20,
            specialDefense = 40,
            speed = 20
        ),
        genderRatio = 0.75
    )
    val NOSEPASS = Species(
        name = "nosepass",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 30,
            attack = 45,
            defense = 135,
            specialAttack = 45,
            specialDefense = 90,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val SKITTY = Species(
        name = "skitty",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 45,
            defense = 45,
            specialAttack = 35,
            specialDefense = 35,
            speed = 50
        ),
        genderRatio = 0.75
    )
    val DELCATTY = Species(
        name = "delcatty",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 65,
            defense = 65,
            specialAttack = 55,
            specialDefense = 55,
            speed = 90
        ),
        genderRatio = 0.75
    )
    val SABLEYE = Species(
        name = "sableye",
        type1 = Dark,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 50,
            attack = 75,
            defense = 75,
            specialAttack = 65,
            specialDefense = 65,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val MAWILE = Species(
        name = "mawile",
        type1 = Steel,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 50,
            attack = 85,
            defense = 85,
            specialAttack = 55,
            specialDefense = 55,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val ARON = Species(
        name = "aron",
        type1 = Steel,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 50,
            attack = 70,
            defense = 100,
            specialAttack = 40,
            specialDefense = 40,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val LAIRON = Species(
        name = "lairon",
        type1 = Steel,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 60,
            attack = 90,
            defense = 140,
            specialAttack = 50,
            specialDefense = 50,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val AGGRON = Species(
        name = "aggron",
        type1 = Steel,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 70,
            attack = 110,
            defense = 180,
            specialAttack = 60,
            specialDefense = 60,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val MEDITITE = Species(
        name = "meditite",
        type1 = Fighting,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 30,
            attack = 40,
            defense = 55,
            specialAttack = 40,
            specialDefense = 55,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val MEDICHAM = Species(
        name = "medicham",
        type1 = Fighting,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 75,
            specialAttack = 60,
            specialDefense = 75,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val ELECTRIKE = Species(
        name = "electrike",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 40,
            specialAttack = 65,
            specialDefense = 40,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val MANECTRIC = Species(
        name = "manectric",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 75,
            defense = 60,
            specialAttack = 105,
            specialDefense = 60,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val PLUSLE = Species(
        name = "plusle",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 40,
            specialAttack = 85,
            specialDefense = 75,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val MINUN = Species(
        name = "minun",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 40,
            defense = 50,
            specialAttack = 75,
            specialDefense = 85,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val VOLBEAT = Species(
        name = "volbeat",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 73,
            defense = 75,
            specialAttack = 47,
            specialDefense = 85,
            speed = 85
        ),
        genderRatio = 0.0
    )
    val ILLUMISE = Species(
        name = "illumise",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 47,
            defense = 75,
            specialAttack = 73,
            specialDefense = 85,
            speed = 85
        ),
        genderRatio = 1.0
    )
    val ROSELIA = Species(
        name = "roselia",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 50,
            attack = 60,
            defense = 45,
            specialAttack = 100,
            specialDefense = 80,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val GULPIN = Species(
        name = "gulpin",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 43,
            defense = 53,
            specialAttack = 43,
            specialDefense = 53,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val SWALOT = Species(
        name = "swalot",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 73,
            defense = 83,
            specialAttack = 73,
            specialDefense = 83,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val CARVANHA = Species(
        name = "carvanha",
        type1 = Water,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 45,
            attack = 90,
            defense = 20,
            specialAttack = 65,
            specialDefense = 20,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val SHARPEDO = Species(
        name = "sharpedo",
        type1 = Water,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 70,
            attack = 120,
            defense = 40,
            specialAttack = 95,
            specialDefense = 40,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val WAILMER = Species(
        name = "wailmer",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 130,
            attack = 70,
            defense = 35,
            specialAttack = 70,
            specialDefense = 35,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val WAILORD = Species(
        name = "wailord",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 170,
            attack = 90,
            defense = 45,
            specialAttack = 90,
            specialDefense = 45,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val NUMEL = Species(
        name = "numel",
        type1 = Fire,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 40,
            specialAttack = 65,
            specialDefense = 45,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val CAMERUPT = Species(
        name = "camerupt",
        type1 = Fire,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 70,
            attack = 100,
            defense = 70,
            specialAttack = 105,
            specialDefense = 75,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val TORKOAL = Species(
        name = "torkoal",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 85,
            defense = 140,
            specialAttack = 85,
            specialDefense = 70,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val SPOINK = Species(
        name = "spoink",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 25,
            defense = 35,
            specialAttack = 70,
            specialDefense = 80,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val GRUMPIG = Species(
        name = "grumpig",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 45,
            defense = 65,
            specialAttack = 90,
            specialDefense = 110,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val SPINDA = Species(
        name = "spinda",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 60,
            specialAttack = 60,
            specialDefense = 60,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val TRAPINCH = Species(
        name = "trapinch",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 100,
            defense = 45,
            specialAttack = 45,
            specialDefense = 45,
            speed = 10
        ),
        genderRatio = 0.5
    )
    val VIBRAVA = Species(
        name = "vibrava",
        type1 = Ground,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 50,
            attack = 70,
            defense = 50,
            specialAttack = 50,
            specialDefense = 50,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val FLYGON = Species(
        name = "flygon",
        type1 = Ground,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 80,
            attack = 100,
            defense = 80,
            specialAttack = 80,
            specialDefense = 80,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val CACNEA = Species(
        name = "cacnea",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 85,
            defense = 40,
            specialAttack = 85,
            specialDefense = 40,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val CACTURNE = Species(
        name = "cacturne",
        type1 = Grass,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 70,
            attack = 115,
            defense = 60,
            specialAttack = 115,
            specialDefense = 60,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val SWABLU = Species(
        name = "swablu",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 45,
            attack = 40,
            defense = 60,
            specialAttack = 40,
            specialDefense = 75,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val ALTARIA = Species(
        name = "altaria",
        type1 = Dragon,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 70,
            defense = 90,
            specialAttack = 70,
            specialDefense = 105,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val ZANGOOSE = Species(
        name = "zangoose",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 73,
            attack = 115,
            defense = 60,
            specialAttack = 60,
            specialDefense = 60,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val SEVIPER = Species(
        name = "seviper",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 73,
            attack = 100,
            defense = 60,
            specialAttack = 100,
            specialDefense = 60,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val LUNATONE = Species(
        name = "lunatone",
        type1 = Rock,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 90,
            attack = 55,
            defense = 65,
            specialAttack = 95,
            specialDefense = 85,
            speed = 70
        ),
        genderRatio = null
    )
    val SOLROCK = Species(
        name = "solrock",
        type1 = Rock,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 90,
            attack = 95,
            defense = 85,
            specialAttack = 55,
            specialDefense = 65,
            speed = 70
        ),
        genderRatio = null
    )
    val BARBOACH = Species(
        name = "barboach",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 50,
            attack = 48,
            defense = 43,
            specialAttack = 46,
            specialDefense = 41,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val WHISCASH = Species(
        name = "whiscash",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 110,
            attack = 78,
            defense = 73,
            specialAttack = 76,
            specialDefense = 71,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val CORPHISH = Species(
        name = "corphish",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 43,
            attack = 80,
            defense = 65,
            specialAttack = 50,
            specialDefense = 35,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val CRAWDAUNT = Species(
        name = "crawdaunt",
        type1 = Water,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 63,
            attack = 120,
            defense = 85,
            specialAttack = 90,
            specialDefense = 55,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val BALTOY = Species(
        name = "baltoy",
        type1 = Ground,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 40,
            attack = 40,
            defense = 55,
            specialAttack = 40,
            specialDefense = 70,
            speed = 55
        ),
        genderRatio = null
    )
    val CLAYDOL = Species(
        name = "claydol",
        type1 = Ground,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 60,
            attack = 70,
            defense = 105,
            specialAttack = 70,
            specialDefense = 120,
            speed = 75
        ),
        genderRatio = null
    )
    val LILEEP = Species(
        name = "lileep",
        type1 = Rock,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 66,
            attack = 41,
            defense = 77,
            specialAttack = 61,
            specialDefense = 87,
            speed = 23
        ),
        genderRatio = 0.125
    )
    val CRADILY = Species(
        name = "cradily",
        type1 = Rock,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 86,
            attack = 81,
            defense = 97,
            specialAttack = 81,
            specialDefense = 107,
            speed = 43
        ),
        genderRatio = 0.125
    )
    val ANORITH = Species(
        name = "anorith",
        type1 = Rock,
        type2 = Bug,
        baseStats = BaseStats(
            hp = 45,
            attack = 95,
            defense = 50,
            specialAttack = 40,
            specialDefense = 50,
            speed = 75
        ),
        genderRatio = 0.125
    )
    val ARMALDO = Species(
        name = "armaldo",
        type1 = Rock,
        type2 = Bug,
        baseStats = BaseStats(
            hp = 75,
            attack = 125,
            defense = 100,
            specialAttack = 70,
            specialDefense = 80,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val FEEBAS = Species(
        name = "feebas",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 20,
            attack = 15,
            defense = 20,
            specialAttack = 10,
            specialDefense = 55,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val MILOTIC = Species(
        name = "milotic",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 60,
            defense = 79,
            specialAttack = 100,
            specialDefense = 125,
            speed = 81
        ),
        genderRatio = 0.5
    )
    val CASTFORM = Species(
        name = "castform",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 70,
            defense = 70,
            specialAttack = 70,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val KECLEON = Species(
        name = "kecleon",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 90,
            defense = 70,
            specialAttack = 60,
            specialDefense = 120,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val SHUPPET = Species(
        name = "shuppet",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 44,
            attack = 75,
            defense = 35,
            specialAttack = 63,
            specialDefense = 33,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val BANETTE = Species(
        name = "banette",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 64,
            attack = 115,
            defense = 65,
            specialAttack = 83,
            specialDefense = 63,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val DUSKULL = Species(
        name = "duskull",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 20,
            attack = 40,
            defense = 90,
            specialAttack = 30,
            specialDefense = 90,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val DUSCLOPS = Species(
        name = "dusclops",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 70,
            defense = 130,
            specialAttack = 60,
            specialDefense = 130,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val TROPIUS = Species(
        name = "tropius",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 99,
            attack = 68,
            defense = 83,
            specialAttack = 72,
            specialDefense = 87,
            speed = 51
        ),
        genderRatio = 0.5
    )
    val CHIMECHO = Species(
        name = "chimecho",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 50,
            defense = 80,
            specialAttack = 95,
            specialDefense = 90,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val ABSOL = Species(
        name = "absol",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 130,
            defense = 60,
            specialAttack = 75,
            specialDefense = 60,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val WYNAUT = Species(
        name = "wynaut",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 23,
            defense = 48,
            specialAttack = 23,
            specialDefense = 48,
            speed = 23
        ),
        genderRatio = 0.5
    )
    val SNORUNT = Species(
        name = "snorunt",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 50,
            specialAttack = 50,
            specialDefense = 50,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val GLALIE = Species(
        name = "glalie",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 80,
            defense = 80,
            specialAttack = 80,
            specialDefense = 80,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val SPHEAL = Species(
        name = "spheal",
        type1 = Ice,
        type2 = Water,
        baseStats = BaseStats(
            hp = 70,
            attack = 40,
            defense = 50,
            specialAttack = 55,
            specialDefense = 50,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val SEALEO = Species(
        name = "sealeo",
        type1 = Ice,
        type2 = Water,
        baseStats = BaseStats(
            hp = 90,
            attack = 60,
            defense = 70,
            specialAttack = 75,
            specialDefense = 70,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val WALREIN = Species(
        name = "walrein",
        type1 = Ice,
        type2 = Water,
        baseStats = BaseStats(
            hp = 110,
            attack = 80,
            defense = 90,
            specialAttack = 95,
            specialDefense = 90,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val CLAMPERL = Species(
        name = "clamperl",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 64,
            defense = 85,
            specialAttack = 74,
            specialDefense = 55,
            speed = 32
        ),
        genderRatio = 0.5
    )
    val HUNTAIL = Species(
        name = "huntail",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 104,
            defense = 105,
            specialAttack = 94,
            specialDefense = 75,
            speed = 52
        ),
        genderRatio = 0.5
    )
    val GOREBYSS = Species(
        name = "gorebyss",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 84,
            defense = 105,
            specialAttack = 114,
            specialDefense = 75,
            speed = 52
        ),
        genderRatio = 0.5
    )
    val RELICANTH = Species(
        name = "relicanth",
        type1 = Water,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 100,
            attack = 90,
            defense = 130,
            specialAttack = 45,
            specialDefense = 65,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val LUVDISC = Species(
        name = "luvdisc",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 43,
            attack = 30,
            defense = 55,
            specialAttack = 40,
            specialDefense = 65,
            speed = 97
        ),
        genderRatio = 0.75
    )
    val BAGON = Species(
        name = "bagon",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 75,
            defense = 60,
            specialAttack = 40,
            specialDefense = 30,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SHELGON = Species(
        name = "shelgon",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 95,
            defense = 100,
            specialAttack = 60,
            specialDefense = 50,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SALAMENCE = Species(
        name = "salamence",
        type1 = Dragon,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 95,
            attack = 135,
            defense = 80,
            specialAttack = 110,
            specialDefense = 80,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val BELDUM = Species(
        name = "beldum",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 80,
            specialAttack = 35,
            specialDefense = 60,
            speed = 30
        ),
        genderRatio = null
    )
    val METANG = Species(
        name = "metang",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 60,
            attack = 75,
            defense = 100,
            specialAttack = 55,
            specialDefense = 80,
            speed = 50
        ),
        genderRatio = null
    )
    val METAGROSS = Species(
        name = "metagross",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 80,
            attack = 135,
            defense = 130,
            specialAttack = 95,
            specialDefense = 90,
            speed = 70
        ),
        genderRatio = null
    )
    val REGIROCK = Species(
        name = "regirock",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 100,
            defense = 200,
            specialAttack = 50,
            specialDefense = 100,
            speed = 50
        ),
        genderRatio = null
    )
    val REGICE = Species(
        name = "regice",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 50,
            defense = 100,
            specialAttack = 100,
            specialDefense = 200,
            speed = 50
        ),
        genderRatio = null
    )
    val REGISTEEL = Species(
        name = "registeel",
        type1 = Steel,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 75,
            defense = 150,
            specialAttack = 75,
            specialDefense = 150,
            speed = 50
        ),
        genderRatio = null
    )
    val LATIAS = Species(
        name = "latias",
        type1 = Dragon,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 80,
            attack = 80,
            defense = 90,
            specialAttack = 110,
            specialDefense = 130,
            speed = 110
        ),
        genderRatio = 1.0
    )
    val LATIOS = Species(
        name = "latios",
        type1 = Dragon,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 80,
            attack = 90,
            defense = 80,
            specialAttack = 130,
            specialDefense = 110,
            speed = 110
        ),
        genderRatio = 0.0
    )
    val KYOGRE = Species(
        name = "kyogre",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 90,
            specialAttack = 150,
            specialDefense = 140,
            speed = 90
        ),
        genderRatio = null
    )
    val GROUDON = Species(
        name = "groudon",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 150,
            defense = 140,
            specialAttack = 100,
            specialDefense = 90,
            speed = 90
        ),
        genderRatio = null
    )
    val RAYQUAZA = Species(
        name = "rayquaza",
        type1 = Dragon,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 105,
            attack = 150,
            defense = 90,
            specialAttack = 150,
            specialDefense = 90,
            speed = 95
        ),
        genderRatio = null
    )
    val JIRACHI = Species(
        name = "jirachi",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val DEOXYS = Species(
        name = "deoxys",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 150,
            defense = 50,
            specialAttack = 150,
            specialDefense = 50,
            speed = 150
        ),
        genderRatio = null
    )
    val TURTWIG = Species(
        name = "turtwig",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 68,
            defense = 64,
            specialAttack = 45,
            specialDefense = 55,
            speed = 31
        ),
        genderRatio = 0.125
    )
    val GROTLE = Species(
        name = "grotle",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 89,
            defense = 85,
            specialAttack = 55,
            specialDefense = 65,
            speed = 36
        ),
        genderRatio = 0.125
    )
    val TORTERRA = Species(
        name = "torterra",
        type1 = Grass,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 95,
            attack = 109,
            defense = 105,
            specialAttack = 75,
            specialDefense = 85,
            speed = 56
        ),
        genderRatio = 0.125
    )
    val CHIMCHAR = Species(
        name = "chimchar",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 44,
            attack = 58,
            defense = 44,
            specialAttack = 58,
            specialDefense = 44,
            speed = 61
        ),
        genderRatio = 0.125
    )
    val MONFERNO = Species(
        name = "monferno",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 64,
            attack = 78,
            defense = 52,
            specialAttack = 78,
            specialDefense = 52,
            speed = 81
        ),
        genderRatio = 0.125
    )
    val INFERNAPE = Species(
        name = "infernape",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 76,
            attack = 104,
            defense = 71,
            specialAttack = 104,
            specialDefense = 71,
            speed = 108
        ),
        genderRatio = 0.125
    )
    val PIPLUP = Species(
        name = "piplup",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 53,
            attack = 51,
            defense = 53,
            specialAttack = 61,
            specialDefense = 56,
            speed = 40
        ),
        genderRatio = 0.125
    )
    val PRINPLUP = Species(
        name = "prinplup",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 64,
            attack = 66,
            defense = 68,
            specialAttack = 81,
            specialDefense = 76,
            speed = 50
        ),
        genderRatio = 0.125
    )
    val EMPOLEON = Species(
        name = "empoleon",
        type1 = Water,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 84,
            attack = 86,
            defense = 88,
            specialAttack = 111,
            specialDefense = 101,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val STARLY = Species(
        name = "starly",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 30,
            specialAttack = 30,
            specialDefense = 30,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val STARAVIA = Species(
        name = "staravia",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 75,
            defense = 50,
            specialAttack = 40,
            specialDefense = 40,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val STARAPTOR = Species(
        name = "staraptor",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 85,
            attack = 120,
            defense = 70,
            specialAttack = 50,
            specialDefense = 60,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val BIDOOF = Species(
        name = "bidoof",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 59,
            attack = 45,
            defense = 40,
            specialAttack = 35,
            specialDefense = 40,
            speed = 31
        ),
        genderRatio = 0.5
    )
    val BIBAREL = Species(
        name = "bibarel",
        type1 = Normal,
        type2 = Water,
        baseStats = BaseStats(
            hp = 79,
            attack = 85,
            defense = 60,
            specialAttack = 55,
            specialDefense = 60,
            speed = 71
        ),
        genderRatio = 0.5
    )
    val KRICKETOT = Species(
        name = "kricketot",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 37,
            attack = 25,
            defense = 41,
            specialAttack = 25,
            specialDefense = 41,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val KRICKETUNE = Species(
        name = "kricketune",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 77,
            attack = 85,
            defense = 51,
            specialAttack = 55,
            specialDefense = 51,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val SHINX = Species(
        name = "shinx",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 65,
            defense = 34,
            specialAttack = 40,
            specialDefense = 34,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val LUXIO = Species(
        name = "luxio",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 49,
            specialAttack = 60,
            specialDefense = 49,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val LUXRAY = Species(
        name = "luxray",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 120,
            defense = 79,
            specialAttack = 95,
            specialDefense = 79,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val BUDEW = Species(
        name = "budew",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 35,
            specialAttack = 50,
            specialDefense = 70,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val ROSERADE = Species(
        name = "roserade",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 70,
            defense = 65,
            specialAttack = 125,
            specialDefense = 105,
            speed = 90
        ),
        genderRatio = 0.5
    )
    val CRANIDOS = Species(
        name = "cranidos",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 67,
            attack = 125,
            defense = 40,
            specialAttack = 30,
            specialDefense = 30,
            speed = 58
        ),
        genderRatio = 0.125
    )
    val RAMPARDOS = Species(
        name = "rampardos",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 97,
            attack = 165,
            defense = 60,
            specialAttack = 65,
            specialDefense = 50,
            speed = 58
        ),
        genderRatio = 0.125
    )
    val SHIELDON = Species(
        name = "shieldon",
        type1 = Rock,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 30,
            attack = 42,
            defense = 118,
            specialAttack = 42,
            specialDefense = 88,
            speed = 30
        ),
        genderRatio = 0.125
    )
    val BASTIODON = Species(
        name = "bastiodon",
        type1 = Rock,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 60,
            attack = 52,
            defense = 168,
            specialAttack = 47,
            specialDefense = 138,
            speed = 30
        ),
        genderRatio = 0.125
    )
    val BURMY = Species(
        name = "burmy",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 29,
            defense = 45,
            specialAttack = 29,
            specialDefense = 45,
            speed = 36
        ),
        genderRatio = 0.5
    )
    val WORMADAM = Species(
        name = "wormadam",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 60,
            attack = 59,
            defense = 85,
            specialAttack = 79,
            specialDefense = 105,
            speed = 36
        ),
        genderRatio = 1.0
    )
    val MOTHIM = Species(
        name = "mothim",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 94,
            defense = 50,
            specialAttack = 94,
            specialDefense = 50,
            speed = 66
        ),
        genderRatio = 0.0
    )
    val COMBEE = Species(
        name = "combee",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 30,
            attack = 30,
            defense = 42,
            specialAttack = 30,
            specialDefense = 42,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val VESPIQUEN = Species(
        name = "vespiquen",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 80,
            defense = 102,
            specialAttack = 80,
            specialDefense = 102,
            speed = 40
        ),
        genderRatio = 1.0
    )
    val PACHIRISU = Species(
        name = "pachirisu",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 45,
            defense = 70,
            specialAttack = 45,
            specialDefense = 90,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val BUIZEL = Species(
        name = "buizel",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 65,
            defense = 35,
            specialAttack = 60,
            specialDefense = 30,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val FLOATZEL = Species(
        name = "floatzel",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 105,
            defense = 55,
            specialAttack = 85,
            specialDefense = 50,
            speed = 115
        ),
        genderRatio = 0.5
    )
    val CHERUBI = Species(
        name = "cherubi",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 35,
            defense = 45,
            specialAttack = 62,
            specialDefense = 53,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val CHERRIM = Species(
        name = "cherrim",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 60,
            defense = 70,
            specialAttack = 87,
            specialDefense = 78,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val SHELLOS = Species(
        name = "shellos",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 76,
            attack = 48,
            defense = 48,
            specialAttack = 57,
            specialDefense = 62,
            speed = 34
        ),
        genderRatio = 0.5
    )
    val GASTRODON = Species(
        name = "gastrodon",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 111,
            attack = 83,
            defense = 68,
            specialAttack = 92,
            specialDefense = 82,
            speed = 39
        ),
        genderRatio = 0.5
    )
    val AMBIPOM = Species(
        name = "ambipom",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 100,
            defense = 66,
            specialAttack = 60,
            specialDefense = 66,
            speed = 115
        ),
        genderRatio = 0.5
    )
    val DRIFLOON = Species(
        name = "drifloon",
        type1 = Ghost,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 90,
            attack = 50,
            defense = 34,
            specialAttack = 60,
            specialDefense = 44,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val DRIFBLIM = Species(
        name = "drifblim",
        type1 = Ghost,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 150,
            attack = 80,
            defense = 44,
            specialAttack = 90,
            specialDefense = 54,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val BUNEARY = Species(
        name = "buneary",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 66,
            defense = 44,
            specialAttack = 44,
            specialDefense = 56,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val LOPUNNY = Species(
        name = "lopunny",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 76,
            defense = 84,
            specialAttack = 54,
            specialDefense = 96,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val MISMAGIUS = Species(
        name = "mismagius",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 60,
            specialAttack = 105,
            specialDefense = 105,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val HONCHKROW = Species(
        name = "honchkrow",
        type1 = Dark,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 100,
            attack = 125,
            defense = 52,
            specialAttack = 105,
            specialDefense = 52,
            speed = 71
        ),
        genderRatio = 0.5
    )
    val GLAMEOW = Species(
        name = "glameow",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 49,
            attack = 55,
            defense = 42,
            specialAttack = 42,
            specialDefense = 37,
            speed = 85
        ),
        genderRatio = 0.75
    )
    val PURUGLY = Species(
        name = "purugly",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 71,
            attack = 82,
            defense = 64,
            specialAttack = 64,
            specialDefense = 59,
            speed = 112
        ),
        genderRatio = 0.75
    )
    val CHINGLING = Species(
        name = "chingling",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 30,
            defense = 50,
            specialAttack = 65,
            specialDefense = 50,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val STUNKY = Species(
        name = "stunky",
        type1 = Poison,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 63,
            attack = 63,
            defense = 47,
            specialAttack = 41,
            specialDefense = 41,
            speed = 74
        ),
        genderRatio = 0.5
    )
    val SKUNTANK = Species(
        name = "skuntank",
        type1 = Poison,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 103,
            attack = 93,
            defense = 67,
            specialAttack = 71,
            specialDefense = 61,
            speed = 84
        ),
        genderRatio = 0.5
    )
    val BRONZOR = Species(
        name = "bronzor",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 57,
            attack = 24,
            defense = 86,
            specialAttack = 24,
            specialDefense = 86,
            speed = 23
        ),
        genderRatio = null
    )
    val BRONZONG = Species(
        name = "bronzong",
        type1 = Steel,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 67,
            attack = 89,
            defense = 116,
            specialAttack = 79,
            specialDefense = 116,
            speed = 33
        ),
        genderRatio = null
    )
    val BONSLY = Species(
        name = "bonsly",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 80,
            defense = 95,
            specialAttack = 10,
            specialDefense = 45,
            speed = 10
        ),
        genderRatio = 0.5
    )
    val MIME_JR = Species(
        name = "mime-jr",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 20,
            attack = 25,
            defense = 45,
            specialAttack = 70,
            specialDefense = 90,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val HAPPINY = Species(
        name = "happiny",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 5,
            defense = 5,
            specialAttack = 15,
            specialDefense = 65,
            speed = 30
        ),
        genderRatio = 1.0
    )
    val CHATOT = Species(
        name = "chatot",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 76,
            attack = 65,
            defense = 45,
            specialAttack = 92,
            specialDefense = 42,
            speed = 91
        ),
        genderRatio = 0.5
    )
    val SPIRITOMB = Species(
        name = "spiritomb",
        type1 = Ghost,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 50,
            attack = 92,
            defense = 108,
            specialAttack = 92,
            specialDefense = 108,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val GIBLE = Species(
        name = "gible",
        type1 = Dragon,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 58,
            attack = 70,
            defense = 45,
            specialAttack = 40,
            specialDefense = 45,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val GABITE = Species(
        name = "gabite",
        type1 = Dragon,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 68,
            attack = 90,
            defense = 65,
            specialAttack = 50,
            specialDefense = 55,
            speed = 82
        ),
        genderRatio = 0.5
    )
    val GARCHOMP = Species(
        name = "garchomp",
        type1 = Dragon,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 108,
            attack = 130,
            defense = 95,
            specialAttack = 80,
            specialDefense = 85,
            speed = 102
        ),
        genderRatio = 0.5
    )
    val MUNCHLAX = Species(
        name = "munchlax",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 135,
            attack = 85,
            defense = 40,
            specialAttack = 40,
            specialDefense = 85,
            speed = 5
        ),
        genderRatio = 0.125
    )
    val RIOLU = Species(
        name = "riolu",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 70,
            defense = 40,
            specialAttack = 35,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val LUCARIO = Species(
        name = "lucario",
        type1 = Fighting,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 70,
            attack = 110,
            defense = 70,
            specialAttack = 115,
            specialDefense = 70,
            speed = 90
        ),
        genderRatio = 0.125
    )
    val HIPPOPOTAS = Species(
        name = "hippopotas",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 68,
            attack = 72,
            defense = 78,
            specialAttack = 38,
            specialDefense = 42,
            speed = 32
        ),
        genderRatio = 0.5
    )
    val HIPPOWDON = Species(
        name = "hippowdon",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 108,
            attack = 112,
            defense = 118,
            specialAttack = 68,
            specialDefense = 72,
            speed = 47
        ),
        genderRatio = 0.5
    )
    val SKORUPI = Species(
        name = "skorupi",
        type1 = Poison,
        type2 = Bug,
        baseStats = BaseStats(
            hp = 40,
            attack = 50,
            defense = 90,
            specialAttack = 30,
            specialDefense = 55,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val DRAPION = Species(
        name = "drapion",
        type1 = Poison,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 70,
            attack = 90,
            defense = 110,
            specialAttack = 60,
            specialDefense = 75,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val CROAGUNK = Species(
        name = "croagunk",
        type1 = Poison,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 48,
            attack = 61,
            defense = 40,
            specialAttack = 61,
            specialDefense = 40,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val TOXICROAK = Species(
        name = "toxicroak",
        type1 = Poison,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 83,
            attack = 106,
            defense = 65,
            specialAttack = 86,
            specialDefense = 65,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val CARNIVINE = Species(
        name = "carnivine",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 74,
            attack = 100,
            defense = 72,
            specialAttack = 90,
            specialDefense = 72,
            speed = 46
        ),
        genderRatio = 0.5
    )
    val FINNEON = Species(
        name = "finneon",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 49,
            attack = 49,
            defense = 56,
            specialAttack = 49,
            specialDefense = 61,
            speed = 66
        ),
        genderRatio = 0.5
    )
    val LUMINEON = Species(
        name = "lumineon",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 69,
            attack = 69,
            defense = 76,
            specialAttack = 69,
            specialDefense = 86,
            speed = 91
        ),
        genderRatio = 0.5
    )
    val MANTYKE = Species(
        name = "mantyke",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 45,
            attack = 20,
            defense = 50,
            specialAttack = 60,
            specialDefense = 120,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val SNOVER = Species(
        name = "snover",
        type1 = Grass,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 60,
            attack = 62,
            defense = 50,
            specialAttack = 62,
            specialDefense = 60,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val ABOMASNOW = Species(
        name = "abomasnow",
        type1 = Grass,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 90,
            attack = 92,
            defense = 75,
            specialAttack = 92,
            specialDefense = 85,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val WEAVILE = Species(
        name = "weavile",
        type1 = Dark,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 70,
            attack = 120,
            defense = 65,
            specialAttack = 45,
            specialDefense = 85,
            speed = 125
        ),
        genderRatio = 0.5
    )
    val MAGNEZONE = Species(
        name = "magnezone",
        type1 = Electric,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 70,
            attack = 70,
            defense = 115,
            specialAttack = 130,
            specialDefense = 90,
            speed = 60
        ),
        genderRatio = null
    )
    val LICKILICKY = Species(
        name = "lickilicky",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 110,
            attack = 85,
            defense = 95,
            specialAttack = 80,
            specialDefense = 95,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val RHYPERIOR = Species(
        name = "rhyperior",
        type1 = Ground,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 115,
            attack = 140,
            defense = 130,
            specialAttack = 55,
            specialDefense = 55,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val TANGROWTH = Species(
        name = "tangrowth",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 125,
            specialAttack = 110,
            specialDefense = 50,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val ELECTIVIRE = Species(
        name = "electivire",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 123,
            defense = 67,
            specialAttack = 95,
            specialDefense = 85,
            speed = 95
        ),
        genderRatio = 0.25
    )
    val MAGMORTAR = Species(
        name = "magmortar",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 95,
            defense = 67,
            specialAttack = 125,
            specialDefense = 95,
            speed = 83
        ),
        genderRatio = 0.25
    )
    val TOGEKISS = Species(
        name = "togekiss",
        type1 = Fairy,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 85,
            attack = 50,
            defense = 95,
            specialAttack = 120,
            specialDefense = 115,
            speed = 80
        ),
        genderRatio = 0.125
    )
    val YANMEGA = Species(
        name = "yanmega",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 86,
            attack = 76,
            defense = 86,
            specialAttack = 116,
            specialDefense = 56,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val LEAFEON = Species(
        name = "leafeon",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 110,
            defense = 130,
            specialAttack = 60,
            specialDefense = 65,
            speed = 95
        ),
        genderRatio = 0.125
    )
    val GLACEON = Species(
        name = "glaceon",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 60,
            defense = 110,
            specialAttack = 130,
            specialDefense = 95,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val GLISCOR = Species(
        name = "gliscor",
        type1 = Ground,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 95,
            defense = 125,
            specialAttack = 45,
            specialDefense = 75,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val MAMOSWINE = Species(
        name = "mamoswine",
        type1 = Ice,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 110,
            attack = 130,
            defense = 80,
            specialAttack = 70,
            specialDefense = 60,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val PORYGON_Z = Species(
        name = "porygon-z",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 80,
            defense = 70,
            specialAttack = 135,
            specialDefense = 75,
            speed = 90
        ),
        genderRatio = null
    )
    val GALLADE = Species(
        name = "gallade",
        type1 = Psychic,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 68,
            attack = 125,
            defense = 65,
            specialAttack = 65,
            specialDefense = 115,
            speed = 80
        ),
        genderRatio = 0.0
    )
    val PROBOPASS = Species(
        name = "probopass",
        type1 = Rock,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 60,
            attack = 55,
            defense = 145,
            specialAttack = 75,
            specialDefense = 150,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val DUSKNOIR = Species(
        name = "dusknoir",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 100,
            defense = 135,
            specialAttack = 65,
            specialDefense = 135,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val FROSLASS = Species(
        name = "froslass",
        type1 = Ice,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 70,
            attack = 80,
            defense = 70,
            specialAttack = 80,
            specialDefense = 70,
            speed = 110
        ),
        genderRatio = 1.0
    )
    val ROTOM = Species(
        name = "rotom",
        type1 = Electric,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 77,
            specialAttack = 95,
            specialDefense = 77,
            speed = 91
        ),
        genderRatio = null
    )
    val UXIE = Species(
        name = "uxie",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 75,
            defense = 130,
            specialAttack = 75,
            specialDefense = 130,
            speed = 95
        ),
        genderRatio = null
    )
    val MESPRIT = Species(
        name = "mesprit",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 105,
            defense = 105,
            specialAttack = 105,
            specialDefense = 105,
            speed = 80
        ),
        genderRatio = null
    )
    val AZELF = Species(
        name = "azelf",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 125,
            defense = 70,
            specialAttack = 125,
            specialDefense = 70,
            speed = 115
        ),
        genderRatio = null
    )
    val DIALGA = Species(
        name = "dialga",
        type1 = Steel,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 100,
            attack = 120,
            defense = 120,
            specialAttack = 150,
            specialDefense = 100,
            speed = 90
        ),
        genderRatio = null
    )
    val PALKIA = Species(
        name = "palkia",
        type1 = Water,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 90,
            attack = 120,
            defense = 100,
            specialAttack = 150,
            specialDefense = 120,
            speed = 100
        ),
        genderRatio = null
    )
    val HEATRAN = Species(
        name = "heatran",
        type1 = Fire,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 91,
            attack = 90,
            defense = 106,
            specialAttack = 130,
            specialDefense = 106,
            speed = 77
        ),
        genderRatio = 0.5
    )
    val REGIGIGAS = Species(
        name = "regigigas",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 110,
            attack = 160,
            defense = 110,
            specialAttack = 80,
            specialDefense = 110,
            speed = 100
        ),
        genderRatio = null
    )
    val GIRATINA = Species(
        name = "giratina",
        type1 = Ghost,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 150,
            attack = 100,
            defense = 120,
            specialAttack = 100,
            specialDefense = 120,
            speed = 90
        ),
        genderRatio = null
    )
    val CRESSELIA = Species(
        name = "cresselia",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 120,
            attack = 70,
            defense = 120,
            specialAttack = 75,
            specialDefense = 130,
            speed = 85
        ),
        genderRatio = 1.0
    )
    val PHIONE = Species(
        name = "phione",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 80,
            defense = 80,
            specialAttack = 80,
            specialDefense = 80,
            speed = 80
        ),
        genderRatio = null
    )
    val MANAPHY = Species(
        name = "manaphy",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val DARKRAI = Species(
        name = "darkrai",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 90,
            defense = 90,
            specialAttack = 135,
            specialDefense = 90,
            speed = 125
        ),
        genderRatio = null
    )
    val SHAYMIN = Species(
        name = "shaymin",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val ARCEUS = Species(
        name = "arceus",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 120,
            attack = 120,
            defense = 120,
            specialAttack = 120,
            specialDefense = 120,
            speed = 120
        ),
        genderRatio = null
    )
    val VICTINI = Species(
        name = "victini",
        type1 = Psychic,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 100,
            attack = 100,
            defense = 100,
            specialAttack = 100,
            specialDefense = 100,
            speed = 100
        ),
        genderRatio = null
    )
    val SNIVY = Species(
        name = "snivy",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 45,
            defense = 55,
            specialAttack = 45,
            specialDefense = 55,
            speed = 63
        ),
        genderRatio = 0.125
    )
    val SERVINE = Species(
        name = "servine",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 75,
            specialAttack = 60,
            specialDefense = 75,
            speed = 83
        ),
        genderRatio = 0.125
    )
    val SERPERIOR = Species(
        name = "serperior",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 75,
            defense = 95,
            specialAttack = 75,
            specialDefense = 95,
            speed = 113
        ),
        genderRatio = 0.125
    )
    val TEPIG = Species(
        name = "tepig",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 63,
            defense = 45,
            specialAttack = 45,
            specialDefense = 45,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val PIGNITE = Species(
        name = "pignite",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 90,
            attack = 93,
            defense = 55,
            specialAttack = 70,
            specialDefense = 55,
            speed = 55
        ),
        genderRatio = 0.125
    )
    val EMBOAR = Species(
        name = "emboar",
        type1 = Fire,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 110,
            attack = 123,
            defense = 65,
            specialAttack = 100,
            specialDefense = 65,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val OSHAWOTT = Species(
        name = "oshawott",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 55,
            defense = 45,
            specialAttack = 63,
            specialDefense = 45,
            speed = 45
        ),
        genderRatio = 0.125
    )
    val DEWOTT = Species(
        name = "dewott",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 75,
            defense = 60,
            specialAttack = 83,
            specialDefense = 60,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val SAMUROTT = Species(
        name = "samurott",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 100,
            defense = 85,
            specialAttack = 108,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val PATRAT = Species(
        name = "patrat",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 55,
            defense = 39,
            specialAttack = 35,
            specialDefense = 39,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val WATCHOG = Species(
        name = "watchog",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 69,
            specialAttack = 60,
            specialDefense = 69,
            speed = 77
        ),
        genderRatio = 0.5
    )
    val LILLIPUP = Species(
        name = "lillipup",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 60,
            defense = 45,
            specialAttack = 25,
            specialDefense = 45,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val HERDIER = Species(
        name = "herdier",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 80,
            defense = 65,
            specialAttack = 35,
            specialDefense = 65,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val STOUTLAND = Species(
        name = "stoutland",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 110,
            defense = 90,
            specialAttack = 45,
            specialDefense = 90,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val PURRLOIN = Species(
        name = "purrloin",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 41,
            attack = 50,
            defense = 37,
            specialAttack = 50,
            specialDefense = 37,
            speed = 66
        ),
        genderRatio = 0.5
    )
    val LIEPARD = Species(
        name = "liepard",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 64,
            attack = 88,
            defense = 50,
            specialAttack = 88,
            specialDefense = 50,
            speed = 106
        ),
        genderRatio = 0.5
    )
    val PANSAGE = Species(
        name = "pansage",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 53,
            defense = 48,
            specialAttack = 53,
            specialDefense = 48,
            speed = 64
        ),
        genderRatio = 0.125
    )
    val SIMISAGE = Species(
        name = "simisage",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 98,
            defense = 63,
            specialAttack = 98,
            specialDefense = 63,
            speed = 101
        ),
        genderRatio = 0.125
    )
    val PANSEAR = Species(
        name = "pansear",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 53,
            defense = 48,
            specialAttack = 53,
            specialDefense = 48,
            speed = 64
        ),
        genderRatio = 0.125
    )
    val SIMISEAR = Species(
        name = "simisear",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 98,
            defense = 63,
            specialAttack = 98,
            specialDefense = 63,
            speed = 101
        ),
        genderRatio = 0.125
    )
    val PANPOUR = Species(
        name = "panpour",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 53,
            defense = 48,
            specialAttack = 53,
            specialDefense = 48,
            speed = 64
        ),
        genderRatio = 0.125
    )
    val SIMIPOUR = Species(
        name = "simipour",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 98,
            defense = 63,
            specialAttack = 98,
            specialDefense = 63,
            speed = 101
        ),
        genderRatio = 0.125
    )
    val MUNNA = Species(
        name = "munna",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 76,
            attack = 25,
            defense = 45,
            specialAttack = 67,
            specialDefense = 55,
            speed = 24
        ),
        genderRatio = 0.5
    )
    val MUSHARNA = Species(
        name = "musharna",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 116,
            attack = 55,
            defense = 85,
            specialAttack = 107,
            specialDefense = 95,
            speed = 29
        ),
        genderRatio = 0.5
    )
    val PIDOVE = Species(
        name = "pidove",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 50,
            attack = 55,
            defense = 50,
            specialAttack = 36,
            specialDefense = 30,
            speed = 43
        ),
        genderRatio = 0.5
    )
    val TRANQUILL = Species(
        name = "tranquill",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 62,
            attack = 77,
            defense = 62,
            specialAttack = 50,
            specialDefense = 42,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val UNFEZANT = Species(
        name = "unfezant",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 80,
            attack = 115,
            defense = 80,
            specialAttack = 65,
            specialDefense = 55,
            speed = 93
        ),
        genderRatio = 0.5
    )
    val BLITZLE = Species(
        name = "blitzle",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 60,
            defense = 32,
            specialAttack = 50,
            specialDefense = 32,
            speed = 76
        ),
        genderRatio = 0.5
    )
    val ZEBSTRIKA = Species(
        name = "zebstrika",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 100,
            defense = 63,
            specialAttack = 80,
            specialDefense = 63,
            speed = 116
        ),
        genderRatio = 0.5
    )
    val ROGGENROLA = Species(
        name = "roggenrola",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 75,
            defense = 85,
            specialAttack = 25,
            specialDefense = 25,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val BOLDORE = Species(
        name = "boldore",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 105,
            defense = 105,
            specialAttack = 50,
            specialDefense = 40,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val GIGALITH = Species(
        name = "gigalith",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 135,
            defense = 130,
            specialAttack = 60,
            specialDefense = 80,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val WOOBAT = Species(
        name = "woobat",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 65,
            attack = 45,
            defense = 43,
            specialAttack = 55,
            specialDefense = 43,
            speed = 72
        ),
        genderRatio = 0.5
    )
    val SWOOBAT = Species(
        name = "swoobat",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 67,
            attack = 57,
            defense = 55,
            specialAttack = 77,
            specialDefense = 55,
            speed = 114
        ),
        genderRatio = 0.5
    )
    val DRILBUR = Species(
        name = "drilbur",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 85,
            defense = 40,
            specialAttack = 30,
            specialDefense = 45,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val EXCADRILL = Species(
        name = "excadrill",
        type1 = Ground,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 110,
            attack = 135,
            defense = 60,
            specialAttack = 50,
            specialDefense = 65,
            speed = 88
        ),
        genderRatio = 0.5
    )
    val AUDINO = Species(
        name = "audino",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 103,
            attack = 60,
            defense = 86,
            specialAttack = 60,
            specialDefense = 86,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val TIMBURR = Species(
        name = "timburr",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 80,
            defense = 55,
            specialAttack = 25,
            specialDefense = 35,
            speed = 35
        ),
        genderRatio = 0.25
    )
    val GURDURR = Species(
        name = "gurdurr",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 105,
            defense = 85,
            specialAttack = 40,
            specialDefense = 50,
            speed = 40
        ),
        genderRatio = 0.25
    )
    val CONKELDURR = Species(
        name = "conkeldurr",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 105,
            attack = 140,
            defense = 95,
            specialAttack = 55,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.25
    )
    val TYMPOLE = Species(
        name = "tympole",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 40,
            specialAttack = 50,
            specialDefense = 40,
            speed = 64
        ),
        genderRatio = 0.5
    )
    val PALPITOAD = Species(
        name = "palpitoad",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 75,
            attack = 65,
            defense = 55,
            specialAttack = 65,
            specialDefense = 55,
            speed = 69
        ),
        genderRatio = 0.5
    )
    val SEISMITOAD = Species(
        name = "seismitoad",
        type1 = Water,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 105,
            attack = 95,
            defense = 75,
            specialAttack = 85,
            specialDefense = 75,
            speed = 74
        ),
        genderRatio = 0.5
    )
    val THROH = Species(
        name = "throh",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 120,
            attack = 100,
            defense = 85,
            specialAttack = 30,
            specialDefense = 85,
            speed = 45
        ),
        genderRatio = 0.0
    )
    val SAWK = Species(
        name = "sawk",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 125,
            defense = 75,
            specialAttack = 30,
            specialDefense = 75,
            speed = 85
        ),
        genderRatio = 0.0
    )
    val SEWADDLE = Species(
        name = "sewaddle",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 45,
            attack = 53,
            defense = 70,
            specialAttack = 40,
            specialDefense = 60,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val SWADLOON = Species(
        name = "swadloon",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 55,
            attack = 63,
            defense = 90,
            specialAttack = 50,
            specialDefense = 80,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val LEAVANNY = Species(
        name = "leavanny",
        type1 = Bug,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 75,
            attack = 103,
            defense = 80,
            specialAttack = 70,
            specialDefense = 80,
            speed = 92
        ),
        genderRatio = 0.5
    )
    val VENIPEDE = Species(
        name = "venipede",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 30,
            attack = 45,
            defense = 59,
            specialAttack = 30,
            specialDefense = 39,
            speed = 57
        ),
        genderRatio = 0.5
    )
    val WHIRLIPEDE = Species(
        name = "whirlipede",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 99,
            specialAttack = 40,
            specialDefense = 79,
            speed = 47
        ),
        genderRatio = 0.5
    )
    val SCOLIPEDE = Species(
        name = "scolipede",
        type1 = Bug,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 60,
            attack = 100,
            defense = 89,
            specialAttack = 55,
            specialDefense = 69,
            speed = 112
        ),
        genderRatio = 0.5
    )
    val COTTONEE = Species(
        name = "cottonee",
        type1 = Grass,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 40,
            attack = 27,
            defense = 60,
            specialAttack = 37,
            specialDefense = 50,
            speed = 66
        ),
        genderRatio = 0.5
    )
    val WHIMSICOTT = Species(
        name = "whimsicott",
        type1 = Grass,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 60,
            attack = 67,
            defense = 85,
            specialAttack = 77,
            specialDefense = 75,
            speed = 116
        ),
        genderRatio = 0.5
    )
    val PETILIL = Species(
        name = "petilil",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 35,
            defense = 50,
            specialAttack = 70,
            specialDefense = 50,
            speed = 30
        ),
        genderRatio = 1.0
    )
    val LILLIGANT = Species(
        name = "lilligant",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 60,
            defense = 75,
            specialAttack = 110,
            specialDefense = 75,
            speed = 90
        ),
        genderRatio = 1.0
    )
    val BASCULIN = Species(
        name = "basculin",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 92,
            defense = 65,
            specialAttack = 80,
            specialDefense = 55,
            speed = 98
        ),
        genderRatio = 0.5
    )
    val SANDILE = Species(
        name = "sandile",
        type1 = Ground,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 50,
            attack = 72,
            defense = 35,
            specialAttack = 35,
            specialDefense = 35,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val KROKOROK = Species(
        name = "krokorok",
        type1 = Ground,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 60,
            attack = 82,
            defense = 45,
            specialAttack = 45,
            specialDefense = 45,
            speed = 74
        ),
        genderRatio = 0.5
    )
    val KROOKODILE = Species(
        name = "krookodile",
        type1 = Ground,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 95,
            attack = 117,
            defense = 80,
            specialAttack = 65,
            specialDefense = 70,
            speed = 92
        ),
        genderRatio = 0.5
    )
    val DARUMAKA = Species(
        name = "darumaka",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 90,
            defense = 45,
            specialAttack = 15,
            specialDefense = 45,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val DARMANITAN = Species(
        name = "darmanitan",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 105,
            attack = 140,
            defense = 55,
            specialAttack = 30,
            specialDefense = 55,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val MARACTUS = Species(
        name = "maractus",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 86,
            defense = 67,
            specialAttack = 106,
            specialDefense = 67,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val DWEBBLE = Species(
        name = "dwebble",
        type1 = Bug,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 50,
            attack = 65,
            defense = 85,
            specialAttack = 35,
            specialDefense = 35,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val CRUSTLE = Species(
        name = "crustle",
        type1 = Bug,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 70,
            attack = 105,
            defense = 125,
            specialAttack = 65,
            specialDefense = 75,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val SCRAGGY = Species(
        name = "scraggy",
        type1 = Dark,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 50,
            attack = 75,
            defense = 70,
            specialAttack = 35,
            specialDefense = 70,
            speed = 48
        ),
        genderRatio = 0.5
    )
    val SCRAFTY = Species(
        name = "scrafty",
        type1 = Dark,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 115,
            specialAttack = 45,
            specialDefense = 115,
            speed = 58
        ),
        genderRatio = 0.5
    )
    val SIGILYPH = Species(
        name = "sigilyph",
        type1 = Psychic,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 72,
            attack = 58,
            defense = 80,
            specialAttack = 103,
            specialDefense = 80,
            speed = 97
        ),
        genderRatio = 0.5
    )
    val YAMASK = Species(
        name = "yamask",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 38,
            attack = 30,
            defense = 85,
            specialAttack = 55,
            specialDefense = 65,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val COFAGRIGUS = Species(
        name = "cofagrigus",
        type1 = Ghost,
        type2 = null,
        baseStats = BaseStats(
            hp = 58,
            attack = 50,
            defense = 145,
            specialAttack = 95,
            specialDefense = 105,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val TIRTOUGA = Species(
        name = "tirtouga",
        type1 = Water,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 54,
            attack = 78,
            defense = 103,
            specialAttack = 53,
            specialDefense = 45,
            speed = 22
        ),
        genderRatio = 0.125
    )
    val CARRACOSTA = Species(
        name = "carracosta",
        type1 = Water,
        type2 = Rock,
        baseStats = BaseStats(
            hp = 74,
            attack = 108,
            defense = 133,
            specialAttack = 83,
            specialDefense = 65,
            speed = 32
        ),
        genderRatio = 0.125
    )
    val ARCHEN = Species(
        name = "archen",
        type1 = Rock,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 112,
            defense = 45,
            specialAttack = 74,
            specialDefense = 45,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val ARCHEOPS = Species(
        name = "archeops",
        type1 = Rock,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 140,
            defense = 65,
            specialAttack = 112,
            specialDefense = 65,
            speed = 110
        ),
        genderRatio = 0.125
    )
    val TRUBBISH = Species(
        name = "trubbish",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 62,
            specialAttack = 40,
            specialDefense = 62,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val GARBODOR = Species(
        name = "garbodor",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 95,
            defense = 82,
            specialAttack = 60,
            specialDefense = 82,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val ZORUA = Species(
        name = "zorua",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 65,
            defense = 40,
            specialAttack = 80,
            specialDefense = 40,
            speed = 65
        ),
        genderRatio = 0.125
    )
    val ZOROARK = Species(
        name = "zoroark",
        type1 = Dark,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 105,
            defense = 60,
            specialAttack = 120,
            specialDefense = 60,
            speed = 105
        ),
        genderRatio = 0.125
    )
    val MINCCINO = Species(
        name = "minccino",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 50,
            defense = 40,
            specialAttack = 40,
            specialDefense = 40,
            speed = 75
        ),
        genderRatio = 0.75
    )
    val CINCCINO = Species(
        name = "cinccino",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 95,
            defense = 60,
            specialAttack = 65,
            specialDefense = 60,
            speed = 115
        ),
        genderRatio = 0.75
    )
    val GOTHITA = Species(
        name = "gothita",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 30,
            defense = 50,
            specialAttack = 55,
            specialDefense = 65,
            speed = 45
        ),
        genderRatio = 0.75
    )
    val GOTHORITA = Species(
        name = "gothorita",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 45,
            defense = 70,
            specialAttack = 75,
            specialDefense = 85,
            speed = 55
        ),
        genderRatio = 0.75
    )
    val GOTHITELLE = Species(
        name = "gothitelle",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 55,
            defense = 95,
            specialAttack = 95,
            specialDefense = 110,
            speed = 65
        ),
        genderRatio = 0.75
    )
    val SOLOSIS = Species(
        name = "solosis",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 30,
            defense = 40,
            specialAttack = 105,
            specialDefense = 50,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val DUOSION = Species(
        name = "duosion",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 40,
            defense = 50,
            specialAttack = 125,
            specialDefense = 60,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val REUNICLUS = Species(
        name = "reuniclus",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 110,
            attack = 65,
            defense = 75,
            specialAttack = 125,
            specialDefense = 85,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val DUCKLETT = Species(
        name = "ducklett",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 62,
            attack = 44,
            defense = 50,
            specialAttack = 44,
            specialDefense = 50,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val SWANNA = Species(
        name = "swanna",
        type1 = Water,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 87,
            defense = 63,
            specialAttack = 87,
            specialDefense = 63,
            speed = 98
        ),
        genderRatio = 0.5
    )
    val VANILLITE = Species(
        name = "vanillite",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 36,
            attack = 50,
            defense = 50,
            specialAttack = 65,
            specialDefense = 60,
            speed = 44
        ),
        genderRatio = 0.5
    )
    val VANILLISH = Species(
        name = "vanillish",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 51,
            attack = 65,
            defense = 65,
            specialAttack = 80,
            specialDefense = 75,
            speed = 59
        ),
        genderRatio = 0.5
    )
    val VANILLUXE = Species(
        name = "vanilluxe",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 71,
            attack = 95,
            defense = 85,
            specialAttack = 110,
            specialDefense = 95,
            speed = 79
        ),
        genderRatio = 0.5
    )
    val DEERLING = Species(
        name = "deerling",
        type1 = Normal,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 50,
            specialAttack = 40,
            specialDefense = 50,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val SAWSBUCK = Species(
        name = "sawsbuck",
        type1 = Normal,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 80,
            attack = 100,
            defense = 70,
            specialAttack = 60,
            specialDefense = 70,
            speed = 95
        ),
        genderRatio = 0.5
    )
    val EMOLGA = Species(
        name = "emolga",
        type1 = Electric,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 75,
            defense = 60,
            specialAttack = 75,
            specialDefense = 60,
            speed = 103
        ),
        genderRatio = 0.5
    )
    val KARRABLAST = Species(
        name = "karrablast",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 75,
            defense = 45,
            specialAttack = 40,
            specialDefense = 45,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val ESCAVALIER = Species(
        name = "escavalier",
        type1 = Bug,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 70,
            attack = 135,
            defense = 105,
            specialAttack = 60,
            specialDefense = 105,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val FOONGUS = Species(
        name = "foongus",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 69,
            attack = 55,
            defense = 45,
            specialAttack = 55,
            specialDefense = 55,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val AMOONGUSS = Species(
        name = "amoonguss",
        type1 = Grass,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 114,
            attack = 85,
            defense = 70,
            specialAttack = 85,
            specialDefense = 80,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val FRILLISH = Species(
        name = "frillish",
        type1 = Water,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 55,
            attack = 40,
            defense = 50,
            specialAttack = 65,
            specialDefense = 85,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val JELLICENT = Species(
        name = "jellicent",
        type1 = Water,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 100,
            attack = 60,
            defense = 70,
            specialAttack = 85,
            specialDefense = 105,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val ALOMOMOLA = Species(
        name = "alomomola",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 165,
            attack = 75,
            defense = 80,
            specialAttack = 40,
            specialDefense = 45,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val JOLTIK = Species(
        name = "joltik",
        type1 = Bug,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 50,
            attack = 47,
            defense = 50,
            specialAttack = 57,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val GALVANTULA = Species(
        name = "galvantula",
        type1 = Bug,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 70,
            attack = 77,
            defense = 60,
            specialAttack = 97,
            specialDefense = 60,
            speed = 108
        ),
        genderRatio = 0.5
    )
    val FERROSEED = Species(
        name = "ferroseed",
        type1 = Grass,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 44,
            attack = 50,
            defense = 91,
            specialAttack = 24,
            specialDefense = 86,
            speed = 10
        ),
        genderRatio = 0.5
    )
    val FERROTHORN = Species(
        name = "ferrothorn",
        type1 = Grass,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 74,
            attack = 94,
            defense = 131,
            specialAttack = 54,
            specialDefense = 116,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val KLINK = Species(
        name = "klink",
        type1 = Steel,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 70,
            specialAttack = 45,
            specialDefense = 60,
            speed = 30
        ),
        genderRatio = null
    )
    val KLANG = Species(
        name = "klang",
        type1 = Steel,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 80,
            defense = 95,
            specialAttack = 70,
            specialDefense = 85,
            speed = 50
        ),
        genderRatio = null
    )
    val KLINKLANG = Species(
        name = "klinklang",
        type1 = Steel,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 100,
            defense = 115,
            specialAttack = 70,
            specialDefense = 85,
            speed = 90
        ),
        genderRatio = null
    )
    val TYNAMO = Species(
        name = "tynamo",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 35,
            attack = 55,
            defense = 40,
            specialAttack = 45,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val EELEKTRIK = Species(
        name = "eelektrik",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 85,
            defense = 70,
            specialAttack = 75,
            specialDefense = 70,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val EELEKTROSS = Species(
        name = "eelektross",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 115,
            defense = 80,
            specialAttack = 105,
            specialDefense = 80,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val ELGYEM = Species(
        name = "elgyem",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 55,
            defense = 55,
            specialAttack = 85,
            specialDefense = 55,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val BEHEEYEM = Species(
        name = "beheeyem",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 75,
            defense = 75,
            specialAttack = 125,
            specialDefense = 95,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val LITWICK = Species(
        name = "litwick",
        type1 = Ghost,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 50,
            attack = 30,
            defense = 55,
            specialAttack = 65,
            specialDefense = 55,
            speed = 20
        ),
        genderRatio = 0.5
    )
    val LAMPENT = Species(
        name = "lampent",
        type1 = Ghost,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 60,
            attack = 40,
            defense = 60,
            specialAttack = 95,
            specialDefense = 60,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val CHANDELURE = Species(
        name = "chandelure",
        type1 = Ghost,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 60,
            attack = 55,
            defense = 90,
            specialAttack = 145,
            specialDefense = 90,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val AXEW = Species(
        name = "axew",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 46,
            attack = 87,
            defense = 60,
            specialAttack = 30,
            specialDefense = 40,
            speed = 57
        ),
        genderRatio = 0.5
    )
    val FRAXURE = Species(
        name = "fraxure",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 66,
            attack = 117,
            defense = 70,
            specialAttack = 40,
            specialDefense = 50,
            speed = 67
        ),
        genderRatio = 0.5
    )
    val HAXORUS = Species(
        name = "haxorus",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 76,
            attack = 147,
            defense = 90,
            specialAttack = 60,
            specialDefense = 70,
            speed = 97
        ),
        genderRatio = 0.5
    )
    val CUBCHOO = Species(
        name = "cubchoo",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 70,
            defense = 40,
            specialAttack = 60,
            specialDefense = 40,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val BEARTIC = Species(
        name = "beartic",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 130,
            defense = 80,
            specialAttack = 70,
            specialDefense = 80,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val CRYOGONAL = Species(
        name = "cryogonal",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 50,
            defense = 50,
            specialAttack = 95,
            specialDefense = 135,
            speed = 105
        ),
        genderRatio = null
    )
    val SHELMET = Species(
        name = "shelmet",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 40,
            defense = 85,
            specialAttack = 40,
            specialDefense = 65,
            speed = 25
        ),
        genderRatio = 0.5
    )
    val ACCELGOR = Species(
        name = "accelgor",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 80,
            attack = 70,
            defense = 40,
            specialAttack = 100,
            specialDefense = 60,
            speed = 145
        ),
        genderRatio = 0.5
    )
    val STUNFISK = Species(
        name = "stunfisk",
        type1 = Ground,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 109,
            attack = 66,
            defense = 84,
            specialAttack = 81,
            specialDefense = 99,
            speed = 32
        ),
        genderRatio = 0.5
    )
    val MIENFOO = Species(
        name = "mienfoo",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 85,
            defense = 50,
            specialAttack = 55,
            specialDefense = 50,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val MIENSHAO = Species(
        name = "mienshao",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 125,
            defense = 60,
            specialAttack = 95,
            specialDefense = 60,
            speed = 105
        ),
        genderRatio = 0.5
    )
    val DRUDDIGON = Species(
        name = "druddigon",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 77,
            attack = 120,
            defense = 90,
            specialAttack = 60,
            specialDefense = 90,
            speed = 48
        ),
        genderRatio = 0.5
    )
    val GOLETT = Species(
        name = "golett",
        type1 = Ground,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 59,
            attack = 74,
            defense = 50,
            specialAttack = 35,
            specialDefense = 50,
            speed = 35
        ),
        genderRatio = null
    )
    val GOLURK = Species(
        name = "golurk",
        type1 = Ground,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 89,
            attack = 124,
            defense = 80,
            specialAttack = 55,
            specialDefense = 80,
            speed = 55
        ),
        genderRatio = null
    )
    val PAWNIARD = Species(
        name = "pawniard",
        type1 = Dark,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 45,
            attack = 85,
            defense = 70,
            specialAttack = 40,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val BISHARP = Species(
        name = "bisharp",
        type1 = Dark,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 65,
            attack = 125,
            defense = 100,
            specialAttack = 60,
            specialDefense = 70,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val BOUFFALANT = Species(
        name = "bouffalant",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 110,
            defense = 95,
            specialAttack = 40,
            specialDefense = 95,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val RUFFLET = Species(
        name = "rufflet",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 83,
            defense = 50,
            specialAttack = 37,
            specialDefense = 50,
            speed = 60
        ),
        genderRatio = 0.0
    )
    val BRAVIARY = Species(
        name = "braviary",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 100,
            attack = 123,
            defense = 75,
            specialAttack = 57,
            specialDefense = 75,
            speed = 80
        ),
        genderRatio = 0.0
    )
    val VULLABY = Species(
        name = "vullaby",
        type1 = Dark,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 70,
            attack = 55,
            defense = 75,
            specialAttack = 45,
            specialDefense = 65,
            speed = 60
        ),
        genderRatio = 1.0
    )
    val MANDIBUZZ = Species(
        name = "mandibuzz",
        type1 = Dark,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 110,
            attack = 65,
            defense = 105,
            specialAttack = 55,
            specialDefense = 95,
            speed = 80
        ),
        genderRatio = 1.0
    )
    val HEATMOR = Species(
        name = "heatmor",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 85,
            attack = 97,
            defense = 66,
            specialAttack = 105,
            specialDefense = 66,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val DURANT = Species(
        name = "durant",
        type1 = Bug,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 58,
            attack = 109,
            defense = 112,
            specialAttack = 48,
            specialDefense = 48,
            speed = 109
        ),
        genderRatio = 0.5
    )
    val DEINO = Species(
        name = "deino",
        type1 = Dark,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 52,
            attack = 65,
            defense = 50,
            specialAttack = 45,
            specialDefense = 50,
            speed = 38
        ),
        genderRatio = 0.5
    )
    val ZWEILOUS = Species(
        name = "zweilous",
        type1 = Dark,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 72,
            attack = 85,
            defense = 70,
            specialAttack = 65,
            specialDefense = 70,
            speed = 58
        ),
        genderRatio = 0.5
    )
    val HYDREIGON = Species(
        name = "hydreigon",
        type1 = Dark,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 92,
            attack = 105,
            defense = 90,
            specialAttack = 125,
            specialDefense = 90,
            speed = 98
        ),
        genderRatio = 0.5
    )
    val LARVESTA = Species(
        name = "larvesta",
        type1 = Bug,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 55,
            attack = 85,
            defense = 55,
            specialAttack = 50,
            specialDefense = 55,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val VOLCARONA = Species(
        name = "volcarona",
        type1 = Bug,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 85,
            attack = 60,
            defense = 65,
            specialAttack = 135,
            specialDefense = 105,
            speed = 100
        ),
        genderRatio = 0.5
    )
    val COBALION = Species(
        name = "cobalion",
        type1 = Steel,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 91,
            attack = 90,
            defense = 129,
            specialAttack = 90,
            specialDefense = 72,
            speed = 108
        ),
        genderRatio = null
    )
    val TERRAKION = Species(
        name = "terrakion",
        type1 = Rock,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 91,
            attack = 129,
            defense = 90,
            specialAttack = 72,
            specialDefense = 90,
            speed = 108
        ),
        genderRatio = null
    )
    val VIRIZION = Species(
        name = "virizion",
        type1 = Grass,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 91,
            attack = 90,
            defense = 72,
            specialAttack = 90,
            specialDefense = 129,
            speed = 108
        ),
        genderRatio = null
    )
    val TORNADUS = Species(
        name = "tornadus",
        type1 = Flying,
        type2 = null,
        baseStats = BaseStats(
            hp = 79,
            attack = 115,
            defense = 70,
            specialAttack = 125,
            specialDefense = 80,
            speed = 111
        ),
        genderRatio = 0.0
    )
    val THUNDURUS = Species(
        name = "thundurus",
        type1 = Electric,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 79,
            attack = 115,
            defense = 70,
            specialAttack = 125,
            specialDefense = 80,
            speed = 111
        ),
        genderRatio = 0.0
    )
    val RESHIRAM = Species(
        name = "reshiram",
        type1 = Dragon,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 100,
            attack = 120,
            defense = 100,
            specialAttack = 150,
            specialDefense = 120,
            speed = 90
        ),
        genderRatio = null
    )
    val ZEKROM = Species(
        name = "zekrom",
        type1 = Dragon,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 100,
            attack = 150,
            defense = 120,
            specialAttack = 120,
            specialDefense = 100,
            speed = 90
        ),
        genderRatio = null
    )
    val LANDORUS = Species(
        name = "landorus",
        type1 = Ground,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 89,
            attack = 125,
            defense = 90,
            specialAttack = 115,
            specialDefense = 80,
            speed = 101
        ),
        genderRatio = 0.0
    )
    val KYUREM = Species(
        name = "kyurem",
        type1 = Dragon,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 125,
            attack = 130,
            defense = 90,
            specialAttack = 130,
            specialDefense = 90,
            speed = 95
        ),
        genderRatio = null
    )
    val KELDEO = Species(
        name = "keldeo",
        type1 = Water,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 91,
            attack = 72,
            defense = 90,
            specialAttack = 129,
            specialDefense = 90,
            speed = 108
        ),
        genderRatio = null
    )
    val MELOETTA = Species(
        name = "meloetta",
        type1 = Normal,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 100,
            attack = 77,
            defense = 77,
            specialAttack = 128,
            specialDefense = 128,
            speed = 90
        ),
        genderRatio = null
    )
    val GENESECT = Species(
        name = "genesect",
        type1 = Bug,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 71,
            attack = 120,
            defense = 95,
            specialAttack = 120,
            specialDefense = 95,
            speed = 99
        ),
        genderRatio = null
    )
    val CHESPIN = Species(
        name = "chespin",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 56,
            attack = 61,
            defense = 65,
            specialAttack = 48,
            specialDefense = 45,
            speed = 38
        ),
        genderRatio = 0.125
    )
    val QUILLADIN = Species(
        name = "quilladin",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 61,
            attack = 78,
            defense = 95,
            specialAttack = 56,
            specialDefense = 58,
            speed = 57
        ),
        genderRatio = 0.125
    )
    val CHESNAUGHT = Species(
        name = "chesnaught",
        type1 = Grass,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 88,
            attack = 107,
            defense = 122,
            specialAttack = 74,
            specialDefense = 75,
            speed = 64
        ),
        genderRatio = 0.125
    )
    val FENNEKIN = Species(
        name = "fennekin",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 40,
            specialAttack = 62,
            specialDefense = 60,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val BRAIXEN = Species(
        name = "braixen",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 59,
            attack = 59,
            defense = 58,
            specialAttack = 90,
            specialDefense = 70,
            speed = 73
        ),
        genderRatio = 0.125
    )
    val DELPHOX = Species(
        name = "delphox",
        type1 = Fire,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 75,
            attack = 69,
            defense = 72,
            specialAttack = 114,
            specialDefense = 100,
            speed = 104
        ),
        genderRatio = 0.125
    )
    val FROAKIE = Species(
        name = "froakie",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 41,
            attack = 56,
            defense = 40,
            specialAttack = 62,
            specialDefense = 44,
            speed = 71
        ),
        genderRatio = 0.125
    )
    val FROGADIER = Species(
        name = "frogadier",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 54,
            attack = 63,
            defense = 52,
            specialAttack = 83,
            specialDefense = 56,
            speed = 97
        ),
        genderRatio = 0.125
    )
    val GRENINJA = Species(
        name = "greninja",
        type1 = Water,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 72,
            attack = 95,
            defense = 67,
            specialAttack = 103,
            specialDefense = 71,
            speed = 122
        ),
        genderRatio = 0.125
    )
    val BUNNELBY = Species(
        name = "bunnelby",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 38,
            attack = 36,
            defense = 38,
            specialAttack = 32,
            specialDefense = 36,
            speed = 57
        ),
        genderRatio = 0.5
    )
    val DIGGERSBY = Species(
        name = "diggersby",
        type1 = Normal,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 85,
            attack = 56,
            defense = 77,
            specialAttack = 50,
            specialDefense = 77,
            speed = 78
        ),
        genderRatio = 0.5
    )
    val FLETCHLING = Species(
        name = "fletchling",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 45,
            attack = 50,
            defense = 43,
            specialAttack = 40,
            specialDefense = 38,
            speed = 62
        ),
        genderRatio = 0.5
    )
    val FLETCHINDER = Species(
        name = "fletchinder",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 62,
            attack = 73,
            defense = 55,
            specialAttack = 56,
            specialDefense = 52,
            speed = 84
        ),
        genderRatio = 0.5
    )
    val TALONFLAME = Species(
        name = "talonflame",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 78,
            attack = 81,
            defense = 71,
            specialAttack = 74,
            specialDefense = 69,
            speed = 126
        ),
        genderRatio = 0.5
    )
    val SCATTERBUG = Species(
        name = "scatterbug",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 38,
            attack = 35,
            defense = 40,
            specialAttack = 27,
            specialDefense = 25,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val SPEWPA = Species(
        name = "spewpa",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 22,
            defense = 60,
            specialAttack = 27,
            specialDefense = 30,
            speed = 29
        ),
        genderRatio = 0.5
    )
    val VIVILLON = Species(
        name = "vivillon",
        type1 = Bug,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 80,
            attack = 52,
            defense = 50,
            specialAttack = 90,
            specialDefense = 50,
            speed = 89
        ),
        genderRatio = 0.5
    )
    val LITLEO = Species(
        name = "litleo",
        type1 = Fire,
        type2 = Normal,
        baseStats = BaseStats(
            hp = 62,
            attack = 50,
            defense = 58,
            specialAttack = 73,
            specialDefense = 54,
            speed = 72
        ),
        genderRatio = 0.875
    )
    val PYROAR = Species(
        name = "pyroar",
        type1 = Fire,
        type2 = Normal,
        baseStats = BaseStats(
            hp = 86,
            attack = 68,
            defense = 72,
            specialAttack = 109,
            specialDefense = 66,
            speed = 106
        ),
        genderRatio = 0.875
    )
    val FLABEBE = Species(
        name = "flabebe",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 44,
            attack = 38,
            defense = 39,
            specialAttack = 61,
            specialDefense = 79,
            speed = 42
        ),
        genderRatio = 1.0
    )
    val FLOETTE = Species(
        name = "floette",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 54,
            attack = 45,
            defense = 47,
            specialAttack = 75,
            specialDefense = 98,
            speed = 52
        ),
        genderRatio = 1.0
    )
    val FLORGES = Species(
        name = "florges",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 78,
            attack = 65,
            defense = 68,
            specialAttack = 112,
            specialDefense = 154,
            speed = 75
        ),
        genderRatio = 1.0
    )
    val SKIDDO = Species(
        name = "skiddo",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 66,
            attack = 65,
            defense = 48,
            specialAttack = 62,
            specialDefense = 57,
            speed = 52
        ),
        genderRatio = 0.5
    )
    val GOGOAT = Species(
        name = "gogoat",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 123,
            attack = 100,
            defense = 62,
            specialAttack = 97,
            specialDefense = 81,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val PANCHAM = Species(
        name = "pancham",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 67,
            attack = 82,
            defense = 62,
            specialAttack = 46,
            specialDefense = 48,
            speed = 43
        ),
        genderRatio = 0.5
    )
    val PANGORO = Species(
        name = "pangoro",
        type1 = Fighting,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 95,
            attack = 124,
            defense = 78,
            specialAttack = 69,
            specialDefense = 71,
            speed = 58
        ),
        genderRatio = 0.5
    )
    val FURFROU = Species(
        name = "furfrou",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 80,
            defense = 60,
            specialAttack = 65,
            specialDefense = 90,
            speed = 102
        ),
        genderRatio = 0.5
    )
    val ESPURR = Species(
        name = "espurr",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 62,
            attack = 48,
            defense = 54,
            specialAttack = 63,
            specialDefense = 60,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val MEOWSTIC = Species(
        name = "meowstic",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 74,
            attack = 48,
            defense = 76,
            specialAttack = 83,
            specialDefense = 81,
            speed = 104
        ),
        genderRatio = 0.5
    )
    val HONEDGE = Species(
        name = "honedge",
        type1 = Steel,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 45,
            attack = 80,
            defense = 100,
            specialAttack = 35,
            specialDefense = 37,
            speed = 28
        ),
        genderRatio = 0.5
    )
    val DOUBLADE = Species(
        name = "doublade",
        type1 = Steel,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 59,
            attack = 110,
            defense = 150,
            specialAttack = 45,
            specialDefense = 49,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val AEGISLASH = Species(
        name = "aegislash",
        type1 = Steel,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 60,
            attack = 50,
            defense = 150,
            specialAttack = 50,
            specialDefense = 150,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val SPRITZEE = Species(
        name = "spritzee",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 78,
            attack = 52,
            defense = 60,
            specialAttack = 63,
            specialDefense = 65,
            speed = 23
        ),
        genderRatio = 0.5
    )
    val AROMATISSE = Species(
        name = "aromatisse",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 101,
            attack = 72,
            defense = 72,
            specialAttack = 99,
            specialDefense = 89,
            speed = 29
        ),
        genderRatio = 0.5
    )
    val SWIRLIX = Species(
        name = "swirlix",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 62,
            attack = 48,
            defense = 66,
            specialAttack = 59,
            specialDefense = 57,
            speed = 49
        ),
        genderRatio = 0.5
    )
    val SLURPUFF = Species(
        name = "slurpuff",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 82,
            attack = 80,
            defense = 86,
            specialAttack = 85,
            specialDefense = 75,
            speed = 72
        ),
        genderRatio = 0.5
    )
    val INKAY = Species(
        name = "inkay",
        type1 = Dark,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 53,
            attack = 54,
            defense = 53,
            specialAttack = 37,
            specialDefense = 46,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val MALAMAR = Species(
        name = "malamar",
        type1 = Dark,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 86,
            attack = 92,
            defense = 88,
            specialAttack = 68,
            specialDefense = 75,
            speed = 73
        ),
        genderRatio = 0.5
    )
    val BINACLE = Species(
        name = "binacle",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 42,
            attack = 52,
            defense = 67,
            specialAttack = 39,
            specialDefense = 56,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val BARBARACLE = Species(
        name = "barbaracle",
        type1 = Rock,
        type2 = Water,
        baseStats = BaseStats(
            hp = 72,
            attack = 105,
            defense = 115,
            specialAttack = 54,
            specialDefense = 86,
            speed = 68
        ),
        genderRatio = 0.5
    )
    val SKRELP = Species(
        name = "skrelp",
        type1 = Poison,
        type2 = Water,
        baseStats = BaseStats(
            hp = 50,
            attack = 60,
            defense = 60,
            specialAttack = 60,
            specialDefense = 60,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val DRAGALGE = Species(
        name = "dragalge",
        type1 = Poison,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 65,
            attack = 75,
            defense = 90,
            specialAttack = 97,
            specialDefense = 123,
            speed = 44
        ),
        genderRatio = 0.5
    )
    val CLAUNCHER = Species(
        name = "clauncher",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 53,
            defense = 62,
            specialAttack = 58,
            specialDefense = 63,
            speed = 44
        ),
        genderRatio = 0.5
    )
    val CLAWITZER = Species(
        name = "clawitzer",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 71,
            attack = 73,
            defense = 88,
            specialAttack = 120,
            specialDefense = 89,
            speed = 59
        ),
        genderRatio = 0.5
    )
    val HELIOPTILE = Species(
        name = "helioptile",
        type1 = Electric,
        type2 = Normal,
        baseStats = BaseStats(
            hp = 44,
            attack = 38,
            defense = 33,
            specialAttack = 61,
            specialDefense = 43,
            speed = 70
        ),
        genderRatio = 0.5
    )
    val HELIOLISK = Species(
        name = "heliolisk",
        type1 = Electric,
        type2 = Normal,
        baseStats = BaseStats(
            hp = 62,
            attack = 55,
            defense = 52,
            specialAttack = 109,
            specialDefense = 94,
            speed = 109
        ),
        genderRatio = 0.5
    )
    val TYRUNT = Species(
        name = "tyrunt",
        type1 = Rock,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 58,
            attack = 89,
            defense = 77,
            specialAttack = 45,
            specialDefense = 45,
            speed = 48
        ),
        genderRatio = 0.125
    )
    val TYRANTRUM = Species(
        name = "tyrantrum",
        type1 = Rock,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 82,
            attack = 121,
            defense = 119,
            specialAttack = 69,
            specialDefense = 59,
            speed = 71
        ),
        genderRatio = 0.125
    )
    val AMAURA = Species(
        name = "amaura",
        type1 = Rock,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 77,
            attack = 59,
            defense = 50,
            specialAttack = 67,
            specialDefense = 63,
            speed = 46
        ),
        genderRatio = 0.125
    )
    val AURORUS = Species(
        name = "aurorus",
        type1 = Rock,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 123,
            attack = 77,
            defense = 72,
            specialAttack = 99,
            specialDefense = 92,
            speed = 58
        ),
        genderRatio = 0.125
    )
    val SYLVEON = Species(
        name = "sylveon",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 65,
            defense = 65,
            specialAttack = 110,
            specialDefense = 130,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val HAWLUCHA = Species(
        name = "hawlucha",
        type1 = Fighting,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 78,
            attack = 92,
            defense = 75,
            specialAttack = 74,
            specialDefense = 63,
            speed = 118
        ),
        genderRatio = 0.5
    )
    val DEDENNE = Species(
        name = "dedenne",
        type1 = Electric,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 67,
            attack = 58,
            defense = 57,
            specialAttack = 81,
            specialDefense = 67,
            speed = 101
        ),
        genderRatio = 0.5
    )
    val CARBINK = Species(
        name = "carbink",
        type1 = Rock,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 50,
            attack = 50,
            defense = 150,
            specialAttack = 50,
            specialDefense = 150,
            speed = 50
        ),
        genderRatio = null
    )
    val GOOMY = Species(
        name = "goomy",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 50,
            defense = 35,
            specialAttack = 55,
            specialDefense = 75,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val SLIGGOO = Species(
        name = "sliggoo",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 68,
            attack = 75,
            defense = 53,
            specialAttack = 83,
            specialDefense = 113,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val GOODRA = Species(
        name = "goodra",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 90,
            attack = 100,
            defense = 70,
            specialAttack = 110,
            specialDefense = 150,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val KLEFKI = Species(
        name = "klefki",
        type1 = Steel,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 57,
            attack = 80,
            defense = 91,
            specialAttack = 80,
            specialDefense = 87,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val PHANTUMP = Species(
        name = "phantump",
        type1 = Ghost,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 43,
            attack = 70,
            defense = 48,
            specialAttack = 50,
            specialDefense = 60,
            speed = 38
        ),
        genderRatio = 0.5
    )
    val TREVENANT = Species(
        name = "trevenant",
        type1 = Ghost,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 85,
            attack = 110,
            defense = 76,
            specialAttack = 65,
            specialDefense = 82,
            speed = 56
        ),
        genderRatio = 0.5
    )
    val PUMPKABOO = Species(
        name = "pumpkaboo",
        type1 = Ghost,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 49,
            attack = 66,
            defense = 70,
            specialAttack = 44,
            specialDefense = 55,
            speed = 51
        ),
        genderRatio = 0.5
    )
    val GOURGEIST = Species(
        name = "gourgeist",
        type1 = Ghost,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 65,
            attack = 90,
            defense = 122,
            specialAttack = 58,
            specialDefense = 75,
            speed = 84
        ),
        genderRatio = 0.5
    )
    val BERGMITE = Species(
        name = "bergmite",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 69,
            defense = 85,
            specialAttack = 32,
            specialDefense = 35,
            speed = 28
        ),
        genderRatio = 0.5
    )
    val AVALUGG = Species(
        name = "avalugg",
        type1 = Ice,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 117,
            defense = 184,
            specialAttack = 44,
            specialDefense = 46,
            speed = 28
        ),
        genderRatio = 0.5
    )
    val NOIBAT = Species(
        name = "noibat",
        type1 = Flying,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 40,
            attack = 30,
            defense = 35,
            specialAttack = 45,
            specialDefense = 40,
            speed = 55
        ),
        genderRatio = 0.5
    )
    val NOIVERN = Species(
        name = "noivern",
        type1 = Flying,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 85,
            attack = 70,
            defense = 80,
            specialAttack = 97,
            specialDefense = 80,
            speed = 123
        ),
        genderRatio = 0.5
    )
    val XERNEAS = Species(
        name = "xerneas",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 126,
            attack = 131,
            defense = 95,
            specialAttack = 131,
            specialDefense = 98,
            speed = 99
        ),
        genderRatio = null
    )
    val YVELTAL = Species(
        name = "yveltal",
        type1 = Dark,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 126,
            attack = 131,
            defense = 95,
            specialAttack = 131,
            specialDefense = 98,
            speed = 99
        ),
        genderRatio = null
    )
    val ZYGARDE = Species(
        name = "zygarde",
        type1 = Dragon,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 108,
            attack = 100,
            defense = 121,
            specialAttack = 81,
            specialDefense = 95,
            speed = 95
        ),
        genderRatio = null
    )
    val DIANCIE = Species(
        name = "diancie",
        type1 = Rock,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 50,
            attack = 100,
            defense = 150,
            specialAttack = 100,
            specialDefense = 150,
            speed = 50
        ),
        genderRatio = null
    )
    val HOOPA = Species(
        name = "hoopa",
        type1 = Psychic,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 80,
            attack = 110,
            defense = 60,
            specialAttack = 150,
            specialDefense = 130,
            speed = 70
        ),
        genderRatio = null
    )
    val VOLCANION = Species(
        name = "volcanion",
        type1 = Fire,
        type2 = Water,
        baseStats = BaseStats(
            hp = 80,
            attack = 110,
            defense = 120,
            specialAttack = 130,
            specialDefense = 90,
            speed = 70
        ),
        genderRatio = null
    )
    val ROWLET = Species(
        name = "rowlet",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 68,
            attack = 55,
            defense = 55,
            specialAttack = 50,
            specialDefense = 50,
            speed = 42
        ),
        genderRatio = 0.125
    )
    val DARTRIX = Species(
        name = "dartrix",
        type1 = Grass,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 78,
            attack = 75,
            defense = 75,
            specialAttack = 70,
            specialDefense = 70,
            speed = 52
        ),
        genderRatio = 0.125
    )
    val DECIDUEYE = Species(
        name = "decidueye",
        type1 = Grass,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 78,
            attack = 107,
            defense = 75,
            specialAttack = 100,
            specialDefense = 100,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val LITTEN = Species(
        name = "litten",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 65,
            defense = 40,
            specialAttack = 60,
            specialDefense = 40,
            speed = 70
        ),
        genderRatio = 0.125
    )
    val TORRACAT = Species(
        name = "torracat",
        type1 = Fire,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 85,
            defense = 50,
            specialAttack = 80,
            specialDefense = 50,
            speed = 90
        ),
        genderRatio = 0.125
    )
    val INCINEROAR = Species(
        name = "incineroar",
        type1 = Fire,
        type2 = Dark,
        baseStats = BaseStats(
            hp = 95,
            attack = 115,
            defense = 90,
            specialAttack = 80,
            specialDefense = 90,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val POPPLIO = Species(
        name = "popplio",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 50,
            attack = 54,
            defense = 54,
            specialAttack = 66,
            specialDefense = 56,
            speed = 40
        ),
        genderRatio = 0.125
    )
    val BRIONNE = Species(
        name = "brionne",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 60,
            attack = 69,
            defense = 69,
            specialAttack = 91,
            specialDefense = 81,
            speed = 50
        ),
        genderRatio = 0.125
    )
    val PRIMARINA = Species(
        name = "primarina",
        type1 = Water,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 80,
            attack = 74,
            defense = 74,
            specialAttack = 126,
            specialDefense = 116,
            speed = 60
        ),
        genderRatio = 0.125
    )
    val PIKIPEK = Species(
        name = "pikipek",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 35,
            attack = 75,
            defense = 30,
            specialAttack = 30,
            specialDefense = 30,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val TRUMBEAK = Species(
        name = "trumbeak",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 55,
            attack = 85,
            defense = 50,
            specialAttack = 40,
            specialDefense = 50,
            speed = 75
        ),
        genderRatio = 0.5
    )
    val TOUCANNON = Species(
        name = "toucannon",
        type1 = Normal,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 80,
            attack = 120,
            defense = 75,
            specialAttack = 75,
            specialDefense = 75,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val YUNGOOS = Species(
        name = "yungoos",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 48,
            attack = 70,
            defense = 30,
            specialAttack = 30,
            specialDefense = 30,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val GUMSHOOS = Species(
        name = "gumshoos",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 88,
            attack = 110,
            defense = 60,
            specialAttack = 55,
            specialDefense = 60,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val GRUBBIN = Species(
        name = "grubbin",
        type1 = Bug,
        type2 = null,
        baseStats = BaseStats(
            hp = 47,
            attack = 62,
            defense = 45,
            specialAttack = 55,
            specialDefense = 45,
            speed = 46
        ),
        genderRatio = 0.5
    )
    val CHARJABUG = Species(
        name = "charjabug",
        type1 = Bug,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 57,
            attack = 82,
            defense = 95,
            specialAttack = 55,
            specialDefense = 75,
            speed = 36
        ),
        genderRatio = 0.5
    )
    val VIKAVOLT = Species(
        name = "vikavolt",
        type1 = Bug,
        type2 = Electric,
        baseStats = BaseStats(
            hp = 77,
            attack = 70,
            defense = 90,
            specialAttack = 145,
            specialDefense = 75,
            speed = 43
        ),
        genderRatio = 0.5
    )
    val CRABRAWLER = Species(
        name = "crabrawler",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 47,
            attack = 82,
            defense = 57,
            specialAttack = 42,
            specialDefense = 47,
            speed = 63
        ),
        genderRatio = 0.5
    )
    val CRABOMINABLE = Species(
        name = "crabominable",
        type1 = Fighting,
        type2 = Ice,
        baseStats = BaseStats(
            hp = 97,
            attack = 132,
            defense = 77,
            specialAttack = 62,
            specialDefense = 67,
            speed = 43
        ),
        genderRatio = 0.5
    )
    val ORICORIO = Species(
        name = "oricorio",
        type1 = Fire,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 75,
            attack = 70,
            defense = 70,
            specialAttack = 98,
            specialDefense = 70,
            speed = 93
        ),
        genderRatio = 0.75
    )
    val CUTIEFLY = Species(
        name = "cutiefly",
        type1 = Bug,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 40,
            attack = 45,
            defense = 40,
            specialAttack = 55,
            specialDefense = 40,
            speed = 84
        ),
        genderRatio = 0.5
    )
    val RIBOMBEE = Species(
        name = "ribombee",
        type1 = Bug,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 60,
            attack = 55,
            defense = 60,
            specialAttack = 95,
            specialDefense = 70,
            speed = 124
        ),
        genderRatio = 0.5
    )
    val ROCKRUFF = Species(
        name = "rockruff",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 65,
            defense = 40,
            specialAttack = 30,
            specialDefense = 40,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val LYCANROC = Species(
        name = "lycanroc",
        type1 = Rock,
        type2 = null,
        baseStats = BaseStats(
            hp = 75,
            attack = 115,
            defense = 65,
            specialAttack = 55,
            specialDefense = 65,
            speed = 112
        ),
        genderRatio = 0.5
    )
    val WISHIWASHI = Species(
        name = "wishiwashi",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 20,
            defense = 20,
            specialAttack = 25,
            specialDefense = 25,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val MAREANIE = Species(
        name = "mareanie",
        type1 = Poison,
        type2 = Water,
        baseStats = BaseStats(
            hp = 50,
            attack = 53,
            defense = 62,
            specialAttack = 43,
            specialDefense = 52,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val TOXAPEX = Species(
        name = "toxapex",
        type1 = Poison,
        type2 = Water,
        baseStats = BaseStats(
            hp = 50,
            attack = 63,
            defense = 152,
            specialAttack = 53,
            specialDefense = 142,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val MUDBRAY = Species(
        name = "mudbray",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 100,
            defense = 70,
            specialAttack = 45,
            specialDefense = 55,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val MUDSDALE = Species(
        name = "mudsdale",
        type1 = Ground,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 125,
            defense = 100,
            specialAttack = 55,
            specialDefense = 85,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val DEWPIDER = Species(
        name = "dewpider",
        type1 = Water,
        type2 = Bug,
        baseStats = BaseStats(
            hp = 38,
            attack = 40,
            defense = 52,
            specialAttack = 40,
            specialDefense = 72,
            speed = 27
        ),
        genderRatio = 0.5
    )
    val ARAQUANID = Species(
        name = "araquanid",
        type1 = Water,
        type2 = Bug,
        baseStats = BaseStats(
            hp = 68,
            attack = 70,
            defense = 92,
            specialAttack = 50,
            specialDefense = 132,
            speed = 42
        ),
        genderRatio = 0.5
    )
    val FOMANTIS = Species(
        name = "fomantis",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 40,
            attack = 55,
            defense = 35,
            specialAttack = 50,
            specialDefense = 35,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val LURANTIS = Species(
        name = "lurantis",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 70,
            attack = 105,
            defense = 90,
            specialAttack = 80,
            specialDefense = 90,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val MORELULL = Species(
        name = "morelull",
        type1 = Grass,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 40,
            attack = 35,
            defense = 55,
            specialAttack = 65,
            specialDefense = 75,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val SHIINOTIC = Species(
        name = "shiinotic",
        type1 = Grass,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 60,
            attack = 45,
            defense = 80,
            specialAttack = 90,
            specialDefense = 100,
            speed = 30
        ),
        genderRatio = 0.5
    )
    val SALANDIT = Species(
        name = "salandit",
        type1 = Poison,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 48,
            attack = 44,
            defense = 40,
            specialAttack = 71,
            specialDefense = 40,
            speed = 77
        ),
        genderRatio = 0.125
    )
    val SALAZZLE = Species(
        name = "salazzle",
        type1 = Poison,
        type2 = Fire,
        baseStats = BaseStats(
            hp = 68,
            attack = 64,
            defense = 60,
            specialAttack = 111,
            specialDefense = 60,
            speed = 117
        ),
        genderRatio = 1.0
    )
    val STUFFUL = Species(
        name = "stufful",
        type1 = Normal,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 70,
            attack = 75,
            defense = 50,
            specialAttack = 45,
            specialDefense = 50,
            speed = 50
        ),
        genderRatio = 0.5
    )
    val BEWEAR = Species(
        name = "bewear",
        type1 = Normal,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 120,
            attack = 125,
            defense = 80,
            specialAttack = 55,
            specialDefense = 60,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val BOUNSWEET = Species(
        name = "bounsweet",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 42,
            attack = 30,
            defense = 38,
            specialAttack = 30,
            specialDefense = 38,
            speed = 32
        ),
        genderRatio = 1.0
    )
    val STEENEE = Species(
        name = "steenee",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 52,
            attack = 40,
            defense = 48,
            specialAttack = 40,
            specialDefense = 48,
            speed = 62
        ),
        genderRatio = 1.0
    )
    val TSAREENA = Species(
        name = "tsareena",
        type1 = Grass,
        type2 = null,
        baseStats = BaseStats(
            hp = 72,
            attack = 120,
            defense = 98,
            specialAttack = 50,
            specialDefense = 98,
            speed = 72
        ),
        genderRatio = 1.0
    )
    val COMFEY = Species(
        name = "comfey",
        type1 = Fairy,
        type2 = null,
        baseStats = BaseStats(
            hp = 51,
            attack = 52,
            defense = 90,
            specialAttack = 82,
            specialDefense = 110,
            speed = 100
        ),
        genderRatio = 0.75
    )
    val ORANGURU = Species(
        name = "oranguru",
        type1 = Normal,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 90,
            attack = 60,
            defense = 80,
            specialAttack = 90,
            specialDefense = 110,
            speed = 60
        ),
        genderRatio = 0.5
    )
    val PASSIMIAN = Species(
        name = "passimian",
        type1 = Fighting,
        type2 = null,
        baseStats = BaseStats(
            hp = 100,
            attack = 120,
            defense = 90,
            specialAttack = 40,
            specialDefense = 60,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val WIMPOD = Species(
        name = "wimpod",
        type1 = Bug,
        type2 = Water,
        baseStats = BaseStats(
            hp = 25,
            attack = 35,
            defense = 40,
            specialAttack = 20,
            specialDefense = 30,
            speed = 80
        ),
        genderRatio = 0.5
    )
    val GOLISOPOD = Species(
        name = "golisopod",
        type1 = Bug,
        type2 = Water,
        baseStats = BaseStats(
            hp = 75,
            attack = 125,
            defense = 140,
            specialAttack = 60,
            specialDefense = 90,
            speed = 40
        ),
        genderRatio = 0.5
    )
    val SANDYGAST = Species(
        name = "sandygast",
        type1 = Ghost,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 55,
            attack = 55,
            defense = 80,
            specialAttack = 70,
            specialDefense = 45,
            speed = 15
        ),
        genderRatio = 0.5
    )
    val PALOSSAND = Species(
        name = "palossand",
        type1 = Ghost,
        type2 = Ground,
        baseStats = BaseStats(
            hp = 85,
            attack = 75,
            defense = 110,
            specialAttack = 100,
            specialDefense = 75,
            speed = 35
        ),
        genderRatio = 0.5
    )
    val PYUKUMUKU = Species(
        name = "pyukumuku",
        type1 = Water,
        type2 = null,
        baseStats = BaseStats(
            hp = 55,
            attack = 60,
            defense = 130,
            specialAttack = 30,
            specialDefense = 130,
            speed = 5
        ),
        genderRatio = 0.5
    )
    val TYPE_NULL = Species(
        name = "type-null",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 95,
            defense = 95,
            specialAttack = 95,
            specialDefense = 95,
            speed = 59
        ),
        genderRatio = null
    )
    val SILVALLY = Species(
        name = "silvally",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 95,
            attack = 95,
            defense = 95,
            specialAttack = 95,
            specialDefense = 95,
            speed = 95
        ),
        genderRatio = null
    )
    val MINIOR = Species(
        name = "minior",
        type1 = Rock,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 60,
            attack = 60,
            defense = 100,
            specialAttack = 60,
            specialDefense = 100,
            speed = 60
        ),
        genderRatio = null
    )
    val KOMALA = Species(
        name = "komala",
        type1 = Normal,
        type2 = null,
        baseStats = BaseStats(
            hp = 65,
            attack = 115,
            defense = 65,
            specialAttack = 75,
            specialDefense = 95,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val TURTONATOR = Species(
        name = "turtonator",
        type1 = Fire,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 60,
            attack = 78,
            defense = 135,
            specialAttack = 91,
            specialDefense = 85,
            speed = 36
        ),
        genderRatio = 0.5
    )
    val TOGEDEMARU = Species(
        name = "togedemaru",
        type1 = Electric,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 65,
            attack = 98,
            defense = 63,
            specialAttack = 40,
            specialDefense = 73,
            speed = 96
        ),
        genderRatio = 0.5
    )
    val MIMIKYU = Species(
        name = "mimikyu",
        type1 = Ghost,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 55,
            attack = 90,
            defense = 80,
            specialAttack = 50,
            specialDefense = 105,
            speed = 96
        ),
        genderRatio = 0.5
    )
    val BRUXISH = Species(
        name = "bruxish",
        type1 = Water,
        type2 = Psychic,
        baseStats = BaseStats(
            hp = 68,
            attack = 105,
            defense = 70,
            specialAttack = 70,
            specialDefense = 70,
            speed = 92
        ),
        genderRatio = 0.5
    )
    val DRAMPA = Species(
        name = "drampa",
        type1 = Normal,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 78,
            attack = 60,
            defense = 85,
            specialAttack = 135,
            specialDefense = 91,
            speed = 36
        ),
        genderRatio = 0.5
    )
    val DHELMISE = Species(
        name = "dhelmise",
        type1 = Ghost,
        type2 = Grass,
        baseStats = BaseStats(
            hp = 70,
            attack = 131,
            defense = 100,
            specialAttack = 86,
            specialDefense = 90,
            speed = 40
        ),
        genderRatio = null
    )
    val JANGMO_O = Species(
        name = "jangmo-o",
        type1 = Dragon,
        type2 = null,
        baseStats = BaseStats(
            hp = 45,
            attack = 55,
            defense = 65,
            specialAttack = 45,
            specialDefense = 45,
            speed = 45
        ),
        genderRatio = 0.5
    )
    val HAKAMO_O = Species(
        name = "hakamo-o",
        type1 = Dragon,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 55,
            attack = 75,
            defense = 90,
            specialAttack = 65,
            specialDefense = 70,
            speed = 65
        ),
        genderRatio = 0.5
    )
    val KOMMO_O = Species(
        name = "kommo-o",
        type1 = Dragon,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 75,
            attack = 110,
            defense = 125,
            specialAttack = 100,
            specialDefense = 105,
            speed = 85
        ),
        genderRatio = 0.5
    )
    val TAPU_KOKO = Species(
        name = "tapu-koko",
        type1 = Electric,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 70,
            attack = 115,
            defense = 85,
            specialAttack = 95,
            specialDefense = 75,
            speed = 130
        ),
        genderRatio = null
    )
    val TAPU_LELE = Species(
        name = "tapu-lele",
        type1 = Psychic,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 70,
            attack = 85,
            defense = 75,
            specialAttack = 130,
            specialDefense = 115,
            speed = 95
        ),
        genderRatio = null
    )
    val TAPU_BULU = Species(
        name = "tapu-bulu",
        type1 = Grass,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 70,
            attack = 130,
            defense = 115,
            specialAttack = 85,
            specialDefense = 95,
            speed = 75
        ),
        genderRatio = null
    )
    val TAPU_FINI = Species(
        name = "tapu-fini",
        type1 = Water,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 70,
            attack = 75,
            defense = 115,
            specialAttack = 95,
            specialDefense = 130,
            speed = 85
        ),
        genderRatio = null
    )
    val COSMOG = Species(
        name = "cosmog",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 43,
            attack = 29,
            defense = 31,
            specialAttack = 29,
            specialDefense = 31,
            speed = 37
        ),
        genderRatio = null
    )
    val COSMOEM = Species(
        name = "cosmoem",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 43,
            attack = 29,
            defense = 131,
            specialAttack = 29,
            specialDefense = 131,
            speed = 37
        ),
        genderRatio = null
    )
    val SOLGALEO = Species(
        name = "solgaleo",
        type1 = Psychic,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 137,
            attack = 137,
            defense = 107,
            specialAttack = 113,
            specialDefense = 89,
            speed = 97
        ),
        genderRatio = null
    )
    val LUNALA = Species(
        name = "lunala",
        type1 = Psychic,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 137,
            attack = 113,
            defense = 89,
            specialAttack = 137,
            specialDefense = 107,
            speed = 97
        ),
        genderRatio = null
    )
    val NIHILEGO = Species(
        name = "nihilego",
        type1 = Rock,
        type2 = Poison,
        baseStats = BaseStats(
            hp = 109,
            attack = 53,
            defense = 47,
            specialAttack = 127,
            specialDefense = 131,
            speed = 103
        ),
        genderRatio = null
    )
    val BUZZWOLE = Species(
        name = "buzzwole",
        type1 = Bug,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 107,
            attack = 139,
            defense = 139,
            specialAttack = 53,
            specialDefense = 53,
            speed = 79
        ),
        genderRatio = null
    )
    val PHEROMOSA = Species(
        name = "pheromosa",
        type1 = Bug,
        type2 = Fighting,
        baseStats = BaseStats(
            hp = 71,
            attack = 137,
            defense = 37,
            specialAttack = 137,
            specialDefense = 37,
            speed = 151
        ),
        genderRatio = null
    )
    val XURKITREE = Species(
        name = "xurkitree",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 83,
            attack = 89,
            defense = 71,
            specialAttack = 173,
            specialDefense = 71,
            speed = 83
        ),
        genderRatio = null
    )
    val CELESTEELA = Species(
        name = "celesteela",
        type1 = Steel,
        type2 = Flying,
        baseStats = BaseStats(
            hp = 97,
            attack = 101,
            defense = 103,
            specialAttack = 107,
            specialDefense = 101,
            speed = 61
        ),
        genderRatio = null
    )
    val KARTANA = Species(
        name = "kartana",
        type1 = Grass,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 59,
            attack = 181,
            defense = 131,
            specialAttack = 59,
            specialDefense = 31,
            speed = 109
        ),
        genderRatio = null
    )
    val GUZZLORD = Species(
        name = "guzzlord",
        type1 = Dark,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 223,
            attack = 101,
            defense = 53,
            specialAttack = 97,
            specialDefense = 53,
            speed = 43
        ),
        genderRatio = null
    )
    val NECROZMA = Species(
        name = "necrozma",
        type1 = Psychic,
        type2 = null,
        baseStats = BaseStats(
            hp = 97,
            attack = 107,
            defense = 101,
            specialAttack = 127,
            specialDefense = 89,
            speed = 79
        ),
        genderRatio = null
    )
    val MAGEARNA = Species(
        name = "magearna",
        type1 = Steel,
        type2 = Fairy,
        baseStats = BaseStats(
            hp = 80,
            attack = 95,
            defense = 115,
            specialAttack = 130,
            specialDefense = 115,
            speed = 65
        ),
        genderRatio = null
    )
    val MARSHADOW = Species(
        name = "marshadow",
        type1 = Fighting,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 90,
            attack = 125,
            defense = 80,
            specialAttack = 90,
            specialDefense = 90,
            speed = 125
        ),
        genderRatio = null
    )
    val POIPOLE = Species(
        name = "poipole",
        type1 = Poison,
        type2 = null,
        baseStats = BaseStats(
            hp = 67,
            attack = 73,
            defense = 67,
            specialAttack = 73,
            specialDefense = 67,
            speed = 73
        ),
        genderRatio = null
    )
    val NAGANADEL = Species(
        name = "naganadel",
        type1 = Poison,
        type2 = Dragon,
        baseStats = BaseStats(
            hp = 73,
            attack = 73,
            defense = 73,
            specialAttack = 127,
            specialDefense = 73,
            speed = 121
        ),
        genderRatio = null
    )
    val STAKATAKA = Species(
        name = "stakataka",
        type1 = Rock,
        type2 = Steel,
        baseStats = BaseStats(
            hp = 61,
            attack = 131,
            defense = 211,
            specialAttack = 53,
            specialDefense = 101,
            speed = 13
        ),
        genderRatio = null
    )
    val BLACEPHALON = Species(
        name = "blacephalon",
        type1 = Fire,
        type2 = Ghost,
        baseStats = BaseStats(
            hp = 53,
            attack = 127,
            defense = 53,
            specialAttack = 151,
            specialDefense = 79,
            speed = 107
        ),
        genderRatio = null
    )
    val ZERAORA = Species(
        name = "zeraora",
        type1 = Electric,
        type2 = null,
        baseStats = BaseStats(
            hp = 88,
            attack = 112,
            defense = 75,
            specialAttack = 102,
            specialDefense = 80,
            speed = 143
        ),
        genderRatio = null
    )
}