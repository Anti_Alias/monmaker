package monmaker.event

/**
 * Fired when the Game's promptor opens.
 */
class PromptOpenEvent : Event

/**
 * Fired when the Game's promptor closes.
 */
class PromptCloseEvent : Event