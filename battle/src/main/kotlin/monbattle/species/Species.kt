package monbattle.species

import monbattle.exceptions.BattleException
import monbattle.type.Type

/**
 * Represents a species of monster.
 * @property name Name of the species.
 * @property type1 Primary type.
 * @property type2 Secondary type. Optional.
 * @property baseStats Base stats of the species.
 * @property genderRatio Percentage of species that is Female.
 * 0.5 means half are female. This is the default value.
 * 1.0 means all are female.
 */
data class Species(
    val name: String,
    val type1: Type,
    val type2: Type?,
    val baseStats: BaseStats,
    val genderRatio: Double? = 0.5
) {

    init {
        if(genderRatio != null && (genderRatio < 0.0 || genderRatio > 1.0))
            throw BattleException("Gender ratio for species must either be null or be in range [0, 1]. Got $genderRatio.")
    }

    /**
     * If true, the species is considered genderless.
     */
    val isGenderless: Boolean get() = genderRatio == null
}