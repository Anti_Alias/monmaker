package monbattle.battle

import monbattle.command.Command
import monbattle.command.CommandMapper
import monbattle.command.SinglesCommandMapper
import monbattle.decision.DecisionFilter
import monbattle.decision.SinglesDecisionFilter
import monbattle.event.*
import monbattle.exceptions.BattleException
import java.util.*

/**
 * Represents a battle between two teams.
 *
 * @param teams All teams in the [Battle].
 * @param eventListener Listener of all [Event]s in the [Battle].
 * @param commandMapper Mapper of all [Command]s in the [Battle].
 * @param decisionFilter Filter of all [Decision]s in the [Battle].
 */
class Battle(
    teams: Set<Team>,
    val eventListener: (Event, Battle)->Unit,
    val commandMapper: CommandMapper,
    val decisionFilter: DecisionFilter
) {
    private var started: Boolean = false
    private var turn: Int = 1
    private var commandIndex: Int = -1
    private val commands: List<Command> = mutableListOf()
    private val events: Queue<Event> = LinkedList()

    /**
     * Starts the battle.
     */
    fun start() {
        if(started) throw BattleException("Battle already started")
        started = true
        fire(TurnStartedEvent(turn))
    }

    /**
     * All teams in the [Battle].
     */
    val teams: Set<Team> = consumeTeams(teams)

    /**
     * @return true if the battle is a draw.
     */
    val isDraw: Boolean get() = teams.find { !it.isDefeated } == null

    /**
     * @return true if the [Battle] is over.
     */
    val isOver: Boolean get() = victor != null || isDraw

    /**
     * @return Victor [Team] of the [Battle].
     * Null if battle is still ongoing of if it's a draw.
     */
    val victor: Team? get() {
        var nonDefeatedTeam: Team? = null
        for(team in teams) {
            if(!team.isDefeated) {
                if(nonDefeatedTeam == null) nonDefeatedTeam = team
                else return null
            }
        }
        return nonDefeatedTeam
    }

    /**
     * Enqueues an [Event] in the [Battle], then immediately handles it if
     * no other [Event]s are enqueued.
     */
    fun fire(event: Event) {
        events.add(event)
        val notProcessingEvents = events.size == 1
        if(notProcessingEvents) do {
            val nextEvent = events.peek()
            on(nextEvent)
            events.remove()
        }
        while(events.isNotEmpty())
    }

    /**
     * Fires an event in the [Battle].
     */
    private fun on(event: Event) {
        eventListener(event, this)
        when(event) {
            is TurnStartedEvent -> handle(event)
            is DecisionPerformedEvent -> handle(event)
            is DecisionRequestedEvent -> handle(event)
        }
        for(team in teams)
            team.on(event, this)
    }

    /**
     * Handles a turn starting by requesting that all teams make a decision.
     */
    private fun handle(event: TurnStartedEvent) {
        for(team in teams)
            fire(DecisionRequestedEvent(team.name))
    }

    private fun handle(event: DecisionRequestedEvent) {

    }

    /**
     * Handles a decision made by a team
     */
    private fun handle(event: DecisionPerformedEvent) {
    }

    /**
     * Reads teams provided, validates them,
     * then returns them in an immutable [Set].
     */
    private fun consumeTeams(teams: Collection<Team>): Set<Team> {

        // Stores teams
        val newTeams = mutableSetOf<Team>()
        for(team in teams) {
            if(newTeams.any { it.name == team.name })
                throw BattleException("Multiple teams with name '${team.name}' found")
            newTeams.add(team)
        }

        // Done
        return newTeams
    }

    companion object {

        /**
         * Creates a team.
         */
        @JvmOverloads fun createSingles(
            firstTeam: Team,
            secondTeam: Team,
            eventListener: (Event, Battle)->Unit,
            maxMonsters: Int = 6
        ) = Battle(
            teams = setOf(firstTeam, secondTeam),
            eventListener = eventListener,
            decisionFilter = SinglesDecisionFilter(),
            commandMapper = SinglesCommandMapper()
        )
    }
}