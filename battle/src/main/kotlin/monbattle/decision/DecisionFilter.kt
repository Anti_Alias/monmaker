package monbattle.decision

import monbattle.battle.Battle

@FunctionalInterface
interface DecisionFilter {

    /**
     * Filters a decision.
     * If decision fails, an event is likely to be propagated through [battle] regarding the failure.
     *
     * @return true if decision is acceptable.
     */
    fun filter(decision: Decision, battle: Battle): Boolean
}