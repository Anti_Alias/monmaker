package monbattle.move

import monbattle.battle.Battle
import monbattle.battle.Team
import monbattle.exceptions.BattleException
import monbattle.monster.Monster

/**
 * Move that can be executed by a [Monster].
 *
 * @property info Immutable information about this [Move].
 * @property behavior Object responsible for the behavior of this [Move].
 */
class Move(val info: MoveInfo, val behavior: Behavior = Behavior.createDefault(info.meta.category)) {

    /**
     * Current PP of this move.
     */
    var currentPP: Int = info.pp
        set(value) {
            if(field < 0) throw BattleException("PP must be >= 0. Got $value")
            else if(field < info.pp) throw BattleException("PP must be <= ${info.pp}. Got $value")
            field = value
        }

    /**
     * Uses this [Move] in battle.
     *
     * @param battle [Battle] this takes place in.
     * @param team [Team] of the monster that is using this [Move].
     * @param monster [Monster] that is using this [Move].
     * @param targets List of [Target]s that this move should hit.
     */
    fun use(battle: Battle, team: Team, monster: Monster, targets: List<Target>) {

    }
}