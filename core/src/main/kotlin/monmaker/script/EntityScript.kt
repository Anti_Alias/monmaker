package monmaker.script

import monmaker.entity.Entity
import monmaker.exceptions.MonmakerException
import monmaker.world.World

/**
 * Represents a [Script] that runs from the perspective of an [Entity].
 */
abstract class EntityScript: Script() {

    internal var _entity: Entity? = null

    /**
     * Entity this script belongs to.
     */
    val entity: Entity
        get() = _entity ?: throw MonmakerException(
        "Script '${javaClass.name}' could not access its entity because it did not belong to one."
    )

    /**
     * World this script belongs to.
     */
    override val world: World get() = entity.world
}