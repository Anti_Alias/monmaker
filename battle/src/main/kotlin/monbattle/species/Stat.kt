package monbattle.species

/**
 * Represents an enum value for a stat.
 */
enum class Stat { HP, ATTACK, DEFENSE, SPECIAL_ATTACK, SPECIAL_DEFENSE, SPEED }