package monbattle.move

import monbattle.battle.Slot
import monbattle.battle.Team

/**
 * Represents a target for a [Move].
 */
data class Target(
    val team: Team,
    val slot: Slot
)