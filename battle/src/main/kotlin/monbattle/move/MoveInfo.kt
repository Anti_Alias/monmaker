package monbattle.move

import monbattle.monster.StatusCondition

/**
 * Info about a [Move].
 *
 * @property name Name.
 * @property power Power. Null if non-attacking.
 * @property pp Maximum number of times move can be used (Power Points).
 * @property targetType Type of target this move can hit.
 * @property priority Priority of move. Higher means sooner.
 * @property statChanges Changes in stats that can occur by using this move.
 * @property meta Metadata of this [MoveInfo].
 */
data class MoveInfo(
    val name: String,
    val power: Int? = null,
    val pp: Int,
    val targetType: TargetType = TargetType.SELECTED,
    val priority: Int = 0,
    val statChanges: Set<StatChange> = emptySet(),
    val meta: Meta
)

/**
 * Metadata about a given move.
 *
 * @property statusCondition Status condition that can occur due this move being performed. Null if none.
 * @property statusConditionChance Chance the status condition may occur. Must be between 0 and 1.
 * @property category Category of move this is.
 * @property drain Percentage of HP that will be drained from damage from this move.
 * @property healing The amount of hp gained by the attacking monster, in percent of it's maximum HP.
 * @property critRate Rate in which critical hits can occur.
 * @property statChance Chance the attack will cause a stat change in the target monster.
 */
data class Meta(
    val statusCondition: StatusCondition?,
    val statusConditionChance: Double = 0.0,
    val category: Category = Category.DAMAGE,
    val minHits: Int = 1,
    val maxHits: Int = 1,
    val minTurns: Int = 1,
    val drain: Double = 0.0,
    val flinchChance: Double = 0.0,
    val healing: Double = 0.0,
    val critRate: Int = 0,
    val statChance: Double = 0.0
)