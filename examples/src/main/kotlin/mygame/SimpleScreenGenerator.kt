package mygame

import monbattle.grid.Grid
import monbattle.grid.MapInfo
import monbattle.sound.Music
import monbattle.world.World

object SimpleScreenGenerator {

    fun generate_Overworld() = World(
        entityGenerator = SimpleEntityGenerator,
        music = Music(file="pewter.ogg", loopStartSeconds = 0.5f),
        grid = Grid(
            tileWidth = 16,
            tileHeight = 16,
            startingMap = "Hello World",
            mapInfos = listOf(
                MapInfo(
                    file = "helloworld.tmx",
                    name = "Hello World",
                    x = 0,
                    y = 0,
                    width = 64,
                    height = 40
                ),
                MapInfo(
                    file = "worldhello.tmx",
                    name = "World Hello",
                    x = 0,
                    y = -32,
                    width = 64,
                    height = 32
                )
            ),
            tileGenerator = SimpleTileGenerator
        )
    )
}
