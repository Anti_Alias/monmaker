package monbattle.battle

import monbattle.event.Event
import monbattle.monster.Monster
import monbattle.move.MoveInfo
import monbattle.repository.Repository
import monbattle.repository.FileRepository
import monbattle.species.Species
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

/**
 * Test for [Battle] class
 */
internal class BattleTest {

    val speciesRepo: Repository<Species> = FileRepository
        .create<Species>("src/test/resources/species")
        .cached()
    val moveRepo: Repository<MoveInfo> = FileRepository
        .create<MoveInfo>("src/test/resources/moves")
        .cached()

    @Test
    fun testBattleStart() {
        val random = Random(1234)
        val bulbasaur: Species = speciesRepo["bulbasaur"]
        val events = mutableListOf<Event>()
        val battle = Battle.createSingles(
            firstTeam = Team(
                name = "red",
                monsters = listOf(Monster.random(bulbasaur, 1, 1, random = random))
            ),
            secondTeam = Team(
                name = "blue",
                monsters = listOf(Monster.random(bulbasaur, 1, 1, random = random))
            ),
            eventListener = { event, battle -> events.add(event) }
        )
        battle.start()
        assertEquals(
            expected = listOf(
                "TurnStartedEvent",
                "DecisionRequestedEvent",
                "DecisionRequestedEvent"
            ),
            actual = events.map { it.javaClass.simpleName }
        )
    }
}