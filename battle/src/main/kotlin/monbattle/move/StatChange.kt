package monbattle.move

import monbattle.species.Stat

/**
 * Represents a change in a stat.
 */
data class StatChange(val stat: Stat, val change: Int)