package monmaker.grid

/**
 * Represents information about a [Tile].
 * @property listener Listener of events.
 * @property properties Properties of this [Tile].
 */
data class TileInfo @JvmOverloads constructor(
    val listener: TileListener = StubTileListener,
    val properties: Map<String, Any> = emptyMap()
)