package monbattle.monster

import monbattle.exceptions.BattleException
import java.util.*

/**
 * Represents the nature of a given monster.
 * Determines stat boosts.
 */
interface Nature {
    val name: String get() = javaClass.simpleName
    val attackMultiplier: Double get() = 1.0
    val defenseMultiplier: Double get() = 1.0
    val specialAttackMultiplier: Double get() = 1.0
    val specialDefenseMultiplier: Double get() = 1.0
    val speedMultiplier: Double get() = 1.0
    companion object {
        /**
         * All natures available.
         */
        private val all: List<Nature> = listOf(
            Hardy, Lonely, Brave, Adamant, Naughty, Bold, Docile,
            Relaxed, Impish, Lax, Timid, Hasty, Serious, Jolly,
            Naive, Modest, Mild, Quiet, Bashful, Rash, Calm,
            Gentle, Sassy, Careful, Quirky
        )

        /**
         * Gets a nature from the specified name
         */
        fun fromName(name: String): Nature {
            val nature: Nature? = all.firstOrNull { it.name.toUpperCase() == name.toUpperCase() }
            return nature ?: throw BattleException("Invalid Nature '$name'")
        }

        /**
         * Generates a random Nature.
         * @param random source of randomness.
         */
        fun random(random: Random): Nature = all[random.nextInt(all.size)]

        /**
         * Generates a random Nature.
         */
        fun random(): Nature = random(Random())
    }
}

object Hardy : Nature
object Lonely : Nature {
    override val attackMultiplier get() = 1.1
    override val defenseMultiplier get() = 0.9
}
object Brave : Nature {
    override val attackMultiplier get() = 1.1
    override val speedMultiplier get() = 0.9
}
object Adamant : Nature {
    override val attackMultiplier get() = 1.1
    override val specialAttackMultiplier get() = 0.9
}
object Naughty : Nature {
    override val attackMultiplier get() = 1.1
    override val specialDefenseMultiplier get() = 0.9
}
object Bold : Nature {
    override val defenseMultiplier get() = 1.1
    override val attackMultiplier get() = 0.9
}
object Docile : Nature
object Relaxed : Nature {
    override val defenseMultiplier get() = 1.1
    override val speedMultiplier get() = 0.9
}
object Impish : Nature {
    override val defenseMultiplier get() = 1.1
    override val specialAttackMultiplier get() = 0.9
}
object Lax : Nature {
    override val defenseMultiplier get() = 1.1
    override val specialDefenseMultiplier get() = 0.9
}
object Timid : Nature {
    override val speedMultiplier get() = 1.1
    override val attackMultiplier get() = 0.9
}
object Hasty : Nature {
    override val speedMultiplier get() = 1.1
    override val defenseMultiplier get() = 0.9
}
object Serious : Nature
object Jolly : Nature {
    override val speedMultiplier get() = 1.1
    override val specialAttackMultiplier get() = 0.9
}
object Naive : Nature {
    override val speedMultiplier get() = 1.1
    override val specialDefenseMultiplier get() = 0.9
}
object Modest : Nature {
    override val specialAttackMultiplier get() = 1.1
    override val attackMultiplier get() = 0.9
}
object Mild : Nature {
    override val specialAttackMultiplier get() = 1.1
    override val defenseMultiplier get() = 0.9
}
object Quiet : Nature {
    override val specialAttackMultiplier get() = 1.1
    override val speedMultiplier get() = 0.9
}
object Bashful : Nature
object Rash : Nature {
    override val specialAttackMultiplier get() = 1.1
    override val specialDefenseMultiplier get() = 0.9
}
object Calm : Nature {
    override val specialDefenseMultiplier get() = 1.1
    override val attackMultiplier get() = 0.9
}
object Gentle : Nature {
    override val specialDefenseMultiplier get() = 1.1
    override val defenseMultiplier get() = 0.9
}
object Sassy : Nature {
    override val specialDefenseMultiplier get() = 1.1
    override val speedMultiplier get() = 0.9
}
object Careful : Nature {
    override val specialDefenseMultiplier get() = 1.1
    override val specialAttackMultiplier get() = 0.9
}
object Quirky : Nature