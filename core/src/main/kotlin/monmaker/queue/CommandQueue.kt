package monmaker.queue

import monmaker.exceptions.MonmakerException
import java.util.*


/**
 * Represents a queue of [Command] objects that will be executed in the order that they are received.
 * [CommandQueue] is distinct from [ActionQueue] in that each command has a specific duration when running.
 * @param T Type of object commands will be executed on.
 */
class CommandQueue<T> {

    private var clearCount: Int = -1
    private val commands: Queue<CommandHolder<T>> = LinkedList()

    /**
     * Number of elements that are in this CommandQueue.
     */
    val size: Int get() = commands.size

    /**
     * Flag that determines if queue is empty or not.
     */
    val isEmpty: Boolean get() = commands.isEmpty()

    /**
     * @return true if this queue will be empty after the specified time in seconds has elapsed.
     */
    fun willBeEmpty(deltaTime: Float): Boolean = deltaTime >= remainingSeconds()

    /**
     * Remaining seconds in this CommandQueue.
     */
    internal fun remainingSeconds(): Float = commands
        .sumByDouble { (it.durationSeconds - it.elapsedSeconds).toDouble() }
        .toFloat()

    /**
     * Adds a command to be executed later.
     * @return this CommandQueue.
     */
    fun add(command: Command<T>, durationSeconds: Float): CommandQueue<T> {
        commands.add(CommandHolder(command, durationSeconds))
        return this
    }

    /**
     * Runs current command, if there is one for one game tick.
     * @param target Target to manipulate.
     * @param deltaTime Time in seconds since last frame.
     */
    internal fun run(target: T, deltaTime: Float) {

        // If we're set to clear commands, clear them
        if(clearCount != 0) {
            currentHolder()?.finish(target)
            repeat(clearCount) {
                commands.remove()
            }
            clearCount = 0
        }

        // Grabs current command
        var currentCommand: CommandHolder<T> = currentHolder() ?: return

        // If command has not started, start it.
        while (!currentCommand.isStarted) {

            // If command finished when starting, or failed to start, go to the next command and try to run it.
            val startedSuccessfully: Boolean = currentCommand.start(target)
            if (currentCommand.isDone || !startedSuccessfully) {
                commands.remove()
                currentCommand = currentHolder() ?: return
            }
        }

        // Run command logic. If failed, try to run the next command and quit.
        val ranSuccessfully: Boolean = currentCommand.run(target, deltaTime)
        if(!ranSuccessfully) {
            commands.remove()
            run(target, deltaTime)
            return
        }

        // If command is done, calculate time debt.
        // If there are more commands in the queue, get started with the next one.
        if (currentCommand.isDone) {
            currentCommand.finish(target)
            val timeDebt: Float = currentCommand.elapsedSeconds - currentCommand.durationSeconds
            commands.remove()
            if(timeDebt > 0f) run(target, timeDebt)
        }
    }

    private fun currentHolder(): CommandHolder<T>? {
        return if (commands.isNotEmpty()) commands.peek() else null
    }

    /**
     * Current command being executed.
     */
    val currentCommand: Command<T>? = currentHolder()?.command

    /**
     * Causes this CommandQueue to do nothing for the duration specified.
     * @param durationSeconds Time in seconds to wait for.
     */
    fun wait(durationSeconds: Float): CommandQueue<T> = add(WaitCommand(), durationSeconds)

    /**
     * Marks this CommandQueue as clearable.
     * @param count Number of commands to clear. Defaults to the current number of commands in the queue.
     */
    fun clear(count: Int = commands.size): CommandQueue<T> {
        if(count < 0 || count > commands.size) throw MonmakerException("Could not clear $count commands.")
        clearCount = count
        return this
    }
}