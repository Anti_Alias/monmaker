package monmaker.queue

import monmaker.exceptions.MonmakerException
import java.util.*

/**
 * Represents a queue of [Action] objects that will be executed in the order that they are received.
 * [ActionQueue] is distinct from [CommandQueue] in that each action may take an indeterminate amount of time.
 * @param T Type of object commands will be executed on.
 */
class ActionQueue<T> {

    private var hasStarted: Boolean = false
    private var clearCount: Int = 0
    private val actions: Queue<Action<T>> = LinkedList()

    /**
     * Number of elements that are in this CommandQueue.
     */
    val size: Int get() = actions.size

    /**
     * Flag that determines if queue is empty or not.
     */
    val isEmpty: Boolean get() = actions.isEmpty()

    /**
     * Runs current action, if there is one.
     * @param target Target to manipulate.
     * @param deltaTime Time in seconds since last frame.
     */
    internal fun run(target: T, deltaTime: Float) {

        // Clears actions if marked to do so
        while(clearCount != 0) {
            actions.remove()
            clearCount -= 1
        }

        // If there is a current action to invoke...
        if(actions.isNotEmpty()) {

            // Acquire it and invoke 'start' if this is the first time.
            val action: Action<T> = actions.peek()
            if(!hasStarted) {
                action.start(target)
                hasStarted = true
            }

            // Run it.
            action.run(target, deltaTime)

            // If it is done, remove it.
            if(action.isDone) {
                hasStarted = false
                actions.remove()
            }
        }
    }

    /**
     * Adds an [Action] to be executed later.
     * @return this [CommandQueue].
     */
    fun add(action: Action<T>): ActionQueue<T> {
        actions.add(action)
        return this
    }

    /**
     * Causes this [ActionQueue] to do nothing for the duration specified.
     * @param durationSeconds Time in seconds to wait for.
     */
    fun wait(durationSeconds: Float): ActionQueue<T> = add(WaitAction(durationSeconds))

    /**
     * Marks this [ActionQueue] as clearable.
     * @param count Number of commands to clear. Defaults to the current number of actions in the queue.
     */
    @JvmOverloads fun clear(count: Int = actions.size): ActionQueue<T> {
        if(count < 0 || count > actions.size) throw MonmakerException("Could not clear $count commands.")
        clearCount = count
        return this
    }
}