package monbattle.command

import monbattle.battle.Battle

/**
 * Represents a command to force the [Battle] to do some action.
 */
interface Command {

    /**
     * Performs the command.
     */
    fun act(battle: Battle)
}