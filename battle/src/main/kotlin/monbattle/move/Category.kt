package monbattle.move

/**
 * Category of a move.
 */
enum class Category {
    DAMAGE,
    AILMENT,
    NET_GOOD_STATS,
    HEAL,
    DAMAGE_AILMENT,
    SWAGGER,
    DAMAGE_LOWER,
    DAMAGE_RAISE,
    DAMAGE_HEAL,
    OHKO,
    WHOLE_FIELD_EFFECT,
    FIELD_EFFECT,
    FORCE_SWITCH,
    UNIQUE;

    /**
     * Description of a [Category].
     */
    val description: String get() = when(this) {
        DAMAGE -> "Inflicts damage"
        AILMENT -> "No damage; Inflicts ailment"
        NET_GOOD_STATS -> "No damage; lowers target's stats or raises user's stats"
        HEAL -> "No damage; Heals the user"
        DAMAGE_AILMENT -> "Inflicts damage; inflicts status ailment"
        SWAGGER -> "No damage; inflicts status ailment; raises target's stats"
        DAMAGE_LOWER -> "Inflicts damage; lowers target's stats"
        DAMAGE_RAISE -> "Inflicts damage; raises user's stats"
        DAMAGE_HEAL -> "Inflicts damage; absorbs damage done to heal the user"
        OHKO -> "One-hit KO"
        WHOLE_FIELD_EFFECT -> "Effect on the whole field"
        FIELD_EFFECT -> "Effect on one side of the field"
        FORCE_SWITCH -> "Forces target to switch out"
        UNIQUE -> "Unique effect"
    }
}