package monmaker.generator

import monmaker.exceptions.MonmakerException
import java.lang.reflect.Method

/**
 * Responsible for generating objects of type <T> by name.
 */
@FunctionalInterface
interface Generator<T> {
    fun generate(name: String): T
    fun generateOrNull(name: String): T?
    companion object {
        inline fun <reified T> empty(): Generator<T> = object : Generator<T> {
            override fun generate(name: String): T = throw MonmakerException(
                "Generator for classes ${T::class.java.name} could not resolve '$name'."
            )
            override fun generateOrNull(name: String): T? = null
        }
    }
}

/**
 * Attempts to create a generator from an object..
 * @param obj Object that has methods prefixed with "generate". Used for building Generator instance.
 */
inline fun <reified T> createGenerator(obj: Any?): Generator<T> {

    if(obj == null) return Generator.empty()

    // Accumulates lambdas
    val prefix = "generate_"
    val prefixLen = prefix.length
    val generators = mutableMapOf<String, ()->T>()
    obj.javaClass.methods
        .asSequence()
        .filter { it.name.startsWith(prefix) && it.name != prefix}
        .forEach { method: Method ->
            if(method.parameterCount != 0) throw MonmakerException(
                "Class ${obj::class.java.name} had a generator named ${method.name}, " +
                 "but had more than 0 parameters."
            )
            if(!T::class.java.isAssignableFrom(method.returnType)) throw MonmakerException(
                "Class ${obj::class.java.name} had a generator named ${method.name}, " +
                "but it returns instances of ${method.returnType.name} rather than ${T::class.java.name}."
            )
            generators[method.name.substring(prefixLen)] = {
                val result: Any? = method.invoke(obj)
                result as T
            }
        }
    println(obj.javaClass)
    println(generators)


    // Returns as a Generator instance
    return object : Generator<T> {
        override fun generate(name: String): T {
            val result: T? = generateOrNull(name)
            return result ?: throw MonmakerException(
                "Generator for classes ${T::class.java.name} could not resolve '$name'."
            )
        }
        override fun generateOrNull(name: String): T? {

            val callback: (()->T)? = generators[name]
            return callback?.invoke()
        }
    }
}