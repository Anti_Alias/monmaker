package monbattle.command

import monbattle.battle.Battle

/**
 * Command mapper for a singles battles with a given number of monsters.
 */
class SinglesCommandMapper : CommandMapper {
    override fun map(command: Command, battle: Battle): Command? {
        return command
    }
}