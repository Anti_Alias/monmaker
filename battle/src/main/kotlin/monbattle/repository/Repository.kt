package monbattle.repository

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.File

/**
 * Repository of battle resources of a given type.
 */
interface Repository<T> {
    operator fun get(name: String): T
    fun getOrNull(name: String): T?
    operator fun contains(name: String): Boolean
    fun cached(): Repository<T> = CachedRepository(this)
}

/**
 * [Repository] that acquires it's species from a directory of .json files.
 *
 * @property directory Directory that holds the .json species files.
 * @property mapper Jackson [ObjectMapper] to use for deserialization.
 */
class FileRepository<T>(
    val directory: File,
    val type: Class<T>,
    val mapper: ObjectMapper = jacksonObjectMapper()
) : Repository<T> {
    override operator fun get(name: String): T {
        return mapper.readValue(File("$directory/$name.json"), type)
    }
    override fun getOrNull(name: String): T? {
        val file = File("$directory/$name.json")
        if(!file.exists()) return null
        return mapper.readValue(file, type)
    }
    override operator fun contains(name: String): Boolean = File("$directory/$name.json").exists()
    companion object {
        @JvmOverloads inline fun <reified T> create(
            directory: File,
            mapper: ObjectMapper = jacksonObjectMapper()
        ): FileRepository<T> = FileRepository(directory, T::class.java, mapper)

        @JvmOverloads inline fun <reified T> create(
            directory: String,
            mapper: ObjectMapper = jacksonObjectMapper()
        ): FileRepository<T> = create(File(directory), mapper)
    }
}

/**
 * [Repository] that caches values if they're being read in for the first time.
 */
private class CachedRepository<T>(val underlying: Repository<T>) : Repository<T> by underlying {
    private val cache: MutableMap<String, T?> = mutableMapOf()
    override operator fun get(name: String): T = cache.getOrPut(name) { underlying[name] }!!
    override fun getOrNull(name: String): T? = cache.getOrPut(name) { underlying[name] }
    override operator fun contains(name: String): Boolean = cache[name] != null || underlying.contains(name)
}