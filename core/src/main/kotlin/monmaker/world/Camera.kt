package monmaker.world

import com.badlogic.gdx.math.Matrix4
import monmaker.entity.Entity
import monmaker.entity.Target
import kotlin.math.roundToInt

/**
 * Represents the camera belonging to a [World].
 * @property x X-coordinate of the camera's center.
 * @property y Y-coordinate of the camera's center.
 */
class Camera(
    var x: Float,
    var y: Float,
    private val world: World
) {

    /**
     * Matrix transformation associated with this Camera.
     */
    internal val transformation: Matrix4 = Matrix4()

    /**
     * Target entity to follow.
     */
    var target: Target? = null
        private set

    /**
     * Width of the camera. Depends on the game's internal resolution.
     */
    val width: Float get() = world.game.resolutionWidth.toFloat()

    /**
     * Height of the camera. Depends on the game's internal resolution.
     */
    val height: Float get() = world.game.resolutionHeight.toFloat()

    /**
     * Minimum-tileX value.
     */
    val minX: Float get() = x - width/2

    /**
     * Maximum-tileX value.
     */
    val maxX: Float get() = x + width/2

    /**
     * Minimum-tileY value.
     */
    val minY: Float get() = y - height/2

    /**
     * Maximum-tileY value.
     */
    val maxY: Float get() = y + height/2

    /**
     * Sets the target entity to follow by name.
     */
    fun setTarget(entityName: String) {
        target = Target(world, entityName)
    }

    /**
     * Sets target to nothing.
     */
    fun unsetTarget() {
        target = null
    }

    /**
     * Sets the target entity to follow.
     */
    fun setTargetEntity(entity: Entity) {
        target = Target(world, entity.name)
    }

    /**
     * Moves camera towards target.
     */
    internal fun update() {

        // Updates target x and y values
        val entityTarget: Entity? = target?.get()
        if(entityTarget != null) {
            x = entityTarget.x
            y = entityTarget.y
        }
    }

    /**
     * Computes the transformation matrix.
     */
    internal fun computeTransformation() {
        val xRounded: Float = x.roundToInt().toFloat()
        val yRounded: Float = y.roundToInt().toFloat()
        transformation
            .idt()
            .translate(-xRounded, -yRounded, 0f)
    }

    override fun toString(): String {
        return "Camera(tileX=$x, tileY=$y, width=$width, height=$height)"
    }
}