package monbattle.battle

import monbattle.monster.Adamant
import monbattle.monster.EffortValues
import monbattle.monster.IndividualValues
import monbattle.monster.Monster
import monbattle.species.BaseStats
import monbattle.species.Gender
import monbattle.species.Species
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

/**
 * Tests the monster class.
 */
class MonsterTest {

    @Test
    fun testStats1() {
        val monster = Monster(
            level = 78,
            species = Species(
                name = "Garchomp",
                baseStats = BaseStats(
                    hp = 108,
                    attack = 130,
                    defense = 95,
                    specialAttack = 80,
                    specialDefense = 85,
                    speed = 102
                )
            ),
            gender = Gender.MALE,
            nature = Adamant,
            ivs = IndividualValues(
                hp = 24,
                attack = 12,
                defense = 30,
                specialAttack = 16,
                specialDefense = 23,
                speed = 5
            ),
            evs = EffortValues(
                hp = 74,
                attack = 190,
                defense = 91,
                specialAttack = 48,
                specialDefense = 84,
                speed = 23
            )
        )
        assertEquals(289, monster.currentHP)
        assertEquals(278, monster.attack)
        assertEquals(193, monster.defense)
        assertEquals(135, monster.specialAttack)
        assertEquals(171, monster.specialDefense)
        assertEquals(171, monster.speed)
    }

    @Test
    fun generateRandom() {
        Monster.random(
            species = Species(
                name = "Bulbasaur",
                baseStats = BaseStats(
                    hp = 45,
                    attack = 49,
                    defense = 49,
                    specialAttack = 65,
                    specialDefense = 65,
                    speed = 45
                ),
                genderRatio = .875
            ),
            minLevel = 1,
            maxLevel = 100,
            nickname = "Plant",
            random = Random(42)
        )
    }
}