package monmaker.script

import monmaker.entity.Direction
import monmaker.event.DisableEvent
import monmaker.event.EnableEvent
import monmaker.event.Event
import monmaker.event.TickEvent
import monmaker.exceptions.MonmakerException
import monmaker.extensions.look
import monmaker.extensions.setState
import monmaker.extensions.travel
import monmaker.grid.Tile

/**
 * Script that causes an Entity to wander aimlessly.
 */
class AimlessScript @JvmOverloads constructor(
    val minDuration: Float = 1f,
    val maxDuration: Float = 5f,
    private val leftPadding: Int = 2,
    private val rightPadding: Int = 2,
    private val topPadding: Int = 2,
    private val bottomPadding: Int = 2,
    private val idleState: String = "idle",
    private val movementState: String = "walk"
) : EntityScript() {

    private var elapsedSeconds: Float = 0f
    private var durationSeconds: Float = 0f
    private var minTileX: Int = -1
    private var minTileY: Int = -1
    private var maxTileX: Int = -1
    private var maxTileY: Int = -1
    private var enabled: Int = 0

    init {
        if(minDuration < 0f) throw MonmakerException("minDuration must be >= 0. Got $minDuration.")
        if(maxDuration < minDuration) throw MonmakerException("maxDuration must be >= minDuration. Got $maxDuration.")
        if(leftPadding < 0 || rightPadding < 0 || topPadding < 0 || bottomPadding < 0) throw MonmakerException(
            "Invalid padding values left: $leftPadding, right: $rightPadding, top: $topPadding, bottom: $bottomPadding."
        )
    }

    override fun register() {
        durationSeconds = nextDuration()
        minTileX = entity.tileX - leftPadding
        minTileY = entity.tileY - bottomPadding
        maxTileX = entity.tileX + rightPadding
        maxTileY = entity.tileY + topPadding
    }

    override fun on(event: Event) {
        when(event) {
            is DisableEvent -> enabled += 1
            is EnableEvent -> enabled -= 1
            is TickEvent -> if(enabled == 0) onTick(event)
        }
    }

    fun onTick(event: TickEvent) {
        elapsedSeconds += event.deltaTime
        if(elapsedSeconds >= durationSeconds && entity.queue.willBeEmpty(event.deltaTime)) {
            val rand: Float = game.random.nextFloat()
            val direction: Direction =
                if(rand < 0.25) Direction.S
                else if(rand < 0.5) Direction.E
                else if(rand < 0.75) Direction.N
                else Direction.W
            val newX: Int = entity.tileX + direction.x
            val newY: Int = entity.tileY + direction.y
            val tile: Tile = world.grid.getTile(newX, newY)
            if(newX in minTileX..maxTileX && newY in minTileY..maxTileY && !(tile.isSolid || tile.isOccupied)) {
                entity.queue
                    .setState(movementState)
                    .travel(direction)
                    .setState(idleState)
            }
            else {
                entity.queue.look(direction)
            }
            durationSeconds = nextDuration()
            elapsedSeconds = 0f
        }
    }
    fun nextDuration(): Float = minDuration + (maxDuration - minDuration) * game.random.nextFloat()
}