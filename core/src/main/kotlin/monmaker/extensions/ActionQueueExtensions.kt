package monmaker.extensions

import com.rafaskoberg.gdx.typinglabel.TypingListener
import monmaker.event.PromptOpenEvent
import monmaker.game.Promptor
import monmaker.queue.ActionQueue
import monmaker.queue.CloseAction
import monmaker.queue.PromptAction
import monmaker.queue.WaitAction

/**
 * Causes the [Promptor] to prompt a message.
 * @param message Message to prompt.
 * @param autoClose Flag that determines if Promptor should immediately close after prompting.
 * @param suppressEvent If true, will not emit a [PromptOpenEvent] upon opening.
 * @param listener Optional [TypingListener] instance that will listen for events in the message.
 */
@JvmOverloads fun ActionQueue<Promptor>.prompt(
    message: String,
    autoClose: Boolean = false,
    suppressEvent: Boolean = false,
    listener: TypingListener? = null
): ActionQueue<Promptor> = this.add(PromptAction(message, autoClose, suppressEvent, listener))

/**
 * Causes [Promptor] to wait.
 * @param durationSeconds Time in seconds to wait for.
 */
fun ActionQueue<Promptor>.wait(durationSeconds: Float): ActionQueue<Promptor> = this.add(WaitAction(durationSeconds))

/**
 * Causes [Promptor] to close.
 * @param suppressEvent If true, will not emit a [PromptCloseEvent] upon closing.
 */
@JvmOverloads fun ActionQueue<Promptor>.close(suppressEvent: Boolean = false): ActionQueue<Promptor> = this.add(CloseAction(suppressEvent))