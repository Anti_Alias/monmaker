package monmaker.app

import com.badlogic.gdx.Graphics


/**
 * Represents a configuration object to be modified before the app starts.
 */
interface MonmakerConfig {

    /**
     * Current windowed width
     */
    var windowWidth: Int

    /**
     * Windowed height
     */
    var windowHeight: Int

    /**
     * Desired FPS
     */
    var fps: Int

    /**
     * Fullscreen display mode
     */
    var fullscreenMode: Graphics.DisplayMode
}