package monmaker.queue

import monmaker.controller.Button
import monmaker.controller.Controller
import monmaker.entity.Direction
import monmaker.entity.Entity
import monmaker.extensions.bump
import monmaker.extensions.look
import monmaker.grid.Grid
import monmaker.grid.Tile
import monmaker.sound.Sound

/**
 * Represents a command to be executed on objects of type [T].
 * @param T Type of object to be executed on.
 */
interface Command<T> {

    /**
     * Runs first step of the command. This may set [isDone] to true.
     * @param target Target to execute on.
     * @return false if the command should be cancelled.
     */
    fun start(target: T): Boolean = true

    /**
     * Executes command.
     * @param target Target to execute on.
     * @param t T value ranging from [0f, 1f].
     * @return false if the command should be cancelled.
     */
    fun run(target: T, t: Float): Boolean = true

    /**
     * Finishes command.
     */
    fun finish(target: T) {}
}

/**
 * Causes CommandQueue to wait.
 * @param T Type of object to be executed on. In this case, target is ignored.
 */
class WaitCommand<T> : Command<T>

/**
 * A command for an entity to travel
 * @property direction Direction to travel in.
 * If null, travels in place.
 * @property bumpSound Sound to play if travel ends up bumping the player.
 */
class TravelCommand(val direction: Direction?, val bumpSound: Sound? = null) : Command<Entity> {

    private var oldX: Float = -1f
    private var oldY: Float = -1f
    private var newX: Float = -1f
    private var newY: Float = -1f
    private var tOffset: Float = 0f

    // Gathers information needed
    override fun start(target: Entity): Boolean {

        // Gets adjacent tile. If it is solid, exit early
        val grid: Grid = target.world.grid
        val newTileX: Int = target.tileX + (direction?.x ?: 0)
        val newTileY: Int = target.tileY + (direction?.y ?: 0)
        val tile: Tile = grid.getTile(newTileX, newTileY)
        if(tile.isSolid || tile.isOccupied) {
            if(direction != null) target.queue
                .look(direction)
                .bump(1/2f, bumpSound)
            return false
        }

        // Otherwise, start normally
        val oldTileX: Int = target.tileX
        val oldTileY: Int = target.tileY
        val tileWidth: Float = target.world.grid.tileWidth.toFloat()
        val tileHeight: Float= target.world.grid.tileWidth.toFloat()
        oldX = oldTileX * tileWidth + tileWidth/2
        oldY = oldTileY * tileHeight
        newX = newTileX * tileWidth + tileWidth/2
        newY  = newTileY * tileHeight
        if(target.foot == 1)
            tOffset = 0.5f
        target.foot *= -1
        if(direction != null)
            target.direction = direction
        target.setLocation(newTileX, newTileY, false)
        target.isTravelling = true
        return true
    }

    override fun run(target: Entity, t: Float): Boolean {
        // Update the target's t-value and move their graphics
        target.setT(t/2 + tOffset)
        target.x = oldX + (newX - oldX) * t
        target.y = oldY + (newY - oldY) * t
        return true
    }

    override fun finish(target: Entity) {
        target.isTravelling = false
    }
}

/**
 * Command that causes an entity to 'bump'.
 */
class BumpCommand(val sound: Sound? = null) : Command<Entity> {
    private var tOffset: Float = 0f
    override fun start(target: Entity): Boolean {
        if(sound != null)
            target.world.game.soundPlayer.playSound(sound)
        if(target.foot == 1)
            tOffset = 0.5f
        target.foot *= -1
        return true
    }
    override fun run(target: Entity, t: Float): Boolean {
        val currentDirection: Direction? = readDirection(target)
        if(currentDirection != null && currentDirection != target.direction)
            return false
        target.setT(t/2 + tOffset)
        return true
    }

    private fun readDirection(target: Entity): Direction? {
        val controller: Controller = target.world.game.controller
        return if(controller.isPressed(Button.DOWN)) Direction.S
            else if(controller.isPressed(Button.RIGHT)) Direction.E
            else if(controller.isPressed(Button.UP)) Direction.N
            else if(controller.isPressed(Button.LEFT)) Direction.W
            else null
    }
}

/**
 * Command that makes an entity look in a particular direction.
 */
class LookCommand(val direction: Direction) : Command<Entity> {
    override fun start(target: Entity): Boolean {
        target.direction = direction
        return true
    }
}

/**
 * Command that sets an entity's state.
 */
class SetStateCommand(val state: String) : Command<Entity> {
    override fun start(target: Entity): Boolean {
        target.states.currentState = state
        return true
    }
}