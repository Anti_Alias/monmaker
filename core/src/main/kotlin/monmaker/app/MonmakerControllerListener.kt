package monmaker.app

import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.controllers.Controller
import com.badlogic.gdx.controllers.ControllerListener
import com.badlogic.gdx.controllers.PovDirection
import com.badlogic.gdx.math.Vector3
import monmaker.logging.Logger

internal class MonmakerInputProcessor(val app: MonmakerApp) : InputAdapter() {
    override fun keyUp(keycode: Int): Boolean {
        app.keyUp(keycode)
        return true
    }
    override fun keyDown(keycode: Int): Boolean {
        app.keyDown(keycode)
        return true
    }
}

/**
 * Game to update.
 */
internal class MonmakerControllerListener(val app: MonmakerApp) : ControllerListener {
    override fun connected(controller: Controller) {
        app.setController(controller)
        logger.log("Connected controller ${controller.name}.")
    }

    override fun disconnected(controller: Controller) {
        app.unsetController(controller)
        logger.log("Disconnected controller ${controller.name}.")
    }

    override fun buttonDown(controller: Controller, buttonCode: Int): Boolean {
        app.buttonDown(controller, buttonCode)
        return true
    }

    override fun buttonUp(controller: Controller, buttonCode: Int): Boolean {
        app.buttonUp(controller, buttonCode)
        return true
    }

    override fun xSliderMoved(controller: Controller, sliderCode: Int, value: Boolean): Boolean {
        return true
    }

    override fun ySliderMoved(controller: Controller, sliderCode: Int, value: Boolean): Boolean {
        return true
    }

    override fun accelerometerMoved(controller: Controller, accelerometerCode: Int, value: Vector3): Boolean {
        return true
    }

    override fun axisMoved(controller: Controller, axisCode: Int, value: Float): Boolean {
        return true
    }

    override fun povMoved(controller: Controller, povCode: Int, value: PovDirection): Boolean {
        app.povMoved(value)
        return true
    }

    companion object {
        private val logger: Logger = Logger.forClass<MonmakerControllerListener>()
    }
}