package monmaker.extensions

import com.badlogic.gdx.assets.AssetLoaderParameters
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.utils.Disposable

/**
 * Loads asset synchronously.
 */
fun <T> AssetManager.loadSync(fileName: String, type: Class<T>): T {
    load(fileName, type)
    return finishLoadingAsset(fileName)
}

/**
 * Kotlin-esque verision of [loadSync].
 */
inline fun <reified T> AssetManager.loadSync(fileName: String): T = loadSync(fileName, T::class.java)

/**
 * Loads asset synchronously.
 */
fun <T> AssetManager.loadSync(
    fileName: String,
    type: Class<T>,
    parameter: AssetLoaderParameters<T>
): T {
    this.load(fileName, type, parameter)
    return finishLoadingAsset(fileName)
}

/**
 * Kotlin-esque verision of [loadSync].
 */
inline fun <reified T> AssetManager.loadSync(fileName: String, parameter: AssetLoaderParameters<T>): T =
    loadSync(fileName, T::class.java)

/**
 * Helpful use function for disposables.
 * Always invokes [Disposable.dispose] regardless of monmaker.exceptions thrown.
 */
inline fun <T : Disposable> T.use(cb: (T)->Unit) {
    try { cb(this) }
    finally { dispose() }
}