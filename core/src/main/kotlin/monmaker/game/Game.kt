package monmaker.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.StretchViewport
import monmaker.generator.Generator
import monmaker.generator.createGenerator
import monmaker.graphics.Renderers
import monmaker.controller.Controller
import monmaker.event.Event
import monmaker.logging.Logger
import monmaker.screen.Screen
import java.util.*
import kotlin.math.max

/**
 * Represents the game as a whole.
 *
 * A [Game] has a name which dictates the title, a resolution, a desired frames-per-second and a [Screen] instance to display content for.
 * If a [Screen] is not present, the default background color is displayed.
 *
 * Includes screen size, framerate, worlds, save data, etc.
 * @property name Name of the game
 * @property resolutionWidth Width of the frame buffer being rendered to.
 * @property resolutionHeight Height of the frame buffer being rendered to.
 * @param showFps If true, displays a calculated framerate.
 * @property scalingType Way the screen should scale. Defaults to [ScalingType.NO_BLACK_LINES]
 * @param startingScreen Name of screen to start in.
 * @property screenGenerator Generator of screens to use when switching between areas/menus/etc.
 * @property random Source of randomness.
  */
class Game @JvmOverloads constructor(
    val name: String,
    val resolutionWidth: Int,
    val resolutionHeight: Int,
    showFps: Boolean = false,
    val scalingType: ScalingType = ScalingType.BLACK_LINES,
    startingScreen: String? = null,
    val screenGenerator: Generator<Screen> = Generator.empty(),
    val promptor: Promptor = Promptor(),
    val random: Random = Random()
) {

    /**
     * Alternate constructor.
     */
    @JvmOverloads constructor(
        name: String,
        resolutionWidth: Int,
        resolutionHeight: Int,
        showFps: Boolean = false,
        scalingType: ScalingType = ScalingType.BLACK_LINES,
        startingScreen: String? = null,
        screenGenerator: Any
    ) : this(name, resolutionWidth, resolutionHeight, showFps, scalingType, startingScreen, createGenerator(screenGenerator))

    /**
     * Paths to assets of specific types.
     */
    val assetPaths: AssetPaths = AssetPaths()

    /**
     * Optional screen being shown.
     */
    var screen: Screen? = null

    /**
     * Object that holds renderers used when drawing to the screen.
     */
    val renderers: Renderers

    /**
     * Controls sounds/music played during the Game.
     */
    val soundPlayer: SoundPlayer

    /**
     * Manages the loading and unloading of assets.
     * Uses reference counting to determine which assets need to be deleted.
     */
    val assetManager: AssetManager

    /**
     * Mutable color of the background of the game.
     */
    val backgroundColor: Color = Color(0f, 0f, 0f, 1f)

    /**
     * Main controller of the Game.
     */
    val controller: Controller = Controller()

    /**
     * Scene2D stage used for rendering the UI.
     */
    val stage: Stage

    private var font: BitmapFont? = null                // Font for rendering the FPS if enabled.
    private val primaryBuffer: FrameBuffer              // Primary buffer for rendering at original resolution.
    private var secondaryBuffer: FrameBuffer? = null    // Secondary buffer to render to for scaling purposes.
    private var gdxCamera: OrthographicCamera           // LibGDX camera used for transformations.

    init {

        // Prepares fields
        renderers = Renderers(
            spriteBatch = SpriteBatch(),
            shapeRenderer = ShapeRenderer()
        )
        assetManager = AssetManager()
        soundPlayer = SoundPlayer(this)
        gdxCamera = OrthographicCamera()
        primaryBuffer = FrameBuffer(Pixmap.Format.RGBA8888, resolutionWidth, resolutionHeight, false)
        primaryBuffer.colorBufferTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)

        // Sets up stage
        stage = Stage(StretchViewport(resolutionWidth.toFloat(), resolutionHeight.toFloat()))
        stage.viewport.setScreenSize(resolutionWidth, resolutionHeight)

        // Configures promptor
        promptor.game = this
        promptor.load()

        // Post configuration
        assetManager.setLoader(TiledMap::class.java, TmxMapLoader())
        show(startingScreen)

        // Loads font if showing fps is necessary
        if (showFps)
            font = BitmapFont()
    }

    /**
     * Disposes of current screen if there is one, generates a new screen and displays it.
     */
    fun show(screenName: String?) {

        // Disposes and unassigns current screen
        val thisScreen = this.screen
        if(thisScreen != null) {
            thisScreen.unload()
            thisScreen._game = null
        }

        // Assigns new screen
        if(screenName != null) {
            val newScreen: Screen = screenGenerator.generate(screenName)
            newScreen._game = this
            newScreen.setViewport(resolutionWidth, resolutionHeight)
            newScreen.load()
            this.screen = newScreen
        }
    }

    /**
     * Runs the game logic.
     */
    internal fun run(deltaTime: Float) {
        screen?.run(deltaTime)
        soundPlayer.update(deltaTime)
        promptor.run(deltaTime)
        stage.act(deltaTime)
        controller.update()
    }

    /**
     * Fires event to [Screen] owned by this [Game], if it has one.
     */
    fun fire(event: Event) {
        screen?.fire(event)
    }

    /**
     * Renders game's graphics.
     *
     * @param displayWidth Width assumed displayWidth of the display.
     * @param displayHeight Assumed displayHeight of the display.
     */
    internal fun render(displayWidth: Int, displayHeight: Int) {

        // Renders current screen
        renderScreen(displayWidth, displayHeight)

        // Renders fps if flag was true
        if(font != null)
            renderFPS(displayWidth, displayHeight)
    }

    private fun renderScreen(displayWidth: Int, displayHeight: Int) {

        // Renders at internal resolution to primary buffer
        primaryBuffer.begin()
        render()
        primaryBuffer.end()

        // Scales the secondary buffer
        var scaleX: Float = displayWidth / resolutionWidth.toFloat()
        var scaleY: Float = displayHeight / resolutionHeight.toFloat()
        when(scalingType) {
            ScalingType.BLACK_LINES -> {
                val scale: Float = if(scaleX > scaleY) scaleY else scaleX
                scaleX = scale
                scaleY = scale
            }
            ScalingType.NO_BLACK_LINES -> {
                val scale: Float = if(scaleX > scaleY) scaleX else scaleY
                scaleX = scale
                scaleY = scale
            }
            else -> {}
        }
        resizeSecondaryBuffer(
            width = max(resolutionWidth, resolutionWidth * scaleX.toInt()),
            height = max(resolutionHeight, resolutionHeight * scaleY.toInt())
        )

        // Renders primary buffer to secondary buffer
        val secondaryBuffer: FrameBuffer = secondaryBuffer!!
        val secondaryWidth: Float = secondaryBuffer.width.toFloat()
        val secondaryHeight: Float = secondaryBuffer.height.toFloat()
        secondaryBuffer.begin()
            computeOrtho(secondaryBuffer.width.toFloat(), secondaryBuffer.height.toFloat())
            val primaryTex: Texture = primaryBuffer.colorBufferTexture
            val batch: Batch = renderers.spriteBatch
            batch.begin()
            batch.draw(primaryTex, -secondaryWidth/2f, -secondaryHeight/2f, secondaryWidth, secondaryHeight, 0, 0, primaryBuffer.width, primaryBuffer.height, false, true)
            batch.end()
        secondaryBuffer.end()

        // Determines how to render texture
        val tex: Texture = secondaryBuffer.colorBufferTexture
        val x = -displayWidth/2f + (displayWidth - resolutionWidth*scaleX)/2f
        val y = -displayHeight/2f + (displayHeight - resolutionHeight*scaleY)/2f
        val width: Float = resolutionWidth.toFloat() * scaleX
        val height: Float = resolutionHeight.toFloat() * scaleY
        val srcX = 0
        val srcY = 0
        val srcWidth: Int = secondaryBuffer.width
        val srcHeight: Int = secondaryBuffer.height
        val flipX = false
        val flipY = true

        // Clears background
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        // Renders the primary buffer to the secondary buffer with scaling applied.
        computeOrtho(displayWidth.toFloat(), displayHeight.toFloat())
        batch.begin()
        batch.draw(tex, x, y, width, height, srcX, srcY, srcWidth, srcHeight, flipX, flipY)
        batch.end()
    }

    private fun renderFPS(displayWidth: Int, displayHeight: Int) {
        // Draws box
        val shapeRenderer: ShapeRenderer = renderers.shapeRenderer
        shapeRenderer.color = Color.DARK_GRAY
        shapeRenderer.setAutoShapeType(true)
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.rect(-displayWidth/2f, -displayHeight/2f, 30f, 20f)
        shapeRenderer.end()

        // Draws text
        val batch: Batch = renderers.spriteBatch
        batch.begin()
        font!!.draw(
            batch,
            Gdx.graphics.framesPerSecond.toString(),
            -displayWidth/2f + 3,
            -displayHeight/2f + 15
        )
        batch.end()
    }

    internal fun render() {

        // Renders screen if available
        Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        // Renders current screen if there is one
        computeOrtho(resolutionWidth.toFloat(), resolutionHeight.toFloat())
        screen?.render()

        // Renders UI
        computeOrtho(resolutionWidth.toFloat(), resolutionHeight.toFloat())
        stage.draw()
    }

    /**
     * Computes orthographic view
     */
    private fun computeOrtho(width: Float, height: Float) {
        gdxCamera.viewportWidth = width
        gdxCamera.viewportHeight = height
        gdxCamera.update()
        val batch: Batch = renderers.spriteBatch
        val shapeRenderer: ShapeRenderer = renderers.shapeRenderer
        batch.projectionMatrix = gdxCamera.combined
        shapeRenderer.projectionMatrix = gdxCamera.combined
        batch.transformMatrix.idt()
        shapeRenderer.transformMatrix = batch.transformMatrix
    }

    private fun loadFont() {
        font = BitmapFont()
    }

    /**
     * Disposes of all game's resources.
     */
    internal fun unload() {
        screen?.unload()
        primaryBuffer.dispose()
        secondaryBuffer?.dispose()
        font?.dispose()
        promptor.unload()
        stage.dispose()
    }

    /**
     * Resizes frame buffer
     */
    private fun resizeSecondaryBuffer(width: Int, height: Int) {
        val secondaryBuffer = secondaryBuffer
        if(secondaryBuffer == null || width != secondaryBuffer.width || height != secondaryBuffer.height) {
            secondaryBuffer?.dispose()
            this.secondaryBuffer = FrameBuffer(Pixmap.Format.RGBA8888, width, height, false)
            logger.log("Game '$name' allocated a new frame buffer at size ${width}x${height}.")
        }
    }

    companion object {
        private val logger = Logger.forClass<Game>()
    }
}