package monmaker.entity

import monmaker.exceptions.MonmakerException
import monmaker.world.World

/**
 * Set of values that an Entity can have.
 */
class States(startingState: String, val values: Map<String, EntityAnimation>) {

    init {
        if(values.isEmpty())
            throw MonmakerException("Cannot construct an empty values object.")
    }

    /**
     * Name of all values available.
     */
    val stateNames: Set<String> get() = this.values.keys

    /**
     * Current state being set.
     */
    var currentState: String = startingState
        set(value) {
            if(value !in stateNames)
                throw MonmakerException("Could not set state to '$field' as it did not exist.")
            field = value
        }


    /**
     * Current animation being rendered.
     * Same as invoking getAnimation([currentState])
     */
    val currentAnimation: EntityAnimation get() = getAnimation(currentState)

    /**
     * Gets animation by name.
     */
    fun getAnimation(state: String): EntityAnimation = this.values.getOrElse(state) {
        throw MonmakerException("State '$state' did not exist.")
    }

    internal fun load(world: World) {
        for(animation in values.values)
            animation.load(world.game.assetManager, world.game.assetPaths)
    }

    internal fun unload(world: World) {
        for(animation in values.values)
            animation.unload(world.game.assetManager, world.game.assetPaths)
    }
}