package monmaker.grid

import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.MapLayers
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Matrix4
import monmaker.world.Camera
import monmaker.exceptions.MonmakerException
import java.lang.Math.max
import java.lang.Math.min

/**
 * Represents a tiled map embedded in a larger [Grid].
 * @property info [MapInfo] metadata used to create this MapData.
 * @property tiledMap TiledMap instance to renderUnderLayers.
 * @param grid [Grid] that owns this [MapData].
 */
internal class MapData(
    internal val info: MapInfo,
    internal val tiledMap: TiledMap,
    private val grid: Grid
) {

    /**
     * Renderer for the TiledMap.
     */
    private val renderer: OrthogonalTiledMapRenderer

    /**
     * Transformation to use when rendering.
     */
    private val transformation: Matrix4

    /**
     * Indices of layers underneath entities.
     */
    private val underLayerIndices: IntArray

    /**
     * Indices of layers over entities.
     */
    private val overLayerIndices: IntArray

    // Variables that determine which area of tiles to renderUnderLayers
    private var x: Float
    private var y: Float
    private var width: Float
    private var height: Float


    init {
        checkMapSize(tiledMap)
        val layers: MapLayers = tiledMap.layers
        val entityLayerIndex: Int = findEntityLayerIndex()
        underLayerIndices = IntArray(entityLayerIndex) { index -> index }
        overLayerIndices = IntArray(layers.size() - entityLayerIndex) { index -> entityLayerIndex + index }
        renderer = OrthogonalTiledMapRenderer(tiledMap)
        transformation = Matrix4()
        x = 0f
        y = 0f
        width = 0f
        height = 0f
    }

    /**
     * Computes transformation matrix for further rendering.
     * Also determines range to renderUnderLayers tiles.
     */
    internal fun computeView() {
        // Transforms matrix
        val minXPixels: Float = (info.x * grid.tileWidth).toFloat()
        val minYPixels: Float = (info.y * grid.tileHeight).toFloat()
        val widthPixels: Float = info.width * grid.tileWidth.toFloat()
        val heightPixels: Float = info.height * grid.tileHeight.toFloat()
        transformation
            .set(grid.world.projection)
            .mul(grid.world.camera.transformation)
            .translate(minXPixels, minYPixels, 0f)

        // Determines view
        val camera: Camera = grid.world.camera
        x = min(widthPixels, max(0f, camera.minX - minXPixels))
        y = min(heightPixels, max(0f, camera.minY - minYPixels))
        width = min(widthPixels, max(0f, camera.maxX - minXPixels))
        height = min(heightPixels, max(0f, camera.maxY - minYPixels - grid.tileHeight))
    }

    /**
     * Layer which stores entity markers.
     */
    internal val entityLayer: MapLayer get() {
        val index: Int =
            if(underLayerIndices.isEmpty()) 0
            else underLayerIndices.last() + 1
        return tiledMap.layers[index]
    }

    /**
     * Renders the layers underneath the entities
     */
    internal fun renderUnderLayers() {
        renderer.setView(transformation, x, y, width, height)
        renderer.render(underLayerIndices)
    }

    /**
     * Renders the layers over the entities
     */
    internal fun renderOverLayers() {
        renderer.setView(transformation, x, y, width, height)
        renderer.render(overLayerIndices)
    }

    /**
     * Finds the Entity layer by name.
     * Errors out if not found or if there are duplicate object layers with the name 'entity'.
     */
    private fun findEntityLayerIndex(): Int {
        var indexFound = -1
        val layers: MapLayers = tiledMap.layers
        for(i in 0 until layers.size()) {
            val layer: MapLayer = layers.get(i)
            if(layer.name == "entities" && isObjectLayer(layer)) {
                if(indexFound == -1) indexFound = i
                else throw MonmakerException(
                    "Tiled map '${info.file}' had a second object layer named 'entities'. " +
                    "Only one allowed."
                )
            }
        }

        // If not found at all, error out
        if(indexFound == -1) throw MonmakerException(
            "Tiled map '${info.file}' did not have an 'entities' object layer. " +
            "The name is case-sensitive. " +
            "Also check that you did not make a non-object layer named 'entities'."
        )

        // Done
        return indexFound
    }

    /**
     * Checks that a MapLayer is an object layer.
     */
    private fun isObjectLayer(layer: MapLayer): Boolean = layer.javaClass == MapLayer::class.java

    /**
     * Ensures that TiledMap size agree's with info object.
     */
    private fun checkMapSize(tiledMap: TiledMap) {
        // Gets sizes from from loaded map
        val width: Int = tiledMap.properties.get("width", Int::class.java)
        val height: Int = tiledMap.properties.get("height", Int::class.java)
        val tileWidth: Int = tiledMap.properties.get("tilewidth", Int::class.java)
        val tileHeight: Int = tiledMap.properties.get("tileheight", Int::class.java)

        // Checks that sizes agree with map infos
        if(width != info.width || height != info.height) throw MonmakerException(
            "Tiled map '${info.file}' has dimensions ${width}tileX${height}, " +
            "but was expecting ${info.width}tileX${info.height}."
        )
        if(tileWidth != grid.tileWidth|| tileHeight != grid.tileHeight) throw MonmakerException(
            "Tile size of tiled map '${info.file}' has dimensions ${tileWidth}tileX${tileHeight}, " +
            "but was expecting ${grid.tileWidth}tileX${grid.tileHeight}."
        )
    }
}