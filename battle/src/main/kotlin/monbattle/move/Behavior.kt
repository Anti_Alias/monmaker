package monbattle.move

import monbattle.battle.Battle
import monbattle.battle.Team
import monbattle.monster.Monster


/**
 * Represents the behavior of a [Move].
 */
interface Behavior {
    /**
     * Uses a [Move] in battle.
     *
     * @param battle [Battle] this takes place in.
     * @param team [Team] of the monster that is using this [Move].
     * @param monster [Monster] that is using this [Move].
     * @param targets List of [Target]s that this move should hit.
     * @param move Move to perform.
     */
    fun use(
        battle: Battle,
        team: Team,
        monster: Monster,
        targets: List<Target>,
        move: Move
    )

    companion object {

        /**
         * Creates a Behavior depending of the [Category] of the [Move].
         */
        fun createDefault(category: Category): Behavior {
            TODO()
        }
    }
}

object DamageBehavior : Behavior {
    /**
     * Uses a [Move] in battle.
     *
     * @param battle [Battle] this takes place in.
     * @param team [Team] of the monster that is using this [Move].
     * @param monster [Monster] that is using this [Move].
     * @param targets List of [Target]s that this move should hit.
     * @param move Move to perform.
     */
    override fun use(
        battle: Battle,
        team: Team,
        monster: Monster,
        targets: List<Target>,
        move: Move
    ){
        TODO()
    }
}