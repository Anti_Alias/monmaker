package monmaker.controller

import com.badlogic.gdx.Input

/**
 * Acts as a mapping of keyboard buttons to a monmaker controller buttons.
 */
class KeyboardMapping(val mapping: Map<Int, Button>) {

    /**
     * Gets a monmaker button from the mapping.
     */
    operator fun get(code: Int): Button? = mapping[code]


    companion object {
        /**
         * Default keyboard mapping
         */
        fun default(): KeyboardMapping = KeyboardMapping(mapOf(
            Input.Keys.RIGHT to Button.RIGHT,
            Input.Keys.UP to Button.UP,
            Input.Keys.LEFT to Button.LEFT,
            Input.Keys.DOWN to Button.DOWN,
            Input.Keys.Z to Button.A,
            Input.Keys.X to Button.B,
            Input.Keys.A to Button.X,
            Input.Keys.S to Button.Y,
            Input.Keys.Q to Button.LB,
            Input.Keys.W to Button.RB,
            Input.Keys.NUM_1 to Button.LT,
            Input.Keys.NUM_2 to Button.RT
        ))
    }
}