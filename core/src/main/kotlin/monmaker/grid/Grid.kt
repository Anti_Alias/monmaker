package monmaker.grid

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import monmaker.world.Camera
import monmaker.entity.Entity
import monmaker.exceptions.MonmakerException
import monmaker.extensions.loadSync
import monmaker.game.AssetPaths
import monmaker.generator.Generator
import monmaker.generator.createGenerator
import monmaker.logging.Logger
import monmaker.world.World
import kotlin.math.floor


/**
 * Data structure in which tile-base entities reside.
 * @property tileWidth Width of each tile in pixels.
 * @property tileHeight Height of each tile in pixels.
 * @param startingMap Map to initially load.
 * @property mapInfos Metadata about the maps to be loaded.
 * @property tileGenerator Generator of tiles
 */
class Grid @JvmOverloads constructor(
    val tileWidth: Int,
    val tileHeight: Int,
    startingMap: String,
    mapInfos: Collection<MapInfo>,
    val tileGenerator: Generator<TileInfo>
) {

    constructor(
        tileWidth: Int,
        tileHeight: Int,
        startingMap: String,
        mapInfos: Collection<MapInfo>,
        tileGenerator: Any? = null
    ) : this(tileWidth, tileHeight, startingMap, mapInfos, createGenerator(tileGenerator))

    /**
     * World this grid belongs to.
     */
    internal lateinit var world: World

    /**
     * Metadata about the maps to be loaded.
     */
    val mapInfos: Set<MapInfo> = consumeMapInfos(mapInfos)

    /**
     * Map info that is initially loaded.
     */
    internal val startingMapInfo: MapInfo = mapInfos
        .find { it.name == startingMap }
        ?: throw MonmakerException("Could not find starting map '$startingMap'")

    /**
     * Mapping of map metadata to loaded maps.
     */
    private val mapData: MutableMap<MapInfo, MapData> = mutableMapOf()

    /**
     * Positions of entities in this Grid.
     */
    internal val entityPositions: MutableMap<GridPosition, MutableList<Entity>> = mutableMapOf()

    /**
     * Accumulated properties of tiles in this Grid.
     */
    private val tiles: MutableMap<GridPosition, Tile> = mutableMapOf()

    /**
     * Gets a tile from the Grid.
     * If no map is present at given location, an empty tile with no properties is returned.
     */
    fun getTile(x: Int, y: Int): Tile = tiles.getOrElse(GridPosition(x, y)) {
        Tile(x, y, mutableMapOf(), this)
    }

    /**
     * Finds the first tile with the name specified.
     * @param name Name of tile desired.
     */
    fun getTile(name: String): Tile = tiles.values.first {
        val tileName: Any? = it.properties["name"]
        tileName == name
    }

    /**
     * Finds the first tile with the name specified.
     * Null if not found.
     * @param name Name of tile desired.
     * @throws MonmakerException if tile is not found.
     */
    fun getTileOrNull(name: String): Tile? = tiles.values.firstOrNull {
        val tileName: Any? = it.properties["name"]
        tileName == name
    }

    /**
     * Finds the first tile with the name specified.
     * @param dest Destination to add tiles to.
     */
    fun getTiles(name: String, dest: MutableCollection<Tile>) {
        for(tile in tiles.values) {
            val tileName: Any? = tile.properties["name"]
            if(tileName == name)
                dest.add(tile)
        }
    }

    /**
     * Finds the first tile with the name specified.
     * @return Tiles with name found.
     */
    fun getTiles(name: String): List<Tile> {
        val dest = mutableListOf<Tile>()
        getTiles(name, dest)
        return dest
    }


    /**
     * Determines what infos are visible, loads the ones that are and unloads the ones that are not.
     */
    internal fun sync() {
        for(mapInfo in mapInfos) {
            if(cameraOverlaps(mapInfo)) {
                if(mapInfo !in mapData.keys) {
                    loadMap(mapInfo)
                }
            }
            else if(mapInfo in mapData.keys) {
                unloadMap(mapInfo)
            }
        }
    }

    /**
     * Computes the view for each loaded map.
     * This determines both the matrix used for transformation as well as
     * which tiles need to be rendered.
     */
    internal fun computeView() {
        mapData.values.forEach { it.computeView() }
    }

    /**
     * Renders the layers underneath entities.
     */
    fun renderUnderLayers() {
        mapData.values.forEach { it.renderUnderLayers()}
    }

    /**
     * Renders the layers over entities.
     */
    fun renderOverLayers() {
        mapData.values.forEach { it.renderOverLayers()}
    }

    /**
     * Disposes this Grid's resources.
     */
    internal fun unload() {
        val keysCopy = mapData.keys.toList()    // toList() done to prevent ConcurrentModificationException
        for(mapInfo in keysCopy)
            unloadMap(mapInfo)
    }

    /**
     * Updates the tile position for this Entity.
     * @param entity Entity to have it's position updated.
     * Assumed its local tileX and tileY coordinates have not yet been updated.
     * @param x New tileX position entity will be assigned to.
     * @param y New tileY position entity will be assigned to.
     */
    internal fun updateEntityPosition(entity: Entity, x: Int, y: Int) {

        // Removes current
        val currentPosition = GridPosition(entity.tileX, entity.tileY)
        val currentList: MutableList<Entity>? = entityPositions[currentPosition]
        if(currentList != null) {
            currentList.remove(entity)
            if(currentList.isEmpty())
                entityPositions.remove(currentPosition)
        }

        // Places in new tile
        val newPosition = GridPosition(x, y)
        val newList: MutableList<Entity> = entityPositions.getOrPut(newPosition) { mutableListOf() }
        newList.add(entity)
    }

    /**
     * Loads a map into this Grid.
     * @param info Map info to load.
     * @return Loaded map data.
     */
    private fun loadMap(info: MapInfo) {
        val paths: AssetPaths = world.game.assetPaths
        val fullPath = "${paths.maps}/${info.file}"
        val assetManager: AssetManager = world.game.assetManager
        val map: TiledMap = assetManager.loadSync(fullPath)
        try {
            val data = MapData(info, map, this)
            mapData[info] = data
            consumeTiles(data)
            consumeMarkers(data)
            spawnEntities(data)
            logger.log("Loaded map ${info.name}")
            logStatus()
            System.gc()
        }
        catch(t: Throwable) {
            assetManager.unload(fullPath)
            throw t
        }
    }

    /**
     * Unloads a map from this Grid.
     * @param data Loaded map data to unload.
     */
    private fun unloadMap(info: MapInfo) {
        val paths: AssetPaths = world.game.assetPaths
        val assetManager: AssetManager = world.game.assetManager
        val fullPath = "${paths.maps}/${info.file}"
        val data: MapData = mapData.getValue(info)
        despawnEntities(info)
        clearTiles(data)
        assetManager.unload(fullPath)
        mapData.remove(info)
        logger.log("Unloaded map ${info.name}")
        logStatus()
        System.gc()
    }

    /**
     * Loads and stores tiles in this Grid.
     */
    private fun consumeTiles(mapData: MapData) {
        val tiledMap: TiledMap = mapData.tiledMap
        val mapInfo: MapInfo = mapData.info
        for(y in 0 until mapInfo.height) {
            for(x in 0 until mapInfo.width) {
                for(layer: MapLayer in tiledMap.layers) if(layer is TiledMapTileLayer) {
                    val gdxTile: TiledMapTile? = layer.getCell(x, y)?.tile
                    if(gdxTile != null)
                        consumesTile(gdxTile, x+mapData.info.x, y+mapData.info.y)
                }
            }
        }
    }

    /**
     * Processes a Gdx tile and potentially stores in in the [Grid].
     */
    private fun consumesTile(gdxTile: TiledMapTile, x: Int, y: Int) {
        val keys = gdxTile.properties.keys
        if(keys.hasNext()) {
            val properties = mutableMapOf<String, Any>()
            for(key: String in keys)
                properties[key] = gdxTile.properties[key]
            tiles[GridPosition(x, y)] = Tile(x, y, properties, this)
        }
    }

    /**
     * Loads markers properties into tiles of this [Grid].
     */
    private fun consumeMarkers(mapData: MapData) {
        val tiledMap: TiledMap = mapData.tiledMap

        // Iterates through all objects in all marker layers for this map.
        for(gdxLayer: MapLayer in tiledMap.layers)
        if(gdxLayer.name == "markers")
        for(obj in gdxLayer.objects) {

            // Dumps all properties of gdx tile into the monmaker tile
            val name: String? = obj.name
            val x: Float = obj.properties.get("x") as Float
            val y: Float = obj.properties.get("y") as Float
            val tileX: Int = (x / tileWidth).toInt()
            val tileY: Int = (y / tileHeight).toInt()
            val tile: Tile = tiles.getOrPut(GridPosition(tileX, tileY)) {
                Tile(tileX, tileY, mutableMapOf(), this)
            }
            for(key in obj.properties.keys)
                tile._properties[key] = obj.properties[key]

            // Dump name and listener if one is associated with this tile
            if(name != null) {
                tile._properties[name] = name
                val newTile: TileInfo? = tileGenerator.generateOrNull(name)
                if(newTile != null)
                    tile.merge(newTile)
            }
        }
    }

    /**
     * Unloads tile properties from a particular MapData.
     */
    private fun clearTiles(mapData: MapData) {
        val mapInfo: MapInfo = mapData.info
        for(position: GridPosition in tiles.keys.toList()) {    // toList() done to prevent ConcurrentModificationException
            val inBounds: Boolean =
                position.x <= mapInfo.maxX &&
                position.x >= mapInfo.minX &&
                position.y <= mapInfo.maxY &&
                position.y >= mapInfo.minY
            if(inBounds) tiles.remove(position)
        }
    }

    /**
     * Spawns entities.
     */
    private fun spawnEntities(mapData: MapData) {
        val entityLayer: MapLayer = mapData.entityLayer
        for(obj in entityLayer.objects) {
            val entityName: String = obj.name
            val x: Float = obj.properties.get("x") as Float
            val y: Float = obj.properties.get("y") as Float
            val tileX: Int = mapData.info.x + floor(x.toDouble() / tileWidth).toInt()
            val tileY: Int = mapData.info.y + floor(y.toDouble() / tileHeight).toInt()
            world.spawnEntity(entityName, tileX, tileY)
        }
    }

    /**
     * Despawns entities.
     */
    private fun despawnEntities(mapInfo: MapInfo) {
        for(entity in world.entities) {
            val entityInBounds: Boolean =
                entity.tileX >= mapInfo.minX &&
                entity.tileY >= mapInfo.minY &&
                entity.tileX <= mapInfo.maxX &&
                entity.tileY <= mapInfo.maxY
            if(entityInBounds) {
                world.aDespawnEntity(entity)
                entityPositions.remove(GridPosition(entity.tileX, entity.tileY))
            }
        }
    }

    private fun cameraOverlaps(mapInfo: MapInfo): Boolean {

        // Gets mins and maxes of mapInfo in pixels
        val mapMinX: Float = (mapInfo.x * tileWidth).toFloat()
        val mapMinY: Float = (mapInfo.y * tileHeight).toFloat()
        val mapMaxX: Float = ((mapInfo.x + mapInfo.width) * tileWidth).toFloat()
        val mapMaxY: Float = ((mapInfo.y + mapInfo.height) * tileHeight).toFloat()

        // Returns comparison
        val camera: Camera = world.camera
        return camera.minX <= mapMaxX &&
               camera.minY <= mapMaxY &&
               camera.maxX >= mapMinX &&
               camera.maxY >= mapMinY
    }

    /**
     * Takes mapInfos supplied in constructor, validates that they don't overlap, and returns them as a set.
     */
    private fun consumeMapInfos(mapInfos: Collection<MapInfo>): Set<MapInfo> {
        val consumedMapInfos = mutableSetOf<MapInfo>()
        for(mapInfo in mapInfos) {
            for(consumedMapInfo in consumedMapInfos) {
                if(mapInfo.file == consumedMapInfo.file) throw MonmakerException(
                    "Grid cannot consume two maps of the same file '${mapInfo.file}'."
                )
                else if(mapInfo.overlaps(consumedMapInfo)) throw MonmakerException(
                    "Grid cannot consume maps that overlap. '${mapInfo.name}' and '${consumedMapInfo.name}' overlap."
                )
            }
            consumedMapInfos.add(mapInfo)
        }
        return consumedMapInfos
    }

    private fun logStatus() {
        logger.log("Tiles with properties: ${tiles.size}.")
    }

    companion object {
        val logger = Logger.forClass<Grid>()
    }
}