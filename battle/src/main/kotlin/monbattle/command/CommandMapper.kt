package monbattle.command

import monbattle.battle.Battle

/**
 * Interface that determines if a Command is allowed to be performed in battle.
 */
@FunctionalInterface
interface CommandMapper {

    /**
     * Maps one [Command] to a new one.
     * In most cases, the original [Command] is returned.
     * However, it is often the case that a [Command] needs to be transformed in some way.
     *
     * @param command [Command] to map.
     * @param battle [Battle] command is to perform in.
     * @return transformed [Command], or null if [Command] was not allowed.
     */
    fun map(command: Command, battle: Battle): Command?
}