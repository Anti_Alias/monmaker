package monbattle.desktop

import com.badlogic.gdx.Graphics.Monitor
import com.badlogic.gdx.Graphics.DisplayMode
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import monbattle.app.MonmakerApp
import monbattle.app.UserConfig
import monbattle.exceptions.MonmakerException
import java.io.File

/**
 * Starts desktop application
 */
fun main() {
	Launcher.main()
}

/**
 * Launcher object.
 */
object Launcher {
	fun main() {
		// If user config is not dumped, dump it now.
		val userConfigFile = File("config.yml")
		if(!userConfigFile.exists()) {
			val configString: String = javaClass.classLoader.getResource("config.yml").readText()
			userConfigFile.writeText(configString)
		}

		// Reads user config, if it exists.
		// Uses it to determine desired fullscreen display mode.
		val userConfig: UserConfig = readUserConfig()
		val fullscreenDisplayMode = findFullscreenDisplayMode(userConfig.fullscreen.fps, userConfig.monitor)

		// Creates LWJGL config using that user config
		val config = Lwjgl3ApplicationConfiguration()
		config.setWindowedMode(userConfig.windowed.width, userConfig.windowed.height)

		// Instantiates app
		val appClassName: String = userConfig.main
		val appClass: Class<*> = Class.forName(appClassName)
		if(appClass.superclass != MonmakerApp::class.java) throw MonmakerException(
			"Application class '${appClass.name}' does not extend ${MonmakerApp::class.java}."
		)
		val app = Class.forName(appClassName).getConstructor().newInstance() as MonmakerApp

		// Sets configuration and starts app
		app.setConfiguration(userConfig)
		app.setFullscreenDisplayMode(fullscreenDisplayMode)
		Lwjgl3Application(app, config)
	}

	/**
	 * Finds a suitable display mode
	 */
	private fun findFullscreenDisplayMode(desiredFps: Int, monitorIndex: Int): DisplayMode {
		val monitors: Array<Monitor> = Lwjgl3ApplicationConfiguration.getMonitors()
		if (monitorIndex >= monitors.size) throw MonmakerException(
			"Monitor $monitorIndex outside of range [0, ${monitors.size - 1}]"
		)
		println(monitorIndex)
		val monitor: Monitor = monitors[monitorIndex]
		val displayModes = Lwjgl3ApplicationConfiguration
			.getDisplayModes(monitor)
			.asSequence()
			.sortedWith(Comparator<DisplayMode> { a, b ->

				// Calculates number of pixels in both display modes
				val aSize = a.width * a.height
				val bSize = b.width * b.height

				// Otherwise, try to find the largest display mode with the fastest refresh rate.
				if (aSize > bSize) -1
				else if (bSize > aSize) 1
				else if (a.refreshRate == desiredFps) -1
				else if (b.refreshRate == desiredFps) 1
				else if (a.refreshRate > b.refreshRate) -1
				else if (b.refreshRate > a.refreshRate) 1
				else 0
			})
			.toList()
		return displayModes.first()
	}

	/**
	 * Reads a user config file if it exists. Otherwise, creates a default one.
	 */
	private fun readUserConfig(): UserConfig {
		val mapper = ObjectMapper(YAMLFactory()).registerModule(KotlinModule())
		return mapper.readValue(File("config.yml"), UserConfig::class.java)
	}
}