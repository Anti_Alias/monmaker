package monbattle.battle

import monbattle.monster.Monster

/**
 * Represents a slot on a team that may or may not hold a monster.
 */
class Slot {
    var monster: Monster? = null
}