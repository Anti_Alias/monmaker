package monmaker.grid

import monmaker.exceptions.MonmakerException

/**
 * Metadata about a particular tiled-map to be loaded into a [Grid] when loaded dynamically.
 *
 * @property file Tiled-map file to load from.
 * @property name Programmable name of the map.
 * @property x Offset-X of this map when loaded into a [Grid].
 * @property y Offset-Y of this map when loaded into a [Grid].
 * @property width Width of this map in tiles.
 * @property height Height of this map in tiles.
 */
class MapInfo(
    val file: String,
    val name: String,
    val x: Int,
    val y: Int,
    val width: Int,
    val height: Int
) {
    init {
        if(width<=0 || height<=0)
            throw MonmakerException("Size of MapInfo must be positive. Got ${width}tileX{$height}.")
    }

    val minX: Int get() = x
    val minY: Int get() = y
    val maxX: Int get() = x + width - 1
    val maxY: Int get() = y + height - 1

    /**
     * @return true if this MapInfo overlaps another
     */
    internal fun overlaps(info: MapInfo): Boolean =
       minX <= info.maxX &&
       minY <= info.maxY &&
       maxX >= info.minX &&
       maxY >= info.minY
}
