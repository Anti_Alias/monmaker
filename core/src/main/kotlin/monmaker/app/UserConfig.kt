package monmaker.app

import monmaker.exceptions.MonmakerException

/**
 * User configuration.
 */
data class UserConfig(
    val main: String,
    val mode: Mode,
    val monitor: Int,
    val windowed: Windowed,
    val fullscreen: Fullscreen
) {
    init {
        if(monitor < 0) throw MonmakerException("Invalid monitor $monitor.")
    }
}

enum class Mode { WINDOWED, FULLSCREEN }

/**
 * Windowed configuration.
 */
data class Windowed(
    val vsync: Boolean,
    val fps: Int,
    val width: Int,
    val height: Int
) {
    init {
        if(fps <= 0) throw MonmakerException("Invalid windowed FPS '$fps'")
        if(width <= 0 || height <= 0) throw MonmakerException("Invalid window size ${width}x${height}.")
    }
}

/**
 * Fullscreen configuration
 */
data class Fullscreen(val vsync: Boolean, val fps: Int) {
    init {
        if(fps <= 0) throw MonmakerException("Invalid fullscreen FPS '$fps'")
    }
}