package monmaker.script

import monmaker.event.*
import monmaker.exceptions.MonmakerException
import monmaker.extensions.*

/**
 * Script that allows an [Entity] to prompt a simple message when selected.
 */
class SimpleTalkerScript(val messages: List<String>) : EntityScript() {

    /**
     * Alternate constructor for a single message.
     */
    constructor(message: String) : this(listOf(message))

    init {
        if(messages.isEmpty()) throw MonmakerException("Messages cannot be empty.")
    }

    private var conversing: Boolean = false
    private var messageIndex: Int = 0

    override fun on(event: Event) {
        when(event) {
            is PromptCloseEvent -> if(conversing) finishConversing()
            is SelectEvent -> onSelect(event)
        }
    }

    private fun onSelect(event: SelectEvent) {
        entity.fire(DisableEvent())
        conversing = true
        entity.queue.look(event.direction.reverse)
        promptor
            .queue
            .prompt(messages[messageIndex])
            .close()
        if(messageIndex != messages.size-1)
            messageIndex += 1
    }

    private fun finishConversing() {
        entity.fire(EnableEvent())
        conversing = false
    }
}