package monmaker.queue

import monmaker.exceptions.MonmakerException
import kotlin.math.max
import kotlin.math.min

/**
 * Holds a command and some extra metadata.
 */
internal class CommandHolder<T>(internal val command: Command<T>, val durationSeconds: Float) {
    var elapsedSeconds: Float = 0f
    init {
        if(durationSeconds < 0f) throw MonmakerException("Invalid duration $durationSeconds.")
    }
    var isStarted: Boolean = false
    val isDone: Boolean get() = elapsedSeconds >= durationSeconds
    fun start(target: T): Boolean {
        isStarted = command.start(target)
        return isStarted
    }
    fun run(target: T, deltaTime: Float): Boolean {
        elapsedSeconds += deltaTime
        val t: Float = max(0f, min(1f, elapsedSeconds/durationSeconds))
        return command.run(target, t)
    }
    fun finish(target: T) {
        command.finish(target)
    }
}
