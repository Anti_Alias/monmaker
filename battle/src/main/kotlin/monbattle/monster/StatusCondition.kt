package monbattle.monster

interface StatusCondition { val name: String }

object Paralysis: StatusCondition { override val name = "paralysis" }
object Sleep: StatusCondition { override val name = "sleep" }
object Freeze: StatusCondition { override val name = "freeze" }
object Burn: StatusCondition { override val name = "burn" }
object Poison: StatusCondition { override val name = "poison" }
object Confusion: StatusCondition { override val name = "confusion" }
object Infatuation: StatusCondition { override val name = "infatuation" }
object Trap: StatusCondition { override val name = "trap" }
object Nightmare: StatusCondition { override val name = "nightmare" }
object Torment: StatusCondition { override val name = "torment" }
object Disable: StatusCondition { override val name = "disable" }
object Yawn: StatusCondition { override val name = "yawn" }
object HealBlock: StatusCondition { override val name = "heal-block" }
object NoTypeImmunity: StatusCondition { override val name = "no-type-immunity" }
object LeechSeed: StatusCondition { override val name = "leech-seed" }
object Embargo: StatusCondition { override val name = "embargo" }
object PerishSong: StatusCondition { override val name = "perish-song" }
object Ingrain: StatusCondition { override val name = "ingrain" }
