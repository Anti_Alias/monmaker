package monmaker.grid

import monmaker.event.EnterEvent
import monmaker.event.Event
import monmaker.event.ExitEvent
import monmaker.event.SelectEvent
import monmaker.extensions.*

/**
 * Represents a simple listener that prompts different messages when
 * a tile is entered, exited or selected.
 * @property onSelect Optional message to prompt when [Tile] is selected.
 * @property onEnter Optional message to prompt when [Tile] is entered.
 * @property onExit Optional message to prompt when [Tile] is exited.
 */
data class PromptListener (
    val onSelect: List<String> = emptyList(),
    val onEnter: List<String> = emptyList(),
    val onExit: List<String> = emptyList()
) : TileListener {

    /**
     * Alternate constructor that makes it easier to put in a single message.
     */
    constructor(
        onSelect: String? = null,
        onEnter: String? = null,
        onExit: String? = null
    ) : this(
        if(onSelect == null) emptyList() else listOf(onSelect),
        if(onEnter == null) emptyList() else listOf(onEnter),
        if(onExit == null) emptyList() else listOf(onExit)
    )

    override fun on(event: Event, tile: Tile) {
        val messages: List<String> = when(event) {
            is SelectEvent -> onSelect
            is EnterEvent -> onEnter
            is ExitEvent -> onExit
            else -> emptyList()
        }
        if(messages.isNotEmpty()) {
            val promptor = tile.grid.world.game.promptor
            for (line in messages)
                promptor.queue.prompt(line)
            promptor.queue.close()
        }
    }
}