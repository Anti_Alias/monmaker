package monmaker.controller

/**
 * Represents a virtual controller.
 */
class Controller {

    /**
     * Button flags
     */
    private var buttons: BooleanArray = BooleanArray(12) { false }
    private var buttonsPrev: BooleanArray = BooleanArray(12) { false }

    /**
     * Left joystick.
     */
    val leftStick = Stick(x = 0f, y = 0f)

    /**
     * Right joystick.
     */
    val rightStick = Stick(x = 0f, y = 9f)

    /**
     * Determines if a given button is down.
     */
    fun isDown(button: Button): Boolean = buttons[button.ordinal]

    /**
     * Determines if a given button is up.
     */
    fun isUp(button: Button): Boolean = !buttons[button.ordinal]

    /**
     * Determines if a given button was just pressed.
     */
    fun isPressed(button: Button): Boolean = buttons[button.ordinal] && !buttonsPrev[button.ordinal]

    /**
     * Determines if a given button was just released.
     */
    fun isReleased(button: Button): Boolean = !buttons[button.ordinal] && buttonsPrev[button.ordinal]

    /**
     * Manually presses a button.
     */
    fun press(button: Button) {
        buttons[button.ordinal] = true
    }

    /**
     * Manually releases button.
     */
    fun release(button: Button) {
        buttons[button.ordinal] = false
    }

    /**
     * Updates previous button statuses with current.
     */
    fun update() {
        for (i in 0 until buttons.size) {
            buttonsPrev[i] = buttons[i]
        }
    }
}

/**
 * Determines the x and y values of the stick.
 * These range from -1 to 1 with 0 representing the center.
 */
class Stick internal constructor(var x: Float = 0f, var y: Float = 0f)

/**
 * Represents a particular button that can be pressed on the controller.
 */
enum class Button {
    A, B, X, Y, RIGHT, UP, LEFT, DOWN, LB, RB, LT, RT;

    companion object {
        /**
         * Converts a LibGDX button code to a button.
         */
        fun fromControllerCode(buttonCode: Int): Button? = when(buttonCode) {
            0 -> A
            1 -> B
            2 -> X
            3 -> Y
            8 -> LB
            9 -> RB
            10 -> LT
            11 -> RT
            else -> null
        }
    }
}