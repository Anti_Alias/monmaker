package monmaker.exceptions

/**
 * Generic monmaker exception.
 */
class MonmakerException @JvmOverloads constructor(msg: String, cause: Throwable? = null) : RuntimeException(msg, cause)