package monmaker.entity

/**
 * Mode in which animation plays.
 */
enum class AnimationMode {

    /**
     * Animation is tied to movement of player.
     * Every two tiles moved is a full animation cycle.
     */
    MOVEMENT,

    /**
     * Animation is free to play on its own.
     */
    FREE
}