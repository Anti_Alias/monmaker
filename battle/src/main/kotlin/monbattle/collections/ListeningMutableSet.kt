package monbattle.collections

/**
 * Set implementation that listens for when elements are added or removed.
 * Invokes callbacks when doing so.
 */
internal class ListeningMutableSet<T>(
    val underlying: MutableSet<T> = HashSet<T>(),
    val onAdd: ((T)->Unit)? = null,
    val onRemove: ((T)->Unit)? = null
) : MutableSet<T> by underlying {
    override fun add(element: T): Boolean {
        val added: Boolean = underlying.add(element)
        if(added) onAdd?.invoke(element)
        return added
    }

    override fun remove(element: T): Boolean {
        val removed: Boolean = underlying.remove(element)
        if(removed) onRemove?.invoke(element)
        return removed
    }

    override fun addAll(collection: Collection<T>): Boolean {
        var added = false
        for(element in collection) {
            if(add(element)) {
                onAdd?.invoke(element)
                added = true
            }
        }
        return added
    }

    override fun removeAll(collection: Collection<T>): Boolean {
        var removed = false
        for(element in collection) {
            if(remove(element)) {
                onAdd?.invoke(element)
                removed = true
            }
        }
        return removed
    }
}