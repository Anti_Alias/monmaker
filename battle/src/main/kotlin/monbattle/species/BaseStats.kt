package monbattle.species

import monbattle.exceptions.BattleException

/**
 * Represents the base stats of a monster.
 * @property hp Base hit points
 * @property attack Base attack
 * @property defense Base defense
 * @property specialAttack base special attack
 * @property specialDefense base special defense
 * @property speed Base speed
 */
data class BaseStats(
    val hp: Int,
    val attack: Int,
    val defense: Int,
    val specialAttack: Int,
    val specialDefense: Int,
    val speed: Int
) {

    // Validates base stats
    init {
        if(hp < 0) throw BattleException("hp must be >= 0. Got $hp.")
        if(attack < 0) throw BattleException("attack must be >= 0. Got $attack.")
        if(defense < 0) throw BattleException("defense must be >= 0. Got $defense.")
        if(specialAttack < 0) throw BattleException("specialAttack must be >= 0. Got $specialAttack.")
        if(specialDefense < 0) throw BattleException("specialDefense must be >= 0. Got $specialDefense.")
        if(speed < 0) throw BattleException("speed must be >= 0. Got $speed.")
    }

    /**
     * Stat total of the monster.
     */
    val total: Int get() = hp + attack + defense + specialAttack + specialDefense + speed
}