package monmaker.entity

import monmaker.controller.Button
import monmaker.event.*
import monmaker.extensions.setState
import monmaker.extensions.travel
import monmaker.script.EntityScript
import monmaker.sound.Sound

/**
 * Simple player script that allows for controlling an Entity via the keyboard.
 * @property selectButton [Button] that is used for selections.
 */
class PlayerScript(private val selectButton: Button = Button.A) : EntityScript() {

    private var enabled: Int = 0
        set(value) {
            if(value < 0) field = 0
            else field = value
        }
    private val movementHandler = MovementHandler()
    private val selectionHandler = SelectionHandler(movementHandler)

    override fun register() {
        world.camera.setTarget(entity.name)
    }

    override fun deregister() {
        world.camera.unsetTarget()
    }

    override fun on(event: Event) {
        when (event) {
            is DisableEvent -> enabled += 1
            is EnableEvent -> enabled -= 1
            is PromptOpenEvent -> entity.fire(DisableEvent())
            is PromptCloseEvent -> entity.fire(EnableEvent())
            is TickEvent -> {
                selectionHandler.handle()
                movementHandler.handle(event)
            }
        }
    }

    /**
     * Helper class to PlayerScript that handles movement.
     */
    private inner class MovementHandler {

        private var request: Direction? = null

        fun cancelRequest() {
            request = null
        }

        fun handle(event: TickEvent) {
            // Determines direction player is requesting to travel in.
            readDirection()

            // If entity can be controlled and is requesting to travel in a given direction...
            if(enabled == 0) {
                val direction = request
                if (direction != null) while (entity.queue.willBeEmpty(event.deltaTime)) entity.queue
                    .setState("walk")
                    .travel(direction, bumpSound = Sound("bump.wav", blockSeconds = 0.25f))
            }
        }

        fun readDirection() {

            // Unsets current requested direction if button was just released
            val controller = controller
            if(
                (controller.isReleased(Button.RIGHT) && request == Direction.E) ||
                (controller.isReleased(Button.UP) && request == Direction.N) ||
                (controller.isReleased(Button.LEFT) && request == Direction.W) ||
                (controller.isReleased(Button.DOWN) && request == Direction.S)
            ) request = null

            // Sets new requested direction
            request = if(request == null) {
                if (controller.isDown(Button.RIGHT)) Direction.E
                else if (controller.isDown(Button.UP)) Direction.N
                else if (controller.isDown(Button.LEFT)) Direction.W
                else if (controller.isDown(Button.DOWN)) Direction.S
                else null
            }
            else {
                if (controller.isPressed(Button.RIGHT)) Direction.E
                else if (controller.isPressed(Button.UP)) Direction.N
                else if (controller.isPressed(Button.LEFT)) Direction.W
                else if (controller.isPressed(Button.DOWN)) Direction.S
                else request
            }
        }
    }

    /**
     * Helper class that handles selection.
     */
    private inner class SelectionHandler(val movementHandler: MovementHandler) {
        fun handle() {
            if(enabled == 0 && controller.isPressed(selectButton) && !entity.isTravelling) {
                movementHandler.cancelRequest()
                entity.selectAdjacentTile()
            }
        }
    }
}