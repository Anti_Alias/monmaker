package monmaker.screen

import monmaker.event.Event
import monmaker.exceptions.MonmakerException
import monmaker.game.Game


/**
 * Renders this Screen.
 */
abstract class Screen {

    internal var _game: Game? = null

    /**
     * Game that this Screen is attached to.
     */
    val game: Game get() =
        _game ?: throw MonmakerException("Screen tried to acces its game, but was not attached to one")

    /**
     * Sets the viewport of this Screen.
     */
    abstract fun setViewport(width: Int, height: Int)

    /**
     * Runs this Screen's logic.
     * @param deltaTime Time since last frame in seconds.
     */
    abstract fun run(deltaTime: Float)

    /**
     * Renders this screen.
     * @param deltaTime Time since last frame in seconds.
     */
    abstract fun render()

    /**
     * Loads resources for this screen
     */
    abstract fun load()

    /**
     * Disposes of Screen's resources.
     */
    abstract fun unload()

    /**
     * Handles an event that is fired.
     */
    abstract fun fire(event: Event)
}