package monmaker.game

/**
 * Determines how a [Game] should stretch the scene.
 */
enum class ScalingType {
    /**
     * Aspect ratio is preserved.
     * Black lines are present when window size does not match aspect ratio.
     */
    BLACK_LINES,

    /**
     * Aspect ratio is preserved.
     * Display is zoomed when windows size does not match aspect ratio.
     * Objects may be cut off.
     */
    NO_BLACK_LINES,

    /**
     * Aspect ratio is not preserved.
     * Display is stretched to match the window size.
     */
    STRETCH
}