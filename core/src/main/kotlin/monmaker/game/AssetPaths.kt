package monmaker.game

/**
 * Object that stores the paths of assets.
 */
data class AssetPaths @JvmOverloads constructor(
    val animations: String = "animations",
    val maps: String = "maps",
    val music: String = "music",
    val sounds: String = "sounds",
    val backgrounds: String = "backgrounds",
    val skins: String = "skins"
)