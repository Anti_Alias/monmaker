package mygame

import monbattle.entity.*
import monbattle.script.AimlessScript
import monbattle.script.SimpleTalkerScript

/**
 * Example class for generating simple entities.
 */
object SimpleEntityGenerator {

    /**gra
     * Generates a simple rocket Entity.
     */
    fun generate_Rocket() = Entity(
        name = "Rocket",
        direction = Direction.W,
        scripts = listOf(
            AimlessScript(),
            SimpleTalkerScript(messages = listOf(
                "Grunt: You {SHAKE}damn{ENDSHAKE} kids with your hormone-laden milk think you're so tall!",
                "Grunt: Back in my day, Soylent was the only thing we drank!"
            ))
        ),
        states = States(
            startingState = "idle",
            values = mapOf(
                "idle" to EntityAnimation(
                    file = "rocket-m-19x26.png",
                    frameWidth = 19,
                    frameHeight = 26,
                    frameIndices = listOf(1, 5, 9, 13)
                ),
                "walk" to EntityAnimation(
                    file = "rocket-m-19x26.png",
                    frameWidth = 19,
                    frameHeight = 26
                )
            )
        )
    )

    /**
     * Generates a simple rocket Entity.
     */
    fun generate_Red() = Entity(
        name = "Red",
        direction = Direction.S,
        scripts = listOf(PlayerScript()),
        states = States(
            startingState = "idle",
            values = mapOf(
                "idle" to EntityAnimation(
                    file = "red-32x32.png",
                    frameIndices = listOf(1, 5, 9, 13)
                ),
                "walk" to EntityAnimation("red-32x32.png")
            )
        )
    )

    fun generate_Lass() = Entity(
        name = "Lass",
        direction = Direction.S,
        scripts = listOf(
            AimlessScript(),
            SimpleTalkerScript(messages = listOf(
                "Could someone explain to me how houses are somehow bigger on the inside than on the outside?",
                "Maybe someone with a 200IQ could figure it out..."
            ))
        ),
        states = States(
            startingState = "idle",
            values = mapOf(
                "idle" to EntityAnimation(
                    file = "lass-32x32.png",
                    frameIndices = listOf(1, 5, 9, 13)
                ),
                "walk" to EntityAnimation("lass-32x32.png")
            )
        )
    )
}