package monmaker.grid

import monmaker.event.Event

/**
 * Object that listens for events that happen to a [Tile] instance.
 */
@FunctionalInterface
interface TileListener {
    fun on(event: Event, tile: Tile)
}

/**
 * Listener that does nothing.
 */
object StubTileListener : TileListener {
    override fun on(event: Event, tile: Tile) {}
}