package monmaker.grid

/**
 * Position in a Grid.
 */
internal data class GridPosition(val x: Int, val y: Int)