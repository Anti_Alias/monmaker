package monmaker.graphics

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

/**
 * Union of multiple batch types.
 */
class Renderers(
    val spriteBatch: SpriteBatch,
    val shapeRenderer: ShapeRenderer
) {
    /**
     * No-arg constructor. Provides defaults.
     */
    constructor() : this(SpriteBatch(), ShapeRenderer())
}