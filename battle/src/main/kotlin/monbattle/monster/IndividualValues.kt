package monbattle.monster

import monbattle.exceptions.BattleException
import java.util.Random
import kotlin.math.roundToInt

/**
 * Represents the initial values of a monster.
 * These values range from 0-32.
 * @property hp Base hit points
 * @property attack Base attack
 * @property defense Base defense
 * @property specialAttack base special attack
 * @property specialDefense base special defense
 * @property speed Base speed
 */
data class IndividualValues(
    val hp: Int,
    val attack: Int,
    val defense: Int,
    val specialAttack: Int,
    val specialDefense: Int,
    val speed: Int
) {

    // Validates base stats
    init {
        if (hp < 0) throw BattleException("hp must be >= 0. Got $hp.")
        if (attack < 0) throw BattleException("attack must be >= 0. Got $attack.")
        if (defense < 0) throw BattleException("defense must be >= 0. Got $defense.")
        if (specialAttack < 0) throw BattleException("specialAttack must be >= 0. Got $specialAttack.")
        if (specialDefense < 0) throw BattleException("specialDefense must be >= 0. Got $specialDefense.")
        if (speed < 0) throw BattleException("speed must be >= 0. Got $speed.")
    }

    companion object {

        /**
         * Generates a random set of IndividualValues.
         * @param random Source of randomness.
         */
        @JvmStatic
        fun random(random: Random): IndividualValues = IndividualValues(
            hp = (random.nextDouble() * 31).roundToInt(),
            attack = (random.nextDouble() * 31).roundToInt(),
            defense = (random.nextDouble() * 31).roundToInt(),
            specialAttack = (random.nextDouble() * 31).roundToInt(),
            specialDefense = (random.nextDouble() * 31).roundToInt(),
            speed = (random.nextDouble() * 31).roundToInt()
        )

        /**
         * Generates a random set of IndividualValues.
         */
        @JvmStatic
        fun random(): IndividualValues = random(Random())
    }
}