package monmaker.entity

import monmaker.exceptions.MonmakerException
import monmaker.world.World

/**
 * Represents a reference to an Entity given its name.
 * Subsequent requests to a target result in the cached entity being returned.
 * If cached entity is removed from its world, the target's cache will be invalidate on the next invocation of
 * [getOrNull].
 */
class Target(val world: World, val entityName: String) {
    private var entity: Entity? = null

    /**
     * Gets an Entity.
     * @throws MonmakerException if Entity is not found.
     */
    fun get(): Entity {
        val result: Entity? = getOrNull()
        return result ?: throw MonmakerException("Could not find entity '$entityName' in world.")
    }

    /**
     * Gets an entity, or null if not found.
     */
    fun getOrNull(): Entity? {
        val entity = entity
        if (entity != null) {
            if (entity._world == world)
                return entity
        } else this.entity = world.getEntityOrNull(entityName)
        return this.entity
    }
}