package mygame

import monbattle.grid.PromptListener
import monbattle.grid.TileInfo

object SimpleTileGenerator {

    fun generate_HM01() = TileInfo(PromptListener("This is just a demo. You can't pick up items yet. {SHAKE}TOUGH LUCK!!!{ENDSHAKE}"))
}