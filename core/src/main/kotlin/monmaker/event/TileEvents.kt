package monmaker.event

import monmaker.entity.Direction
import monmaker.entity.Entity
import monmaker.grid.Tile

/**
 * Represents the event in which an [Entity] finishes entering a [Tile].
 * @param tile [Tile] being entered.
 * @param entity [Entity] entering the tile.
 * @param direction [Direction] the [Entity] travelled.
 */
class EnterEvent(
    val entity: Entity,
    val direction: Direction
) : Event

/**
 * Represents the event in which an [Entity] finishes exiting a [Tile].
 * @param tile [Tile] being exited.
 * @param entity [Entity] exiting the tile.
 * @param direction [Direction] the [Entity] travelled.
 */
class ExitEvent(
    val entity: Entity,
    val direction: Direction
) : Event

/**
 * Represents the event in which an [Entity] selects this [Tile].
 * @param tile [Tile] being selected.
 * @param entity [Entity] doing the selecting.
 * @param direction [Direction] the [Entity] was facing when the selecting.
 */
class SelectEvent(
    val entity: Entity,
    val direction: Direction
) : Event