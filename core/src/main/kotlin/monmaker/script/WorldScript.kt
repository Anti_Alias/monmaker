package monmaker.script

import monmaker.exceptions.MonmakerException
import monmaker.world.World

/**
 * Represents a [Script] that runs from the perspective of a [World].
 */
abstract class WorldScript : Script() {

    internal var _world: World? = null

    override val world: World = _world ?: throw MonmakerException(
        "Script '${javaClass.name}' could not access its world because it did not belong to one."
    )
}