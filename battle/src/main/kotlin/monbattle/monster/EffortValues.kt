package monbattle.monster

import monbattle.exceptions.BattleException

/**
 * Represents the effort values of a monster.
 */
class EffortValues(
    hp: Int = 0,
    attack: Int=0,
    defense: Int = 0,
    specialAttack: Int = 0,
    specialDefense: Int = 0,
    speed: Int = 0)
{

    private var _hp: Int = 0
    private var _attack: Int = 0
    private var _defense: Int = 0
    private var _specialAttack: Int = 0
    private var _specialDefense: Int = 0
    private var _speed: Int = 0

    init {
        _hp = hp
        _attack = attack
        _defense = defense
        _specialAttack = specialAttack
        _specialDefense = specialDefense
        _speed = speed
    }

    var hp: Int
        get() = _hp
        set(value) {
            _hp = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _hp -= total - MAX_TOTAL
        }

    var attack: Int
        get() = _attack
        set(value) {
            _attack = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _attack -= total - MAX_TOTAL
        }

    var defense: Int
        get() = _defense
        set(value) {
            _defense = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _defense -= total - MAX_TOTAL
        }

    var specialAttack: Int
        get() = _specialAttack
        set(value) {
            _specialAttack = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _specialAttack -= total - MAX_TOTAL
        }

    var specialDefense: Int
        get() = _specialDefense
        set(value) {
            _specialDefense = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _specialDefense -= total - MAX_TOTAL
        }

    var speed: Int
        get() = _speed
        set(value) {
            _speed = clamp(value)
            val total = total
            if(total > MAX_TOTAL)
                _speed -= total - MAX_TOTAL
        }

    init {
        if(_hp !in 0..255) throw BattleException("hp EV outside of range [0, 255]")
        if(_attack !in 0..255) throw BattleException("attack EV outside of range [0, 255]")
        if(_defense !in 0..255) throw BattleException("defense EV outside of range [0, 255]")
        if(_specialAttack !in 0..255) throw BattleException("specialAttack EV outside of range [0, 255]")
        if(_specialDefense !in 0..255) throw BattleException("specialDefense EV outside of range [0, 255]")
        if(_speed !in 0..255) throw BattleException("speed EV outside of range [0, 255]")
        if(_hp + _attack + _defense + _specialAttack + _specialDefense + _speed > 510)
            throw BattleException("EV total was $total, which is outside the range [0, 510]")
        this._hp = hp
        this._attack = attack
        this._defense = defense
        this._specialAttack = specialAttack
        this._specialDefense = specialDefense
        this._speed = speed
    }

    /**
     * Total of all effort values.
     */
    val total: Int get() = hp + attack + defense + specialAttack + specialDefense + speed

    private fun clamp(value: Int): Int = if(value < 0) 0 else if(value > 255) 255 else value

    companion object {
        const val MAX_TOTAL: Int = 510
    }
}