package monbattle.battle

import monbattle.monster.EffortValues
import monbattle.exceptions.BattleException
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EffortValuesTest {
    @Test
    fun testValues() {
        val values = EffortValues(hp = 255, attack = 245, defense = 1, specialAttack = 2, specialDefense = 3, speed = 4)
        assertEquals(255, values.hp)
        assertEquals(245, values.attack)
        assertEquals(1, values.defense)
        assertEquals(2, values.specialAttack)
        assertEquals(3, values.specialDefense)
        assertEquals(4, values.speed)
    }

    @Test
    fun testStatTotalTooLarge() {
        assertFailsWith<BattleException> {
            EffortValues(hp = 255, attack = 255, defense = 0, specialAttack = 0, specialDefense = 0, speed = 1)
        }
    }

    @Test
    fun testSingleValueTooLarge() {
        assertFailsWith<BattleException> {
            EffortValues(hp = 256, attack = 0, defense = 0, specialAttack = 0, specialDefense = 0, speed = 0)
        }
        assertFailsWith<BattleException> {
            EffortValues(hp = 0, attack = 256, defense = 0, specialAttack = 0, specialDefense = 0, speed = 0)
        }
        assertFailsWith<BattleException> {
            EffortValues(hp = 0, attack = -1, defense = 0, specialAttack = 0, specialDefense = 0, speed = 0)
        }
        assertFailsWith<BattleException> {
            EffortValues(hp = 0, attack = 0, defense = -1, specialAttack = 256, specialDefense = 0, speed = 0)
        }
        assertFailsWith<BattleException> {
            EffortValues(hp = 0, attack = 0, defense = 0, specialAttack = 0, specialDefense = 256, speed = 0)
        }
        assertFailsWith<BattleException> {
            EffortValues(hp = 0, attack = 0, defense = 0, specialAttack = 0, specialDefense = 0, speed = 256)
        }
    }
}