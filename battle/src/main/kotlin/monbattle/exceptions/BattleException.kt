package monbattle.exceptions

/**
 * Exception for when something incorrect occurs in battle-related code.
 */
class BattleException(message: String?, cause: Throwable? = null) : RuntimeException(message, cause)